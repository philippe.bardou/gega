# GEGA - Gallus Enriched Gene Annotation - Easily explore the richness of the chicken genome

## About

Welcome to GEGA, a user-friendly tool for navigating through genomics and functional annotations for **78,323 genes** produced by gathering the reference databases, NCBI-RefSeq & EMBL-Ensembl. With this gene-enriched atlas, GEGA offers information for **24,102 protein-coding genes (PCGs)** and **44,428 long non-coding RNAs (lncRNAs)**, greatly enhancing the number of genes provided by each resource separately.

But GEGA is more than just a gene locus database. It offers a range of features that allow you to go deeper into the functional aspects of these genes. Explore their expression and coexpression profiles across a collection of **47 tissues** from **36 datasets** and **1400 samples**, discover tissue-specific variations and study expression as a function of sex or age.

If you are interested in **one specific gene**, a **list of genes** or a **genomic region** (e.g., QTL region), GEGA's user-friendly interface enables efficient gene analysis, easy downloading of results and a multitude of graphical representations, from basic gene information to detailed visualization of expression levels. 

## License

GEGA source code is distributed under the [GPL3 license](https://forgemia.inra.fr/asterics/asterics/-/blob/master/LICENSE).

INRAE, as the publisher of this website, owns the trademarks and logos on the site unless otherwise stipulated. Consequently, you must not reuse the said trademarks, logos, and images without prior written authorisation from INRAE. Other content may also be subject to intellectual property rights.