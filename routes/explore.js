var express = require('express');
var router = express.Router();

var Json2csvParser = require('json2csv').Parser;
var ss = require('simple-statistics')
var database = require('../database');


/* GET explore page. */
router.get('/explore', function(req, res, next) {
    res.render('explore', { pagename:'Explore' });
});


/* GET chr and pos from field and value */
router.get('/get_pos', function(request, response, next){
  var search_field = decodeURIComponent(request.query.field);
  var search_query = request.query.query;
  var output = []
  var query = `SELECT chr,start,end FROM annotation where ${search_field} = "${search_query}"`;
  database.query(query, function(error, data){
    if(typeof data != "undefined") {
      data.forEach(function(row){
        output.push({
          'chr' : row.chr,
          'start' : row.start,
          'end' : row.end
        });
      });
    }
    response.json(output);
  });
});

/* GET header for annotation datatable */
router.get('/get_annotation_header', function(request, response, next){
  var query = `SELECT * FROM annotation_header order by ID`;
  var output = [];
  database.query(query, function(error, data){
    data.forEach(function(row){
      output.push({
        'name' : row.name,
        'displayName' : row.displayName,
        'meta' : row.meta,
        'description' : row.description,
        'example' : row.example,
        'type' : row.type,
        'visible' : row.visible,
        'sortable' : row.sortable,
        'searchable' : row.searchable,
        'facetable' : row.facetable,
        'graph' : row.graph,
        'templateLink' : row.templateLink
      });
    });
    response.json(output);
  });
});


/* GET distinct tissue for expression_by_tissue table */
router.get('/get_expression_distinct_tissue', function(request, response, next){
  var query = `SELECT DISTINCT tissue FROM expression_by_tissue order by tissue`;
  var output = [];
  database.query(query, function(error, data){
    data.forEach(function(row){
      output.push({
        'tissue' : row.tissue
      });
    });
    response.json(output);
  });
});


/* GET expression data for filtered geneID => heatmap */
router.post('/get_expression_for_filtered_gnid', function(request, response, next){
  // User search (from filter form)
  var user_search_query = buildUserSearchQuery(request.body.usersearch);
 
  // Search data
  var search_query = buildSearchQuery(request.body.tablesearch);
  
  var query = `SELECT annotation.gnId from annotation, expression
    WHERE annotation.gnId = expression.gnId ${search_query} ${user_search_query}
    ORDER BY ${request.body.tissue} DESC LIMIT 40`
  database.query(query, function(error, data){
    var gnIds = [];
    data.forEach(function(row){
      gnIds.push('"'+row.gnId+'"')
    });
    var query = `
      SELECT gnId,tissue,median FROM expression_by_tissue 
      WHERE gnId in (${gnIds})`;
    
    var output = [];
    database.query(query, function(error, data){
      if(typeof data != "undefined") {
        data.forEach(function(row){
          output.push({ 
            'gnId'  : row.gnId,
            'tissue' : row.tissue,
            'median' : row.median
          });
        });
      }
      response.json(output);
    });
  });
});

/* GET expression data for geneIDs => bar plot and scatter for co expression */
router.post('/get_expression_for_gnids', function(request, response, next){
  var gnIdsX = request.body.geneIdsX.replace(/[\s,]+/g,'","');
  var gnIdX = gnIdsX.split(/"/)[0]
  var query = `SELECT e.gnId, a.gnName, a.gnSimpleBiotype, e.tissue, e.median
    FROM expression_by_tissue AS e, annotation AS a
    WHERE e.gnId = a.gnId
    AND e.gnId in ("${gnIdsX}")`;
  var outputX = [];
  var outputY = [];
  database.query(query, function(error, data){
    data.forEach(function(row){
      outputX.push({ 
        'gnId'  : row.gnId,
        'gnName' : row.gnName,
        'gnSimpleBiotype' : row.gnSimpleBiotype,
        'tissue' : row.tissue,
        'median' : row.median
      });
    });
    var gnIdsY = request.body.geneIdsY.replace(/[\s,]+/g,'","');
    var query = `SELECT e.gnId, a.gnName, a.gnSimpleBiotype, e.tissue, e.median
      FROM expression_by_tissue AS e, annotation AS a
      WHERE e.gnId = a.gnId
      AND e.gnId in ("${gnIdsY}")`;
    database.query(query, function(error, data){
      data.forEach(function(row){
        outputY.push({ 
          'gnId'  : row.gnId,
          'gnName' : row.gnName,
          'gnSimpleBiotype' : row.gnSimpleBiotype,
          'tissue' : row.tissue,
          'median' : row.median
        });
      });
      var output = {
        'X' : outputX,
        'Y' : outputY,
        'gnIdX' : gnIdX,
        'gnIdY' : gnIdsY,
        'gd' : request.body.graphDiv
      };
      response.json(output);
    });
  });
});

/* GET expressions data for geneIDs => box plot (for co expression) */
router.post('/get_expressions_for_gnids_boxplot', function(request, response, next){
  var gnIds = request.body.geneIds.replace(/[\s,]+/g,'","');
  var query = `SELECT e.gnId, a.gnName, a.gnSimpleBiotype, e.tissue, e.median, e.median_values
    FROM expression_by_tissue AS e, annotation AS a
    WHERE e.gnId = a.gnId
    AND e.gnId in ("${gnIds}")`;
  var output = []
  database.query(query, function(error, data){
    data.forEach(function(row){
      output.push({ 
        'gnId'  : row.gnId,
        'gnName' : row.gnName,
        'gnSimpleBiotype' : row.gnSimpleBiotype,
        'tissue' : row.tissue,
        'median' : row.median,
        'median_values' : row.median_values
      });
    });
    response.json(output); 
  });
});

/* Ortho HSA : GET expression data for geneIDs */
router.post('/get_expression_for_ortho', function(request, response, next){
  var gnIds = request.body.geneIds.split(/,|\s+/);
  var gnId_GGA = [];
  var gnId_HSA = [];
  var expr_GGA = {};
  var expr_HSA = {};
  var tissues = ["adip","blod","brai","coln","crbl","crtx","hert","hypt","ileu","kdny","livr",
                 "lung","lymp","mscB","ovry","pcrs","pitu","skin","spln","test","thyr","uter"]
  let tmp = gnIds.map(e => `"${e}"`).join(',')
  var query = `SELECT * FROM ortho_HSA WHERE gnId in (${tmp}) or gnId_HSA_GTEx in (${tmp})`;
  database.query(query, function(error, data){
    if(typeof data != "undefined") {
      data.forEach(function(row){
        gnId_GGA.push(row.gnId);
        gnId_HSA.push(row.gnId_HSA_GTEx);
      });
    }
    tmp =  gnId_GGA.map(e => `"${e}"`).join(',')
    var query = `SELECT * FROM ortho_expression_GGA WHERE gnId in (${tmp})`;
    database.query(query, function(error, data){
      if(typeof data != "undefined") {
        data.forEach(function(row){
          expr_GGA[row.gnId] = []
          tissues.forEach(function(t) {
            expr_GGA[row.gnId].push(row[t])
          });
        });
      }
      tmp =  gnId_HSA.map(e => `"${e}"`).join(',')
      var query = `SELECT * FROM ortho_expression_HSA WHERE gnId_HSA_GTEx in (${tmp})`;
      database.query(query, function(error, data){
        if(typeof data != "undefined") {
          data.forEach(function(row){
            expr_HSA[row.gnId_HSA_GTEx] = []
            tissues.forEach(function(t) {
              expr_HSA[row.gnId_HSA_GTEx].push(row[t])
            });
          });
        }
        // Build output object
        var output = {};
        output["tissues"] = tissues;
        gnIds.forEach(function(g){
          let index = gnId_GGA.indexOf(g);
          if(index == -1) { index = gnId_HSA.indexOf(g); }
          // gnId not found in ortho_HSA table
          if(index == -1) {
            output[g + "_GGA"]  = null;
            output[g + "_HSA"]  = null;
            output[g + "_GGAe"] = null;
            output[g + "_HSAe"] = null;
          }
          else {
            // If g not in output (possible if user provide gnId and is ortho HSA)
            if(Object.keys(output).every(k => !k.endsWith("_GGA") || output[k] !== gnId_GGA[index])) {
              output[g+"_GGA"] = gnId_GGA[index];
              output[g+"_HSA"] = gnId_HSA[index];
              if (gnId_GGA[index] in expr_GGA) {
                output[g+"_GGAe"] = expr_GGA[gnId_GGA[index]];
              }
              else {
                output[g+"_GGAe"] = null;
              }
              if (gnId_HSA[index] in expr_HSA) {
                output[g+"_HSAe"] = expr_HSA[gnId_HSA[index]].map(valeur => valeur * -1);
              }
              else {
                output[g+"_HSAe"] = null;
              }
            }
          }
        });
        output["gd"]   = request.body.graphDiv;
        response.json(output);
      });
    });
  });
});

/* GET linearRegression from array of array */
router.post('/get_linearRegression', function(request, response, next){
  // Build to array for compute correlation
  var a1 = []
  var a2 = []
  for(let i=0; i<request.body.aa.length; i++) {
    a1.push(request.body.aa[i][0])
    a2.push(request.body.aa[i][1])
  }
  // Linear regression
  var l = ss.linearRegressionLine(ss.linearRegression(request.body.aa));
  var output = []
  request.body.x.forEach(function(x1){
    //if(l(x1)>0) { output.push([x1, l(x1)]) }
    output.push([x1, l(x1)]) 
  });
  if(a1.length > 1 && a2.length >1) {
    response.json({'xy': output, 'corr': ss.sampleCorrelation(a1,a2).toFixed(2)});
  }
  else {
    response.json({'xy': output, 'corr': null});
  }
});

/* GET the 40 most correlated genes from one geneId of interest - coexpression */
router.post('/get_mostCorrGene', function(request, response, next){
  // GET available tissue 
  var query = `SELECT DISTINCT tissue FROM expression_by_tissue order by tissue`;
  var tissues = [];
  var tissuesFields = ""
  database.query(query, function(error, data){
    data.forEach(function(row){
      tissues.push(row.tissue);
      tissuesFields += row.tissue+","
    });
    tissuesFields = tissuesFields.slice(0, tissuesFields.length-1);
    var gnIdOfInterest = request.body.geneId
    var query = `SELECT e.gnId, a.gnName, a.gnSimpleBiotype, a.chr, a.start, a.end, ${tissuesFields}
      FROM expression AS e, annotation AS a
      WHERE e.gnId = a.gnId`;
    database.query(query, function(error, data){
      var curVal = []
      var maxCorr = []
      let tmp = data.filter(objet => objet.gnId === gnIdOfInterest)[0];
      if(typeof tmp != "undefined") {
        let gnIdOfInterestVal = tissuesFields.split(",").map(key => tmp[key]);
        data.forEach(function(row){
          //if(row.gnId != gnIdOfInterest) {
            curVal = tissuesFields.split(",").map(key => row[key]);
            let corr = ss.sampleCorrelation(gnIdOfInterestVal,curVal)
            if(!isNaN(corr)) {
              corr = parseFloat(corr.toFixed(3));
              if (maxCorr.length <= 50 || corr > maxCorr[maxCorr.length-1].corr) {
                maxCorr.unshift({
                  'gnId':row.gnId,
                  'gnName':row.gnName,
                  'gnSimpleBiotype':row.gnSimpleBiotype,
                  'chr':row.chr,
                  'start':row.start,
                  'end':row.end,
                  'corr':corr
                });
                maxCorr.sort((a, b) => b.corr - a.corr);
                if (maxCorr.length > 51) {
                  maxCorr.pop();
                }
              }
            }
         // }
        })
      }
      response.json(maxCorr);
    });
  });
});
  
/* GET header groups for annotation datatable */
router.get('/get_annotation_header_meta', function(request, response, next){
  var query = `SELECT * FROM annotation_header_meta order by ID`;
  var output = [];
  database.query(query, function(error, data){
    data.forEach(function(row){
      output.push({
        'meta' : row.meta,
        'description' : row.description,
        'fields' : row.fields,
        'orderby' : row.orderby,
        'ordertype' : row.ordertype,
        'visible' : row.visible,
        'searchable' : row.searchable
      });
    });
    response.json(output);
  });
});

/* GET tissues (short and long name and color */
router.get('/get_tissues_info', function(request, response, next){
  var query = `SELECT * FROM tissue`;
  var output = [];
  database.query(query, function(error, data){
    data.forEach(function(row){
      output.push({
        'shortName' : row.tissue_shortName,
        'name' : row.tissue_longName,
        'proximity' : row.proximity,
        'orthoHSA' : row.ortho_HSA,
        'embryonic' : row.embryonic,
        'fctClassif1' : row.fctClassif1,
        'comment' : row.comment,
        'color' : row.color,
        'nbSample' : row.nb_sample,
        'nbProject' : row.nb_project
      });
    });
    response.json(output);
  });
});

/* GET projects info */
router.get('/get_projects_info', function(request, response, next){
  var query = `SELECT * FROM project`;
  var output = [];
  database.query(query, function(error, data){
    data.forEach(function(row){
      output.push({
        'accession' : row.project_accession,
        'nbSample' : row.nb_sample,
        'nbTissues' : row.nb_tissues,
        'details' : row.details.replace(/;/g, ' '),
        'name' : row.project_name,
        'breed' : row.breed,
        'institution' : row.institution,
        'description' : row.description,
        'color' : row.color
      });
    });
    response.json(output);
  });
});

/* GET distinct annotation values for field(s) (autocomplete)
 * From one field or several fields (group) get distinct values
 * From one field => all distinct values ordered
 * From several fields (comma sep) => limit 20 and ordered */
router.get('/get_distinct_annotation_from_field', function(request, response, next){
  var search_ope   = request.query.ope;
  var search_query = request.query.query;
  var fields = decodeURIComponent(request.query.field).split(",")
  var output = []

  var query = "SELECT DISTINCT * FROM ("
  
  // Due to time process order only if one field required else no order
  // and limit 20
  var l = ""
  if(fields.length != 1) { l = "LIMIT 20" }

  for(var i=0; i<fields.length; i++) {
    let search_field = fields[i];
    if(i!=0) {  query += `UNION `}
    query += `
      ( SELECT ${search_field} AS res FROM annotation 
      WHERE ${search_field} 
      LIKE '`
    if(search_ope === 'LIKE' ) { query += '%' }
    query += `${search_query}%' ${l} )`;
  }
  query += `) t ORDER BY 1 ${l}`

  database.query(query, function(error, data){
    data.forEach(function(row){
      output.push({ 'res' : row.res })
    });
    response.json(output)
  });
});


/* GET distinct filtered annotation values for one field (graph => venn) */
router.post('/get_filtered_annotation_from_field_distinct', function(request, response, next){
  // User search (from filter form)
  var user_search_query = buildUserSearchQuery(request.body.usersearch);
 
  // Search data
  var search_query = buildSearchQuery(request.body.tablesearch);
  
  // Build the table join if needed
  var db_join = buildUserSearchJoin(request.body.usersearch);

  // Build the table list
  var db_table = buildUserSearchTable(request.body.usersearch);

  var query = `
    SELECT DISTINCT annotation.${request.body.field} FROM ${db_table}
    WHERE 1 ${search_query} ${user_search_query} ${db_join}`;

  var output = [];
  database.query(query, function(error, data){
    data.forEach(function(row){
      output.push({
        'val' : row[request.body.field] === null ? 'NA' : row[request.body.field]
      });
    });
    response.json(output);
  });
});


/* GET filtered annotation values for one field (graph => pie)
 * Group by field and order by count ASC 
 */
router.post('/get_filtered_annotation_from_field_group', function(request, response, next){
  // User search (from filter form)
  var user_search_query = buildUserSearchQuery(request.body.usersearch);
 
  // Search data
  var search_query = buildSearchQuery(request.body.tablesearch);
  
  // Build the table join if needed
  var db_join = buildUserSearchJoin(request.body.usersearch);

  // Build the table list
  var db_table = buildUserSearchTable(request.body.usersearch);

  var query = `
    SELECT annotation.${request.body.field}, COUNT(*) AS count FROM ${db_table}
    WHERE 1 ${search_query} ${user_search_query} ${db_join}
    GROUP BY annotation.${request.body.field}
    ORDER BY count DESC`;

  var output = [];
  database.query(query, function(error, data){
    data.forEach(function(row){
      output.push({
        'val'  : row[request.body.field] === null ? 'NA' : row[request.body.field],
        'count' : row.count
      });
    });
    response.json(output);
  });
});


/* GET filtered annotation values for field(s) (graph => boxplot and scatter plot AND copy buttons */
router.post('/get_filtered_annotation_from_fields', function(request, response, next){
  // User search (from filter form)
  var user_search_query = buildUserSearchQuery(request.body.usersearch);
 
  // Search data
  var search_query = buildSearchQuery(request.body.tablesearch);
  
  // Build the table join if needed
  var db_join = buildUserSearchJoin(request.body.usersearch);

  // Build the table list
  var db_table = buildUserSearchTable(request.body.usersearch);
  
  var query = `
    SELECT ${request.body.field} FROM ${db_table}
    WHERE 1 ${search_query} ${user_search_query} ${db_join}`;
  var output = [];
  database.query(query, function(error, data){
    data.forEach(function(row){
      let r = {}
      for(let i=0; i<request.body.field.length; i++) {
        let f = request.body.field[i].includes('.') ? request.body.field[i].split('.')[1] : request.body.field[i];
        if(row[f] !== null) {
          r[f] = row[f]
        }
      }
      output.push(r);
    });
    response.json(output);
  });
});


/* GET age/sex expression data for geneIDs by project => bar plot and coexpression plot */
router.post('/get_expression_for_gnids_by_project', function(request, response, next){
  var gnIdsX = request.body.geneIdsX.replace(/[\s,]+/g,'","');
  var gnIdX = gnIdsX.split(/"/)[0]
  var query = `SELECT e.gnId, a.gnName, a.gnSimpleBiotype, e.age, e.sex, e.conditions, e.val
    FROM expression_for_${request.body.project} AS e, annotation AS a
    WHERE e.gnId = a.gnId
    AND e.tissue = "${request.body.tissue}"
    AND e.gnId in ("${gnIdsX}")
    ORDER BY e.age`;
  var outputX = [];
  var outputY = [];
  database.query(query, function(error, data){
    data.forEach(function(row){
      outputX.push({ 
        'gnId'  : row.gnId,
        'gnName' : row.gnName,
        'gnSimpleBiotype' : row.gnSimpleBiotype,
        'age' : row.age.includes("#") ? row.age.split("#")[1] :  row.age,
        'sex' : row.sex,
        'conditions' : row.conditions,
        'val' : row.val
      });
    });
    var gnIdsY = request.body.geneIdsY.replace(/[\s,]+/g,'","');
    var query = `SELECT e.gnId, a.gnName, a.gnSimpleBiotype, e.age, e.sex, e.conditions, e.val
      FROM expression_for_${request.body.project} AS e, annotation AS a
      WHERE e.gnId = a.gnId
      AND e.tissue = "${request.body.tissue}"
      AND e.gnId in ("${gnIdsY}")
      ORDER BY e.age`;
    database.query(query, function(error, data){
      data.forEach(function(row){
        outputY.push({ 
          'gnId'  : row.gnId,
          'gnName' : row.gnName,
          'gnSimpleBiotype' : row.gnSimpleBiotype,
          'age' : row.age.includes("#") ? row.age.split("#")[1] :  row.age,
          'sex' : row.sex,
          'conditions' : row.conditions,
          'val' : row.val
        });
      });
      var output = {
        'X' : outputX,
        'Y' : outputY,
        'gnIdX' : gnIdX,
        'gnIdY' : gnIdsY,
        'gd' : request.body.graphDiv
      };
      response.json(output);
    });
  });
});

/* GET age/sex expressions data for geneIDs by project => box plot */
router.post('/get_expressions_for_gnids_boxplot_by_project', function(request, response, next){
  var gnIds = request.body.geneIds.replace(/[\s,]+/g,'","');
  var tissues = request.body.tissue.join('","')
  var orderby = ""
  if(request.body.orderby == 'sextissue')      { orderby = "e.sex, e.tissue" }
  else if(request.body.orderby == 'tissuesex') { orderby = "e.tissue, e.sex" }
  else if(request.body.orderby == 'agetissue') { orderby = "e.age, e.tissue" }
  else if(request.body.orderby == 'tissueage') { orderby = "e.tissue, e.age"
  }
  var query = `SELECT e.gnId, a.gnName, a.gnSimpleBiotype, e.tissue, e.age, e.sex, GROUP_CONCAT(e.val SEPARATOR ';') AS vals
    FROM expression_for_${request.body.project} AS e, annotation AS a
    WHERE e.gnId = a.gnId
    AND e.tissue in ("${tissues}")
    AND e.gnId in ("${gnIds}")
    GROUP BY e.gnId, a.gnName, a.gnSimpleBiotype, e.tissue, e.age, e.sex ORDER BY ${orderby}`;
  var output = []
  database.query(query, function(error, data){
    data.forEach(function(row){
      output.push({ 
        'gnId'  : row.gnId,
        'gnName' : row.gnName,
        'gnSimpleBiotype' : row.gnSimpleBiotype,
        'tissue' : row.tissue,
        'age' : row.age.includes("#") ? row.age.split("#")[1] :  row.age,
        'sex' : row.sex,
        'vals' : row.vals
      });
    });
    response.json(output);
  });
});


/* GET intra expression data for geneIDs for all projects => coexpression plot */
router.post('/get_expression_for_gnids_all_project', function(request, response, next){
  var gnIdsX = request.body.geneIdsX.replace(/[\s,]+/g,'","');
  var gnIdX = gnIdsX.split(/"/)[0]
  var query = `SELECT e.gnId, a.gnName, a.gnSimpleBiotype, e.project, e.vals
    FROM expression_by_project AS e, annotation AS a
    WHERE e.gnId = a.gnId
    AND e.tissue = "${request.body.tissue}"
    AND e.gnId in ("${gnIdsX}")`;
  var outputX = [];
  var outputY = [];
  database.query(query, function(error, data){
    data.forEach(function(row){
      outputX.push({ 
        'gnId'  : row.gnId,
        'gnName' : row.gnName,
        'gnSimpleBiotype' : row.gnSimpleBiotype,
        'project' : row.project,
        'vals' : row.vals
      });
    });
    var gnIdsY = request.body.geneIdsY.replace(/[\s,]+/g,'","');
    var query = `SELECT e.gnId, a.gnName, a.gnSimpleBiotype, e.project, e.vals
      FROM expression_by_project AS e, annotation AS a
      WHERE e.gnId = a.gnId
      AND e.tissue = "${request.body.tissue}"
      AND e.gnId in ("${gnIdsY}")`;
    database.query(query, function(error, data){
      data.forEach(function(row){
        outputY.push({ 
          'gnId'  : row.gnId,
          'gnName' : row.gnName,
          'gnSimpleBiotype' : row.gnSimpleBiotype,
          'project' : row.project,
          'vals' : row.vals
        });
      });
      var output = {
        'X' : outputX,
        'Y' : outputY,
        'gnIdX' : gnIdX,
        'gnIdY' : gnIdsY,
        'gd' : request.body.graphDiv
      };
      response.json(output);
    });
  });
});

/* GET intra expressions data for geneIDs for all projects => box plot */
router.post('/get_expressions_for_gnids_boxplot_all_project', function(request, response, next){
  var gnIds = request.body.geneIds.replace(/[\s,]+/g,'","');
  var tissues = request.body.tissue.join('","')
  var query = `SELECT e.gnId, a.gnName, a.gnSimpleBiotype, e.project, e.tissue, e.vals
    FROM expression_by_project AS e, annotation AS a
    WHERE e.gnId = a.gnId
    AND e.tissue in ("${tissues}")
    AND e.gnId in ("${gnIds}")`;
  var output = []
  database.query(query, function(error, data){
    data.forEach(function(row){
      output.push({ 
        'gnId'  : row.gnId,
        'gnName' : row.gnName,
        'gnSimpleBiotype' : row.gnSimpleBiotype,
        'tissue' : row.tissue,
        'project' : row.project,
        'vals' : row.vals
      });
    });
    response.json(output);
  });
});


// Build part of search query from filter form
// us must contain an array of json object like :
// { f: 'gnId', d: 'annotation', o: '~', v: '100859336' }
// f = field(s) ... for group comma separated
// d = database 
// o = operator
// v = value(s) ... for multiple values comma separated
const buildUserSearchQuery = (us) => {
  res = ""
  for(let i=0; i<us.length; i++) {
    res += ' AND ('
    // field(s) ? (comma separated) => group
    let fields = decodeURIComponent(us[i].f).split(",")
    for(let j=0; j<fields.length; j++) {
      let field = us[i].d+".`"+fields[j]+"`";
      // operator
      let ope = decodeURIComponent(us[i].o)
      if(ope === "~") { ope = ' LIKE' }
      else if(ope === "!~")  { ope = ' NOT LIKE' }
      // value(s) ? (comma separated)
      let values = decodeURIComponent(us[i].v).split(",")
      for(let k=0; k<values.length; k++) {
        let val;
        if(ope === ' LIKE' || ope === ' NOT LIKE') {
          val = ' \'%' + values[k] + '%\''
        }
        else { val = ' \'' + values[k] + '\'' }
        res += field + ope + val
        if (k != values.length-1) {
          res += ' OR ' 
        }
      }
      if (j != fields.length-1) {
        res += ' OR ' 
      }
    }
    res += ')'
  }
  return res;
}

// Build part of search query from search datatable field
const buildSearchQuery = (s) => {
  res = ""
  if(s != "") {
    res = `
      AND (chr     LIKE '%${s}%'
      OR   gnId    LIKE '%${s}%'
      OR   gnName  LIKE '%${s}%' 
      OR   strand  LIKE '%${s}%'
      OR   source  LIKE '%${s}%'
      OR   version LIKE '%${s}%'
      OR   gnSimpleBiotype LIKE '%${s}%')`;
  }
  return res
}

// Make a join if needed (if "expression" table used in usersearch)
const buildUserSearchJoin = (us) => {
  res = ""
  if(us.filter(obj => obj.d === 'expression').length > 0) {
    res = ` AND annotation.gnId = expression.gnId `;
  }
  return res
}

// Make the list of the table needed for request (from usersearch)
const buildUserSearchTable = (us) => {
  res = Array.from(new Set(us.map(obj => obj.d)));
  if (! res.includes("annotation")) {
    res.push("annotation")
  }
  return res.join(",")
}

/* GET data for annotation datatable */
router.post('/get_annotation', function(request, response, next){
  var draw = request.body.draw;
  var start = request.body.start;
  var length = request.body.length;
  var order_data = request.body.order;

  if(order_data.length == 0) {
    var column_name = request.body.vieworderby;
    var column_sort_order = request.body.viewordertype;
  }
  else {
    var column_index = request.body.order[0]['column'];
    var column_name = request.body.columns[column_index]['data'];
    var column_sort_order = request.body.order[0]['dir'];
  }

  // User search (from filter form)
  var user_search_query = buildUserSearchQuery(request.body.usersearch);

  // Search data
  var search_query = buildSearchQuery(request.body.search['value']);

  // Build the table join if needed
  var db_join = buildUserSearchJoin(request.body.usersearch);

  // Build the table list
  var db_table = buildUserSearchTable(request.body.usersearch);

  //Total number of records without filtering
  database.query("SELECT COUNT(*) AS Total FROM annotation", function(error, data){
    var total_records = data[0].Total;

    //Total number of records with filtering
    var query = `
      SELECT COUNT(*) AS Total FROM ${db_table}
      WHERE 1 ${search_query} ${user_search_query} ${db_join}`;
    database.query(query, function(error, data){
      var total_records_with_filter = data[0].Total;
      
      var query = `
        SELECT annotation.* FROM ${db_table}
        WHERE 1  ${search_query} ${user_search_query} ${db_join}
        ORDER BY ${column_name} ${column_sort_order} 
        LIMIT    ${start}, ${length}`;
      var data_arr = [];
      database.query(query, function(error, data){
        data.forEach(function(row){
          /*data_arr.push({
            'chr' : row.chr,
            'start' : row.start,
            'end' : row.end,
            'strand' : row.strand,
            'gnId' : row.gnId,
            'tpId' : row.tpId,
            'version' : row.version,
            'source' : row.source,
            'gnBiotype' : row.gnBiotype,
            'gnSimpleBiotype' : row.gnSimpleBiotype,
            'tpBiotype' : row.tpBiotype,
            'gnName' : row.gnName,
            'gnNameDesc' : row.gnNameDesc,
            'gnNameGga' : row.gnNameGga,
            'hgncGga' : row.hgncGga,
            'wikigeneGga' : row.wikigeneGga,
            'hgncHsa1to1' : row.hgncHsa1to1,
            'wikigeneHsa1to1' : row.wikigeneHsa1to1,
            'hgncMmu1to1' : row.hgncMmu1to1,
            'wikigeneMmu1to1' : row.wikigeneMmu1to1,
            'gnNameDescGga' : row.gnNameDescGga,
            'gnNameDescHsa1to1' : row.gnNameDescHsa1to1,
            'gnNameDescMmu1to1' : row.gnNameDescMmu1to1,
            'gnNameDescHsaXtoMany' : row.gnNameDescHsaXtoMany,
            'gnNameDescMmuXtoMany' : row.gnNameDescMmuXtoMany,
            'orthHsaType' : row.orthHsaType,
            'orthMmuType' : row.orthMmuType,
            'orthHsaGnId' : row.orthHsaGnId,
            'orthMmuGnId' : row.orthMmuGnId,
            'hgncHsaXtoMany' : row.hgncHsaXtoMany,
            'hgncMmuXtoMany' : row.hgncMmuXtoMany,
            'wikigeneHsaXtoMany' : row.wikigeneHsaXtoMany,
            'wikigeneMmuXtoMany' : row.wikigeneMmuXtoMany,
            'exNb' : row.exNb,
            'exSz' : row.exSz,
            'inSz' : row.inSz,
            'tpSz' : row.tpSz,
            'nbTp' : row.nbTp,
            'gnSz' : row.gnSz,
            'feelLncPcgClassName' : row.feelLncPcgClassName,
            'feelLncPcgClassType' : row.feelLncPcgClassType,
            'feelLncPcgGnId' : row.feelLncPcgGnId,
            'feelLncPcgGnName' : row.feelLncPcgGnName,
            'feelLncPcgGnDist' : row.feelLncPcgGnDist,
            'feelPcgPcgClassName' : row.feelPcgPcgClassName,
            'feelPcgPcgClassType' : row.feelPcgPcgClassType,
            'feelPcgPcgGnId' : row.feelPcgPcgGnId,
            'feelPcgPcgGnName' : row.feelPcgPcgGnName,
            'feelPcgPcgGnDist' : row.feelPcgPcgGnDist,
            'feelMirLncClassName' : row.feelMirLncClassName,
            'feelMirLncGnId' : row.feelMirLncGnId,
            'feelMirLncGnName' : row.feelMirLncGnName,
            'feelMirLncGnDist' : row.feelMirLncGnDist,
            'feelSmlLncClassName' : row.feelSmlLncClassName,
            'feelSmlLncGnId' : row.feelSmlLncGnId,
            'feelSmlLncGnName' : row.feelSmlLncGnName,
            'feelSmlLncGnDist' : row.feelSmlLncGnDist,
            'feelLncPcgClassNameByTp' : row.feelLncPcgClassNameByTp,
            'feelLncPcgGnIdByTp' : row.feelLncPcgGnIdByTp,
            'feelLncPcgGnNameByTp' : row.feelLncPcgGnNameByTp,
            'feelLncPcgGnDistByTp' : row.feelLncPcgGnDistByTp,
            'feelPcgPcgClassNameByTp' : row.feelPcgPcgClassNameByTp,
            'feelPcgPcgGnIdByTp' : row.feelPcgPcgGnIdByTp,
            'feelPcgPcgGnNameByTp' : row.feelPcgPcgGnNameByTp,
            'feelPcgPcgGnDistByTp' : row.feelPcgPcgGnDistByTp,
            'feelMirLncClassNameByTp' : row.feelMirLncClassNameByTp,
            'feelMirLncGnIdByTp' : row.feelMirLncGnIdByTp,
            'feelMirLncGnNameByTp' : row.feelMirLncGnNameByTp,
            'feelMirLncGnDistByTp' : row.feelMirLncGnDistByTp,
            'feelSmlLncClassNameByTp' : row.feelSmlLncClassNameByTp,
            'feelSmlLncGnIdByTp' : row.feelSmlLncGnIdByTp,
            'feelSmlLncGnNameByTp' : row.feelSmlLncGnNameByTp,
            'feelSmlLncGnDistByTp' : row.feelSmlLncGnDistByTp,
            'go_gga_BP' : row.go_gga_BP,
            'go_gga_MF' : row.go_gga_MF,
            'go_gga_CC' : row.go_gga_CC,
            'go_Hsa_BP' : row.go_Hsa_BP,
            'go_Hsa_MF' : row.go_Hsa_MF,
            'go_Hsa_CC' : row.go_Hsa_CC,
            'go_Mmu_BP' : row.go_Mmu_BP,
            'go_Mmu_MF' : row.go_Mmu_MF,
            'go_Mmu_CC' : row.go_Mmu_CC,
            'MGIinputType' : row.MGIinputType,
            'MGI_MPid' : row.MGI_MPid,
            'MGIterms' : row.MGIterms,
            'expr_tissues_RpRm' : row.expr_tissues_RpRm,
            'expr_values_RpRm' : row.expr_values_RpRm,
            'Davis_tauValue' : row.Davis_tauValue,
            'Davis_isTS' : row.Davis_isTS,
            'Davis_nbTissueWithExpr' : row.Davis_nbTissueWithExpr,
            'Davis_FCtop1_top2tissue' : row.Davis_FCtop1_top2tissue,
            'expr_tissues_Davis' : row.expr_tissues_Davis,
            'expr_values_Davis' : row.expr_values_Davis,
            'expr_tissues_FrAg2' : row.expr_tissues_FrAg2,
            'expr_values_FrAg2' : row.expr_values_FrAg2,
            'Rosl_tauValue' : row.Rosl_tauValue,
            'Rosl_isTS' : row.Rosl_isTS,
            'Rosl_nbTissueWithExpr' : row.Rosl_nbTissueWithExpr,
            'Rosl_FCtop1_top2tissue' : row.Rosl_FCtop1_top2tissue,
            'expr_tissues_Rosl' : row.expr_tissues_Rosl,
            'expr_values_Rosl' : row.expr_values_Rosl,
            'expr_tissues_RFh' : row.expr_tissues_RFh,
            'expr_values_tissues_RJFh' : row.expr_values_tissues_RJFh,
            'isInEns' : row.isInEns,
            'isInNCBI' : row.isInNCBI,
            'isInALDB' : row.isInALDB,
            'isInNONCODE' : row.isInNONCODE,
            'isInTAGADA' : row.isInTAGADA
          });*/
          // Replace null by NA for display in datatable
          Object.keys(row).forEach(function(key) {
            if (row[key] === null) {
              row[key] = "NA";
            }
          });
          data_arr.push({
            'gnId' : row.gnId,
            'gnName' : row.gnName,
            'gnName_full' : row.gnName_full,
            'chr' : row.chr,
            'start' : row.start,
            'end' : row.end,
            'strand' : row.strand,
            'source' : row.source,
            'version' : row.version,
            'gnBiotype' : row.gnBiotype,
            'gnSimpleBiotype' : row.gnSimpleBiotype,
            'gnId_eqEnsemblBiomart' : row.gnId_eqEnsemblBiomart,
            'gnName_eqEnsemblBiomart' : row.gnName_eqEnsemblBiomart,
            'gnId_eqNcbiBiomart' : row.gnId_eqNcbiBiomart,
            'gnName_eqNcbiBiomart' : row.gnName_eqNcbiBiomart,
            'gnId_eqEnsemblOvlp' : row.gnId_eqEnsemblOvlp,
            'gnName_eqEnsemblOvlp' : row.gnName_eqEnsemblOvlp,
            'gnId_eqGalgal5' : row.gnId_eqGalgal5,
            'gnId_eqGRCg6a' : row.gnId_eqGRCg6a,
            'gnId_eqGRCg7w' : row.gnId_eqGRCg7w,
            'gnSz' : row.gnSz,
            'tpNb' : row.tpNb,
            'tpId' : row.tpId,
            'tpSz' : row.tpSz,
            'tpSz_stats' : row.tpSz_stats,
            'tpBiotype' : row.tpBiotype,
            'tpSimpleBiotype' : row.tpSimpleBiotype,
            'exNb' : row.exNb,
            'exNb_stats' : row.exNb_stats,
            'exId' : row.exId,
            'exSz' : row.exSz,
            'exSz_stats' : row.exSz_stats,
            'inNb' : row.inNb,
            'inNb_stats' : row.inNb_stats,
            'inSz' : row.inSz,
            'inSz_stats' : row.inSz_stats,
            'ortho_type_hsa' : row.ortho_type_hsa,
            'ortho_gnIdEnsembl_hsa' : row.ortho_gnIdEnsembl_hsa,
            'ortho_gnNameEnsembl_hsa' : row.ortho_gnNameEnsembl_hsa,
            'ortho_gnIdNcbi_hsa' : row.ortho_gnIdNcbi_hsa,
            'ortho_gnNameNcbi_hsa' : row.ortho_gnNameNcbi_hsa,
            'ortho_gnNameFullNcbi_hsa' : row.ortho_gnNameFullNcbi_hsa,
            'ortho_type_mmu' : row.ortho_type_mmu,
            'ortho_gnIdEnsembl_mmu' : row.ortho_gnIdEnsembl_mmu,
            'ortho_gnNameEnsembl_mmu' : row.ortho_gnNameEnsembl_mmu,
            'ortho_gnIdNcbi_mmu' : row.ortho_gnIdNcbi_mmu,
            'ortho_gnNameNcbi_mmu' : row.ortho_gnNameNcbi_mmu,
            'ortho_gnNameFullNcbi_mmu' : row.ortho_gnNameFullNcbi_mmu,
            'FEELnc_classNameByTp_LncPcg' : row.FEELnc_classNameByTp_LncPcg,
            'FEELnc_gnIdByTp_LncPcg' : row.FEELnc_gnIdByTp_LncPcg,
            'FEELnc_gnNameByTp_LncPcg' : row.FEELnc_gnNameByTp_LncPcg,
            'FEELnc_gnDistByTp_LncPcg' : row.FEELnc_gnDistByTp_LncPcg,
            'FEELnc_className_LncPcg' : row.FEELnc_className_LncPcg,
            'FEELnc_classType_LncPcg' : row.FEELnc_classType_LncPcg,
            'FEELnc_gnId_LncPcg' : row.FEELnc_gnId_LncPcg,
            'FEELnc_gnName_LncPcg' : row.FEELnc_gnName_LncPcg,
            'FEELnc_gnDist_LncPcg' : row.FEELnc_gnDist_LncPcg,
            'FEELnc_coExpr_LncPcg' : row.FEELnc_coExpr_LncPcg,
            'FEELnc_pval_LncPcg' : row.FEELnc_pval_LncPcg,
            'FEELnc_isTsBoth_LncPcg' : row.FEELnc_isTsBoth_LncPcg,
            'FEELnc_tsSameTissues_LncPcg' : row.FEELnc_tsSameTissues_LncPcg,
            'FEELnc_classNameByTp_PcgPcg' : row.FEELnc_classNameByTp_PcgPcg,
            'FEELnc_gnIdByTp_PcgPcg' : row.FEELnc_gnIdByTp_PcgPcg,
            'FEELnc_gnNameByTp_PcgPcg' : row.FEELnc_gnNameByTp_PcgPcg,
            'FEELnc_gnDistByTp_PcgPcg' : row.FEELnc_gnDistByTp_PcgPcg,
            'FEELnc_className_PcgPcg' : row.FEELnc_className_PcgPcg,
            'FEELnc_classType_PcgPcg' : row.FEELnc_classType_PcgPcg,
            'FEELnc_gnId_PcgPcg' : row.FEELnc_gnId_PcgPcg,
            'FEELnc_gnName_PcgPcg' : row.FEELnc_gnName_PcgPcg,
            'FEELnc_gnDist_PcgPcg' : row.FEELnc_gnDist_PcgPcg,
            'FEELnc_coExpr_PcgPcg' : row.FEELnc_coExpr_PcgPcg,
            'FEELnc_pval_PcgPcg' : row.FEELnc_pval_PcgPcg,
            'FEELnc_isTsBoth_PcgPcg' : row.FEELnc_isTsBoth_PcgPcg,
            'FEELnc_tsSameTissues_PcgPcg' : row.FEELnc_tsSameTissues_PcgPcg,
            'FEELnc_classNameByTp_LncLnc' : row.FEELnc_classNameByTp_LncLnc,
            'FEELnc_gnIdByTp_LncLnc' : row.FEELnc_gnIdByTp_LncLnc,
            'FEELnc_gnNameByTp_LncLnc' : row.FEELnc_gnNameByTp_LncLnc,
            'FEELnc_gnDistByTp_LncLnc' : row.FEELnc_gnDistByTp_LncLnc,
            'FEELnc_className_LncLnc' : row.FEELnc_className_LncLnc,
            'FEELnc_classType_LncLnc' : row.FEELnc_classType_LncLnc,
            'FEELnc_gnId_LncLnc' : row.FEELnc_gnId_LncLnc,
            'FEELnc_gnName_LncLnc' : row.FEELnc_gnName_LncLnc,
            'FEELnc_gnDist_LncLnc' : row.FEELnc_gnDist_LncLnc,
            'FEELnc_coExpr_LncLnc' : row.FEELnc_coExpr_LncLnc,
            'FEELnc_pval_LncLnc' : row.FEELnc_pval_LncLnc,
            'FEELnc_isTsBoth_LncLnc' : row.FEELnc_isTsBoth_LncLnc,
            'FEELnc_tsSameTissues_LncLnc' : row.FEELnc_tsSameTissues_LncLnc,
            'FEELnc_classNameByTp_MirLnc' : row.FEELnc_classNameByTp_MirLnc,
            'FEELnc_gnIdByTp_MirLnc' : row.FEELnc_gnIdByTp_MirLnc,
            'FEELnc_gnNameByTp_MirLnc' : row.FEELnc_gnNameByTp_MirLnc,
            'FEELnc_gnDistByTp_MirLnc' : row.FEELnc_gnDistByTp_MirLnc,
            'FEELnc_className_MirLnc' : row.FEELnc_className_MirLnc,
            'FEELnc_classType_MirLnc' : row.FEELnc_classType_MirLnc,
            'FEELnc_gnId_MirLnc' : row.FEELnc_gnId_MirLnc,
            'FEELnc_gnName_MirLnc' : row.FEELnc_gnName_MirLnc,
            'FEELnc_gnDist_MirLnc' : row.FEELnc_gnDist_MirLnc,
            'FEELnc_classNameByTp_MirPcg' : row.FEELnc_classNameByTp_MirPcg,
            'FEELnc_gnIdByTp_MirPcg' : row.FEELnc_gnIdByTp_MirPcg,
            'FEELnc_gnNameByTp_MirPcg' : row.FEELnc_gnNameByTp_MirPcg,
            'FEELnc_gnDistByTp_MirPcg' : row.FEELnc_gnDistByTp_MirPcg,
            'FEELnc_className_MirPcg' : row.FEELnc_className_MirPcg,
            'FEELnc_classType_MirPcg' : row.FEELnc_classType_MirPcg,
            'FEELnc_gnId_MirPcg' : row.FEELnc_gnId_MirPcg,
            'FEELnc_gnName_MirPcg' : row.FEELnc_gnName_MirPcg,
            'FEELnc_gnDist_MirPcg' : row.FEELnc_gnDist_MirPcg,
            'FEELnc_classNameByTp_SmlLnc' : row.FEELnc_classNameByTp_SmlLnc,
            'FEELnc_gnIdByTp_SmlLnc' : row.FEELnc_gnIdByTp_SmlLnc,
            'FEELnc_gnNameByTp_SmlLnc' : row.FEELnc_gnNameByTp_SmlLnc,
            'FEELnc_gnDistByTp_SmlLnc' : row.FEELnc_gnDistByTp_SmlLnc,
            'FEELnc_className_SmlLnc' : row.FEELnc_className_SmlLnc,
            'FEELnc_classType_SmlLnc' : row.FEELnc_classType_SmlLnc,
            'FEELnc_gnId_SmlLnc' : row.FEELnc_gnId_SmlLnc,
            'FEELnc_gnName_SmlLnc' : row.FEELnc_gnName_SmlLnc,
            'FEELnc_gnDist_SmlLnc' : row.FEELnc_gnDist_SmlLnc,
            'FEELnc_classNameByTp_SmlPcg' : row.FEELnc_classNameByTp_SmlPcg,
            'FEELnc_gnIdByTp_SmlPcg' : row.FEELnc_gnIdByTp_SmlPcg,
            'FEELnc_gnNameByTp_SmlPcg' : row.FEELnc_gnNameByTp_SmlPcg,
            'FEELnc_gnDistByTp_SmlPcg' : row.FEELnc_gnDistByTp_SmlPcg,
            'FEELnc_className_SmlPcg' : row.FEELnc_className_SmlPcg,
            'FEELnc_classType_SmlPcg' : row.FEELnc_classType_SmlPcg,
            'FEELnc_gnId_SmlPcg' : row.FEELnc_gnId_SmlPcg,
            'FEELnc_gnName_SmlPcg' : row.FEELnc_gnName_SmlPcg,
            'FEELnc_gnDist_SmlPcg' : row.FEELnc_gnDist_SmlPcg,
            'expr_tissues' : row.expr_tissues,
            'expr_values' : row.expr_values,
            'expr_isExpressed' : row.expr_isExpressed,
            'expr_nbTissueSup01Tpm' : row.expr_nbTissueSup01Tpm,
            'expr_nbTissueSup1Tpm' : row.expr_nbTissueSup1Tpm,
            'expr_tau' : row.expr_tau,
            'expr_isTS' : row.expr_isTS,
            'expr_tauLog10' : row.expr_tauLog10,
            'expr_isTsLog10' : row.expr_isTsLog10,
            'expr_valueTop1' : row.expr_valueTop1,
            'expr_tissueTop1' : row.expr_tissueTop1,
            'expr_valueTop2' : row.expr_valueTop2,
            'expr_tissueTop2' : row.expr_tissueTop2,
            'expr_fcTop1Top2' : row.expr_fcTop1Top2,
            'expr_fcTop1Top2Log10' : row.expr_fcTop1Top2Log10,
            'expr_nbTissueUnexpressed' : row.expr_nbTissueUnexpressed,
            'expr_category' : row.expr_category,
            'expr_tissueNbFirstBreak' : row.expr_tissueNbFirstBreak,
            'expr_tissueFirstBreak' : row.expr_tissueFirstBreak,
            'expr_fcFirstBreak_lowToTop' : row.expr_fcFirstBreak_lowToTop,
            'expr_fcFirstBreak_topToTop' : row.expr_fcFirstBreak_topToTop,
            'expr_fcFirstBreak_medianGroup' : row.expr_fcFirstBreak_medianGroup,
            'expr_nbBreaks' : row.expr_nbBreaks,
            'expr_fcBreaks' : row.expr_fcBreaks,
            'expr_tissueBreaks' : row.expr_tissueBreaks,
            'expr_tissueNbBreaks' : row.expr_tissueNbBreaks,
            'fctAnnot_phenoDesc_gga' : row.fctAnnot_phenoDesc_gga,
            'fctAnnot_GOterms_BP_gga' : row.fctAnnot_GOterms_BP_gga,
            'fctAnnot_GOterms_CC_gga' : row.fctAnnot_GOterms_CC_gga,
            'fctAnnot_GOterms_MF_gga' : row.fctAnnot_GOterms_MF_gga,
            'fctAnnot_phenoDesc_hsa' : row.fctAnnot_phenoDesc_hsa,
            'fctAnnot_GOterms_BP_hsa' : row.fctAnnot_GOterms_BP_hsa,
            'fctAnnot_GOterms_CC_hsa' : row.fctAnnot_GOterms_CC_hsa,
            'fctAnnot_GOterms_MF_hsa' : row.fctAnnot_GOterms_MF_hsa,
            'fctAnnot_phenoDesc_mmu' : row.fctAnnot_phenoDesc_mmu,
            'fctAnnot_GOterms_BP_mmu' : row.fctAnnot_GOterms_BP_mmu,
            'fctAnnot_GOterms_CC_mmu' : row.fctAnnot_GOterms_CC_mmu,
            'fctAnnot_GOterms_MF_mmu' : row.fctAnnot_GOterms_MF_mmu,
            'fctAnnot_MGIterms_mmu' : row.fctAnnot_MGIterms_mmu,
            'fctAnnot_MGIid_mmu' : row.fctAnnot_MGIid_mmu,
            'locusNb_RefSeq' : row.locusNb_RefSeq,
            'locusNb_Ensembl' : row.locusNb_Ensembl,
            'locusNb_FrAgEncode' : row.locusNb_FrAgEncode,
            'locusNb_DavisUniv' : row.locusNb_DavisUniv,
            'locusNb_Inra' : row.locusNb_Inra,
            'locusNb_Noncode' : row.locusNb_Noncode,
            'locusIn_NbDt' : row.locusIn_NbDt
          });
        });

        var output = {
         'draw' : draw,
         'iTotalRecords' : total_records,
         'iTotalDisplayRecords' : total_records_with_filter,
         'aaData' : data_arr
        };

        response.json(output);
      });
    });
  });
});

/* Export (filtered) annotation table */
router.post('/export_annotation2csv',function(request, response, next){
  // User search (from filter form)
  var user_search_query = buildUserSearchQuery(request.body.usersearch);

  // Search data
  var search_query = buildSearchQuery(request.body.tablesearch);

  // Build the table join if needed
  var db_join = buildUserSearchJoin(request.body.usersearch);

  // Build the table list
  var db_table = buildUserSearchTable(request.body.usersearch);
 
  // Fields
  var f = request.body.fields.map(el => 'annotation.' + el)
  
  var query = `
    SELECT ${f} from ${db_table}
    WHERE 1 ${search_query} ${user_search_query} ${db_join}`
 
  database.query(query, function (err, ann) {
    if (err) throw err;
         
    const jsonoutput = JSON.parse(JSON.stringify(ann));
 
    // -> Convert JSON to CSV data
    const csvFields = request.body.fields;
    const json2csvParser = new Json2csvParser({ csvFields, delimiter: ';'});
    const csv = json2csvParser.parse(jsonoutput);
    response.setHeader('Content-disposition', 'attachment; filename=data.csv');
    response.set('Content-Type', 'text/csv');
    response.status(200).send(csv);
  });
});

/* Export (filtered) expression table */
router.post('/export_expression2csv',function(request, response, next){
  // User search (from filter form)
  var user_search_query = buildUserSearchQuery(request.body.usersearch);

  // Search data
  var search_query = buildSearchQuery(request.body.tablesearch);

  // Build the table join if needed
  var db_join = buildUserSearchJoin(request.body.usersearch);

  // Build the table list
  var db_table = buildUserSearchTable(request.body.usersearch);
 
  var query = `SELECT annotation.gnId from ${db_table}
    WHERE 1 ${search_query} ${user_search_query} ${db_join}`

  database.query(query, function(error, data){
    var gnIds = [];
    data.forEach(function(row){
      gnIds.push('"'+row.gnId+'"')
    });
    var query = `
      SELECT * FROM expression
      WHERE gnId in (${gnIds})`;
    
    database.query(query, function(err, expr){
      if (err) throw err;
      const jsonoutput = JSON.parse(JSON.stringify(expr));

      // -> Convert JSON to CSV data
      const csvFields = [];
      const json2csvParser = new Json2csvParser({ csvFields, delimiter: ';'});
      const csv = json2csvParser.parse(jsonoutput);
      response.setHeader('Content-disposition', 'attachment; filename=data.csv');
      response.set('Content-Type', 'text/csv');
      response.status(200).send(csv);
    });
  });
});


module.exports = router;