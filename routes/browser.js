var express = require('express');
var router = express.Router();

/* GET browser page. */
router.get('/browser', function(req, res, next) {
  if (req.query.region) {
    const regionValue = req.query.region;
    res.render('browser', { pagename:'Browser', region: regionValue });
  }
  else {
    res.render('browser', { pagename: 'Browser', region: '1:1,913,510-2,116,565' });
  }
});

module.exports = router;
