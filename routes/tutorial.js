var express = require('express');
var router = express.Router();

/* GET tutorial page. */
router.get('/tutorial', function(req, res, next) {
  res.render('tutorial', { pagename:'Tutorial' });
});

module.exports = router;