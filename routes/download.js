var express = require('express');
var router = express.Router();

/* GET dowload page. */
router.get('/download', function(req, res, next) {
  res.render('download', { pagename:'Download' });
});

module.exports = router;
