#!/usr/bin/perl

use strict;
use warnings;

if ($#ARGV != 0) {
	print "One input tab separated file required:\n";
	exit();
}

# index of corresponding col in input file
# Ex colonne relative au brain => indices
my %h_tissu_mean = ();
my %h_tissu_sd = ();
my %h_tissu_nbdataset = ();

open(INPUT, "$ARGV[0]") || die "Error enable to open chr len file: $ARGV[0]";
my $linenb = 0;
while (my $line = <INPUT>) {
	$linenb++;
	chomp $line;
	my @f = split('\t', $line);
	if($linenb == 1) {
		for (my $col=0; $col<=$#f; $col++) {
			if($f[$col]=~/(.*)\_mean$/) {
				$h_tissu_mean{$1} = $col;
			}
			if($f[$col]=~/(.*)\_sd$/) {
				$h_tissu_sd{$1} = $col;
			}

			if($f[$col]=~/(.*)\_nbDataset$/) {
				$h_tissu_nbdataset{$1} = $col;
			}

		}
	}
	else {
		foreach my $k (keys(%h_tissu_mean)) {
			print "$linenb\t$f[0]\t$f[1]\t$k\t";
			$f[$h_tissu_mean{$k}] =~ s/^NA$/\\N/;
			print "$f[$h_tissu_mean{$k}]\t";
			$f[$h_tissu_sd{$k}] =~ s/^NA$/\\N/;
			print "$f[$h_tissu_sd{$k}]\t";
			$f[$h_tissu_nbdataset{$k}] =~ s/^NA$/\\N/;
			print "$f[$h_tissu_nbdataset{$k}]\n";
			$linenb++;
		}
	}
}
#foreach my $k (keys(%h_tissu)) {
#	print "$k => ".join("\t", @{$h_tissu{$k}})."\n";
#}
close INPUT;

