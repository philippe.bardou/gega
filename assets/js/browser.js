const options = {
  reference: {
    "id": "GRCg7b",
    "name": "Chicken (GRCg7b)",
    "fastaURL": "/igvbrowser/ASSEMBLY_GRCg7b.dna.toplevel.fa",
    "indexURL": "/igvbrowser/ASSEMBLY_GRCg7b.dna.toplevel.fa.fai",
  },
  locus: $('#igvDiv').data('region'),
  tracks: [
    {
      name: "Gathered",
      format: "gtf",
      displayMode: "expanded",
      height: 300,
      url: "/igvbrowser/GRCG7b_enriched6db_NcbiEnsFrAgDavisInraNoncode_manualRemove_78323genes23926pcg44428lncRNA.sorted.gtf.gz",
      indexURL: "/igvbrowser/GRCG7b_enriched6db_NcbiEnsFrAgDavisInraNoncode_manualRemove_78323genes23926pcg44428lncRNA.sorted.gtf.gz.tbi",
      visibilityWindow: 1000000,
      color: "rgba(0, 0, 0, 1)",
      altColor: "rgba(0, 0, 0, 0.9)"
    },
    {
      name: "RefSeq",
      format: "gtf",
      displayMode: "expanded",
      height: 150,
      url: "/igvbrowser/REFSEQ_allFeatures.gtf.gz",
      indexURL: "/igvbrowser/REFSEQ_allFeatures.gtf.gz.tbi",
      visibilityWindow: 1000000,
      color: "rgba(255, 33, 1, 1)",
      altColor: "rgba(255, 33, 1, 0.9)"
    },
    {
      name: "Ensembl",
      format: "gtf",
      displayMode: "expanded",
      height: 150,
      url: "/igvbrowser/ENSEMBL_allFeatures.gtf.gz",
      indexURL: "/igvbrowser/ENSEMBL_allFeatures.gtf.gz.tbi",
      visibilityWindow: 1000000,
      color: "rgba(255, 136, 2, 1)",
      altColor: "rgba(255, 136, 2, 0.9)",
    },
    {
      name: "FrAgENCODE",
      format: "gtf",
      displayMode: "expanded",
      height: 150,
      url: "/igvbrowser/FrAgENCODE_allFeatures.gtf.gz",
      indexURL: "/igvbrowser/FrAgENCODE_allFeatures.gtf.gz.tbi",
      visibilityWindow: 1000000,
      color: "rgba(5, 248, 2, 1)",
      altColor: "rgba(5, 248, 2, 0.9)"
    },
    {
      name: "Davis",
      format: "gtf",
      displayMode: "expanded",
      height: 150,
      url: "/igvbrowser/DAVIS_allFeatures.gtf.gz",
      indexURL: "/igvbrowser/DAVIS_allFeatures.gtf.gz.tbi",
      visibilityWindow: 1000000,
      color: "rgba(1, 132, 72, 1)",
      altColor: "rgba(1, 132, 72, 0.9)"
    },
    {
      name: "INRAE",
      format: "gtf",
      displayMode: "expanded",
      height: 150,
      url: "/igvbrowser/INRA_allFeatures.gtf.gz",
      indexURL: "/igvbrowser/INRA_allFeatures.gtf.gz.tbi",
      visibilityWindow: 1000000,
      color: "rgba(0, 140, 255, 1)",
      altColor: "rgba(0, 140, 255, 0.9)"
    },
    {
      name: "Noncode",
      format: "gtf",
      displayMode: "expanded",
      height: 150,
      url: "/igvbrowser/NONCODE_allFeatures.gtf.gz",
      indexURL: "/igvbrowser/NONCODE_allFeatures.gtf.gz.tbi",
      visibilityWindow: 1000000,
      color: "rgba(210, 120, 255, 1)",
      altColor: "rgba(210, 120, 255, 0.9)"
    },
    {
      name: "Gene models",
      format: "gtf",
      displayMode: "collapse",
      height: 50,
      url: "/igvbrowser/GRCG7b_enriched6db_NcbiEnsFrAgDavisInraNoncode_manualRemove_78323genes23926pcg44428lncRNA.sorted.onlyGenes.gtf",
      visibilityWindow: 1000000,
      searchable: true
    }
  ]
}
var igvDiv = document.getElementById("igvDiv");
igv.createBrowser(igvDiv, options).then(function (browser) {
  browser.on('trackclick', function (track, popoverData) {
    if(track.id == "Ensembl") {
      var markup = "<table class=\"igv-popover-table\">";
      // Don't show a pop-over when there's no data.
      if (!popoverData || !popoverData.length) {
        return false;
      }

      popoverData.forEach(function (nameValue) {
        if (nameValue.name) {
          var value = nameValue.value;
          if(nameValue.name.toLowerCase() === 'gene_id:') {
            value = '<a target="_blank" href="https://www.ensembl.org/Gallus_gallus/Gene/Summary?db=core;g=' + nameValue.value + '">' + nameValue.value + '</a>'
          }
          else if(nameValue.name.toLowerCase() === 'transcript_id:') {
            value = '<a target="_blank" href="https://www.ensembl.org/Gallus_gallus/Transcript/Summary?db=core;t=' + nameValue.value + '">' + nameValue.value + '</a>'
          }

          markup += "<div title=\"" + nameValue.value + "\">" +
            "<span>" + nameValue.name + "</span>&nbsp;&nbsp;&nbsp;" + value + "</div>";
        }
        else {
          // not a name/value pair
          markup += "<div title=\"<hr/>\"><hr></div>";
        }
      });

      // By returning a string from the trackclick handler we're asking IGV to use our custom HTML in its pop-over.
      return markup;
    }
    if(track.id == "RefSeq") {
      var markup = "<table class=\"igv-popover-table\">";
      // Don't show a pop-over when there's no data.
      if (!popoverData || !popoverData.length) {
        return false;
      }

      popoverData.forEach(function (nameValue) {
        if (nameValue.name) {
          var value = nameValue.value;
          if(nameValue.name.toLowerCase() === 'gene_id:') {
            value = '<a target="_blank" href="https://www.ncbi.nlm.nih.gov/gene/?term=' + nameValue.value + '">' + nameValue.value + '</a>'
          }
          else if(nameValue.name.toLowerCase() === 'transcript_id:') {
            value = '<a target="_blank" href="https://www.ncbi.nlm.nih.gov/nuccore/' + nameValue.value + '">' + nameValue.value + '</a>'
          }

          markup += "<div title=\"" + nameValue.value + "\">" +
            "<span>" + nameValue.name + "</span>&nbsp;&nbsp;&nbsp;" + value + "</div>";
        }
        else {
          // not a name/value pair
          markup += "<div title=\"<hr/>\"><hr></div>";
        }
      });

      // By returning a string from the trackclick handler we're asking IGV to use our custom HTML in its pop-over.
      return markup;
    }
  });
});