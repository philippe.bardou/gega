$(document).ready(function(){
  // Annotation table
  var annotationTable;
  var annotationTable_iTotalRecords;
  var annotationTable_iTotalDisplayRecords;

  // Annotation selected rows (save ID of the user selected rows)
  var annot_table_selected_rows = []
  
  // Annotation table column def and params
  var annot_table_columnDefs = [];

  // Tree for customize cols to display
  var coltree = [];

  // Options for selectpickers (showhide and filter)
  var showhide_select_options = '';
  var filter_select_options = '<optgroup label="Annotation">';
  
  // Tissues infos (key=shortname and val {name,color,...})
  var tissues_info = {};
  // Array of short tissue names order by proximity
  var tissues_sort_default = [];
  var tissues_sort_proximity = [];
  // Tissues infos only available for ortho GGA/GSA (key=shortname and val {name,color,...})
  var tissues_info_ortho = {};

  // Projects infos (key=accession and val {...})
  var projects_info = {};

  // Gloobal ptions for all highcharts
  Highcharts.setOptions({
    exporting: {
      buttons: {
        contextButton: {
          menuItems: [
            'downloadPNG',
            'downloadJPEG',
            'downloadPDF',
            'downloadSVG',
            "separator",
            "downloadCSV",
            "downloadXLS"
          ]
        }
      }
    },
    lang: {
      decimalPoint: '.',
      thousandsSep: ''
    }
  });
  Highcharts.SVGRenderer.prototype.symbols.minus = function (x, y, w, h) {
    return ['M', x-(w/2), y+(h/2), 'L', x+w*1.5, y+(h/2)];
  };
  Highcharts.SVGRenderer.prototype.symbols.cross = function (x, y, w, h) {
    return ['M', x, y, 'L', x + w, y + h, 'M', x + w, y, 'L', x, y + h, 'z'];
  };

  // Active tooltip
  $('[data-toggle="tooltip"]').tooltip();

  // Get annotation header (col infos and params)
  // Build Show/Hide div (#showhideSelect options)
  // Build Filter div (#filterSelect_ options)
  $.ajax({
    type : "GET",
    url : window.location.origin + "/get_annotation_header",
    success: function(data){
      // Array of fields for Default view
      let defviewFields = []
      // Data for Column descriptions modal
      let dataset = [];
      // Data for build jstree
      let meta4jstree = [];
      let data4jstree = []
      // First column of annotation datatable : checkboxes
      annot_table_columnDefs.push({
        targets : 0,
        orderable: false,
        className: 'select-checkbox',
        orderDataType: 'select-checkbox',
        data: null,
        defaultContent: '',
        visible: true
      })

      $.each(data, function(i, v) {
        // ColunmDefs for annotation datatable
        annot_table_columnDefs.push({ 
          data : v.name,
          title : v.displayName,
          visible : v.visible == '0' ? false : true,
          sortable : v.sortable == '0' ? false : true,
          className: v.type == 'int' ? "text-right" : "text-left",
          targets : i+1,
          render: function (data, type, row, meta) {
            if(v.displayName == "Chr." || v.displayName == "Start" || v.displayName == "End") {
              return '<a href="/browser?region='+row.chr+':'+row.start+'-'+row.end+'">'+data+'</a>'
            }
            else {
              return data;
            }
          }
        });
        // Data for Column descriptions datatable
        var item = []
        item.push(i+1);
        item.push(v.name);
        item.push(v.displayName);
        item.push(v.meta);
        item.push(v.description);
        item.push(v.example);
        item.push(v.type);
        item.push(v.visible);
        item.push(v.sortable);
        item.push(v.searchable);
        item.push(v.facetable);
        item.push(v.graph);
        item.push(v.templateLink);
        dataset.push(item);

        // Data for build jstree
        if(! meta4jstree.includes(v.meta)) {
          meta4jstree.push(v.meta)
        }
        data4jstree.push({parent_id : v.meta, element: v.displayName})

        if(v.visible !== 0) {
          defviewFields.push(v.name)
        }
        
        if(v.searchable !== 0) {
          filter_select_options += "<option value=\"" +v.name+"\" data-type=\""+v.type+"\" data-database=\"annotation\">"+v.displayName+"</option>";
        }
        
        if(v.graph.includes("pie")) {
          $('#graphPieSelect').append( "<option value=\""+v.name+"\">"+v.displayName+"</option>");
        }
        if(v.graph.includes("boxplot")) {
          $('#graphBoxPlotSelect').append( "<option value=\""+v.name+"\">"+v.displayName+"</option>");
        }
        if(v.graph.includes("scatter")) {
          $('#graphScatterXSelect').append( "<option value=\""+v.name+"\">"+v.displayName+"</option>");
          $('#graphScatterYSelect').append( "<option value=\""+v.name+"\">"+v.displayName+"</option>");
        }
        $('#graphVennFieldSelect').append( "<option value=\""+v.name+"\">"+v.displayName+"</option>");
      });
      showhide_select_options += "<option value=\""+defviewFields.join(',')+"\" data-orderby=\"chr,start\" data-ordertype=\"asc\">Default view</option>";
      filter_select_options += '</optgroup>';
      $('#showhideSelect').append(showhide_select_options);
      $('#showhideSelect option').filter(function() { return $(this).html() == 'Default view'; }).prop('selected', true);
      $('#showhideSelect').selectpicker('refresh');
      $('#filterSelect_1').append(filter_select_options);
      $('#filterSelect_1').selectpicker('refresh');
      $('#graphPieSelect').selectpicker('refresh');
      $('#graphBoxPlotSelect').selectpicker('refresh');
      $('#graphScatterXSelect').selectpicker('refresh');
      $('#graphScatterYSelect').selectpicker('refresh');
      $('#graphVennFieldSelect').selectpicker('refresh');
      updateOperatorOnFilterEvent();
      
      /* Customize show/hide cols => jstree */
      function transformArrayToJson(data) {
        var json = [];
        var parentNodes = {};
        for (var i = 0; i < data.length; i++) {
          var nodes = data[i].split("_");
          var parent = "#";
          for (var j = 0; j < nodes.length; j++) {
            var nodeId = nodes.slice(0, j + 1).join("_");
            if (!parentNodes[nodeId]) {
              parentNodes[nodeId] = { "id": nodeId, "parent": parent, "text": nodes[j] };
              json.push(parentNodes[nodeId]);
            }
            parent = nodeId;
          }
        }
        return json;
      }
      function addElementsToJsonTree(data, jsonTree) {
        function getDataFromElement(elem) {
          let col = annot_table_columnDefs.find(function(obj) { return obj.title === elem; })
          return ({ index: col.targets, name:col.data, displayName: col.title})
        };
        for (var i = 0; i < data.length; i++) {
          var parentId = data[i].parent_id;
          var element = data[i].element;
          var parentNode = jsonTree.find(node => node.id === parentId);
          if (parentNode) {
            var newNode = {
              "id": element, //parentId + "_" + element,
              "parent": parentId,
              "text": element,
              data: getDataFromElement(element)
            };
            jsonTree.push(newNode);
          }
        }
        return jsonTree;
      }
      $('#jstree').jstree({
        'core': {
          'data': addElementsToJsonTree(data4jstree, transformArrayToJson(meta4jstree)),
          'themes': {
            'name': 'proton',
            'responsive': true
          }
        },
        "plugins" : ["checkbox","wholerow","search","types"],
        "search" : {
          search_leaves_only: true
        },
        'types': {
          'default': {
            'icon': false
          }
        }
      });
      var jstreeSearchNodes = []
      $("#jstreeQuery").keyup(function(event){
        $("#jstree").jstree().search($("#jstreeQuery").val());
        if($("#jstreeQuery").val().length != 0) {
          $('#jstreeClearSearch').prop('disabled', false);
        }
        else {
          $('#jstreeClearSearch').prop('disabled', true);
        }
      });
      $('#jstree').on('search.jstree', function(e, data) {
        jstreeSearchNodes = [];
        var matches = data.nodes.length;
        if (matches === 0) {
          $('#jstreeQueryStatus').text("Not found")
          $('#jstreeQuerySelect').prop('disabled', true);
        } else {
          for (var i = 0; i < matches; i++) {
            jstreeSearchNodes.push(data.nodes[i])
          }
          $('#jstreeQueryStatus').text(matches + " col. found")
          $('#jstreeQuerySelect').prop('disabled', false);
        }
      });
      $('#jstreeQuerySelect').click(function() {
        for (var i = 0; i < jstreeSearchNodes.length; i++) {
          $('#jstree').jstree(true).check_node(jstreeSearchNodes[i]);
          selectedNodes.push(jstreeSearchNodes[i])
        }
        $("#jstreeClearSearch").trigger("click");
      });
      $('#jstreeClearSearch').click(function() {
        jstreeSearchNodes = []
        $('#jstree').jstree(true).clear_search();
        $("#jstreeQuery").val("")
        $('#jstreeQueryStatus').text("")
        $('#jstreeQuerySelect').prop('disabled', true);
        $('#jstreeClearSearch').prop('disabled', true);
      });
      var selectedNodes = []
      $('#jstree').on('select_node.jstree', function (e, data) {
        selectedNodes = []
        let sn = $('#jstree').jstree(true).get_selected();
        for (var i = 0; i < sn.length; i++) {
          var node = $('#jstree').jstree(true).get_node(sn[i]);
          if (!node.children.length && !selectedNodes.includes(sn[i])) {
            selectedNodes.push(sn[i])
          }
        }
      });
      $('#jstree').on('deselect_node.jstree', function (e, data) {
        selectedNodes = []
        let sn = $('#jstree').jstree(true).get_selected();
        for (var i = 0; i < sn.length; i++) {
          var node = $('#jstree').jstree(true).get_node(sn[i]);
          if (!node.children.length && !selectedNodes.includes(sn[i])) {
            selectedNodes.push(sn[i])
          }
        }
      });
      $('#jstree').on("changed.jstree", function (e, data) {
        if (data.action === "select_all") {
          let sn = $('#jstree').jstree(true).get_selected();
          for (var i = 0; i < sn.length; i++) {
            var node = $('#jstree').jstree(true).get_node(sn[i]);
            if (!node.children.length && !selectedNodes.includes(sn[i])) {
              selectedNodes.push(sn[i])
            }
          }
        }
        else if (data.action === "deselect_all") {
          selectedNodes = []
        }
        $('#jstree_nb').text(selectedNodes.length)
        $('#jstree_s').text("")
        if (selectedNodes.length>1) {
          $('#jstree_s').text("s")
        }
        if (selectedNodes.length>3) {
          $('#displayCustomize').removeClass("btn-secondary")
          $('#displayCustomize').addClass("btn-primary")
          $('#displayCustomize').text("Display the " + selectedNodes.length + " selected columns")
          $('#displayCustomize').prop('disabled', false);
        }
        else {
          $('#displayCustomize').removeClass("btn-primary")
          $('#displayCustomize').addClass("btn-secondary")
          $('#displayCustomize').text("Select at least 4 columns to display")
          $('#displayCustomize').prop('disabled', true);
        }
        $('#jstreeResult').val(selectedNodes.join('\n'))
      });
      function traverseLeaves(node, array) {
        if (node.children.length === 0) {
          if(array.includes(node.id)) {
            $('#jstree').jstree(true).check_node(node.id);
            nbfound += 1
            foundList.push(node.id)
          }
          else {
            $('#jstree').jstree(true).uncheck_node(node.id);
          }
        } else {
          for (var i = 0; i < node.children.length; i++) {
            traverseLeaves(node.children[i], array);
          }
        }
      }
      var nbfound = 0
      var foundlist = []
      $('#jstreeResult').on("keyup", function() {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
          // Split input by \n and remove empty 
          let colInputList = $('#jstreeResult').val().split('\n').filter(function(element) { return element.trim().length > 0; });
          nbfound = 0
          foundList = []
          var treeData = $('#jstree').jstree(true).get_json('#', {flat: false});
          for (var i = 0; i < treeData.length; i++) {
            traverseLeaves(treeData[i], colInputList);
          }
          $('#jstreeResultNotFound').text(colInputList.filter(x => !foundList.includes(x)).join('\n'));
          $('#jstree_nbInputNotFound').text(colInputList.length-nbfound)
        }
      });
      $("#jstreeSelect").click(function () {
        $("#jstree").jstree(true).select_all();
      });
      $("#jstreeDeselect").click(function () {
        $("#jstree").jstree(true).deselect_all();
      });
      $("#jstreeCollapse").click(function () {
        $("#jstree").jstree(true).close_all();
      });
      $("#jstreeExpand").click(function () {
        $("#jstree").jstree(true).open_all();
      });
      var customViewNb = 1;
      $('#displayCustomize').click(function() {
        var selectedNodes = $('#jstree').jstree().get_selected();
        var selectedDataIndexes = [0]; // 0 for checkboxes in first col
        //var selectedDataIndexes = [];
        var selectedDataName = [];
        for (var i = 0; i < selectedNodes.length; i++) {
          var node = $('#jstree').jstree().get_node(selectedNodes[i]);
          if(typeof node.data !== "undefined") {
            selectedDataIndexes.push(node.data.index);
            selectedDataName.push(node.data.name)
          }
        }
        annotationTable.columns().visible( false, false );
        annotationTable.columns( selectedDataIndexes ).visible( true, false );
        annotationTable.columns.adjust().draw( false );
        // Save in showhideSelect the custom view
        let viewName = ($('#customViewName').val() === "" ? "Custom view" : $('#customViewName').val()) + " #"+customViewNb+" ("+selectedDataName.length+" col.)"
        customViewNb += 1;
        //showhide_select_options += "<option value=\""+selectedDataName.join(',')+"\" data-orderby=\"chr,start\" data-ordertype=\"asc\">"+viewName+"</option>";
        //$('#showhideSelect').empty().append(showhide_select_options);
        $('#showhideSelect').append($('<option>', {
          value: selectedDataName.join(','),
          text: viewName,
          'data-orderby' : "chr,start",
          'data-ordertype' : "asc"
        }));
        $('#showhideSelect').selectpicker('val', []);
        $('#showhideSelect option').filter(function() { return $(this).html() == viewName; }).prop('selected', true);
        $('#showhideSelect').selectpicker('refresh');
        $('#showhideSelect').trigger('change');
        $('#customizeColsRemove').prop("disabled", false);
        $('#customizeColsClear').prop("disabled", false);
        $('#modalCustomize').modal('hide');
      });
      

      // Init column descriptions datatable
      var cdt = $('#colDescriptionTable').DataTable({
        data: dataset,
        columns: [
          { title: "" },
          { title: "Name" },
          { title: "Name" },
          { title: "Group" },
          { title: "Description" },
          { title: "Example" },
          { title: "Type" },
          { title: "Visible" },
          { title: "Sortable" },
          { title: "Searchable" },
          { title: "Facetable" },
          { title: "Graph" },
          { title: "Template Link" }
        ],
        "columnDefs": [
          { visible: false, targets: [1,6,7,8,9,10,11,12] },
          {
            "targets": [4], "width": "100%"
          },
          {
            "targets": [5],
            "render": function(data, type, row) {
              if (data && data.length > 20) {
                return '<div style="cursor:pointer" title="' + data + '">' + data.substring(0, 20) + '...</div>';
              } else {
                return data;
              }
            }
          }
        ],
        'rowCallback': function(row, data, index){
          $(row).find('td:eq(0)').css('text-align', 'center');
          if(data[7] == "1"){
            $(row).find('td:eq(0)').css('font-weight', 'bold');
            $(row).find('td:eq(1)').css('font-weight', 'bold');
          }
        }
      });
      $("#cdtView").click(function() {
        if($("#cdtViewLabel").text() == 'Full view') {
          cdt.columns( [8,9] ).visible( false, false );
          $("#cdtViewLabel").text("Summary view")
        }
        else {
          cdt.columns( [8,9] ).visible( true, false );
          $("#cdtViewLabel").text("Full view")
        }
        cdt.columns.adjust().draw( false );
      });

      annotationTable = $('#annotationTable').DataTable({
        'processing' : true,
        'serverSide' : true,
        'scrollX' : true,
        'scrollY' : false,
        'scrollCollapse' : true,
        'language': {
          "search": "Search"
        },
        'ajax' : {
          url : '/get_annotation',
          contentType: "application/json; charset=utf-8",
          type: "POST",
          dataType: "json",
          data: function ( d ) {
            d["usersearch"] = buildFilterArray();
            d["vieworderby"] = $('#showhideSelect').find('option:selected').attr("data-orderby");
            d["viewordertype"] = $('#showhideSelect').find('option:selected').attr("data-ordertype");
            return JSON.stringify(d);
          },
          dataSrc: function (json) {
            annotationTable_iTotalRecords = json.iTotalRecords;
            annotationTable_iTotalDisplayRecords = json.iTotalDisplayRecords;
            return json.aaData;
          }
        },
        'aaSorting' : [],
        'columnDefs' : annot_table_columnDefs,
        select: {
            style:    'multi',
            selector: 'td:first-child'
        },
        "initComplete": function () {
          var searchContainer = $('#annotationTable_filter');
          var infoSup = $('<sup><i class="bx bx-info-circle"></i></sup><span>:</span>')
          infoSup.attr('data-toggle', 'tooltip');
          infoSup.attr('data-html', 'true');
          infoSup.attr('title', '<b>Global search</b><br>'+
            '<p class="mb-0 mt-0">This search queries '+
            'the following fields: '+
            '<br/>Chr., Gene ID, Gene Name, Strand,<br/>Source, '+
            'Version and gnSimpleBiotype</p>');
          searchContainer.find('input').before(infoSup);
          $('[data-toggle="tooltip"]').tooltip();
        }
      });
      // Disabled copy button
      annotationTable.on('select deselect draw.dt', function() {
        let nbRows = annotationTable.rows().count();
        let nbSelectedRows = annotationTable.rows({ selected: true }).count();
        $("[id^=copycheck]").prop('disabled', nbSelectedRows === 0);
        $('#annotTblDeselectAll').prop('disabled', nbSelectedRows === 0);
        if(nbRows === nbSelectedRows) {
          $('#annotTblSelectAll').prop('disabled', true)
        }
        else {
          $('#annotTblSelectAll').prop('disabled', false)
        }
      });
    },
    error : function(e) {
      console.log("ERROR: ", e);
    }
  });

  // Explore tissue expression modal
  // Events for Define Region modal
  // Clear all Define Region
  var deInvalid = 0
  var deValid = 0
  $('#deClearAll').click(function() {
    $('#defineExpressionFilter').removeClass("btn-primary")
    $('#defineExpressionFilter').addClass("btn-secondary")
    $('#defineExpressionFilter').prop('disabled', true);
    $("[id^='valueEte_int']").val("0");
    $("[id^='valueEte_med']").val("0");
    $("[id^='valueEte_txt']").val("");
    $("#valueEte_select_expr_isTS").val("null");
    $("[id^='valueEte_']").removeClass('modal-form-error')
    $("[id^='valueEte_']").removeClass('modal-form-valid')
    deInvalid = 0;
    deValid = 0;
  });
  
  $("#valueEte_int_expr_tau, #valueEte_int_expr_tauLog10").focusin(function() {
    $("#valueEte_select_expr_isTS").val("null");
  });

  $(document).on('focusout click', "[id^='valueEte_'], [id^='valueEte_txt_expr_tissue']", function() {
    deInvalid = 0;
    deValid = 0;
    $("[id^='valueEte_int_'], [id^='valueEte_med_']").each(function() {
      if(!/^\d*\.?\d+$/.test($(this).val())) {
        deInvalid++;
        $(this).removeClass('modal-form-valid')
        $(this).addClass('modal-form-error')
      }
      else if($(this).val() != 0) {
        $(this).removeClass('modal-form-error')
        $(this).addClass('modal-form-valid')
        deValid++;
      }
      else {
        $(this).removeClass('modal-form-valid')
        $(this).removeClass('modal-form-error')
      }
    });
    $("[id^='valueEte_txt_']").each(function(){
      if(!/^([a-zA-Z]{4}(;[a-zA-Z]{4})*)?$/.test($(this).val())) {
        deInvalid++;
        $(this).removeClass('modal-form-valid')
        $(this).addClass('modal-form-error')
      }
      else if($(this).val() != "") {
        $(this).removeClass('modal-form-error')
        $(this).addClass('modal-form-valid')
        deValid++;
      }
      else {
        $(this).removeClass('modal-form-valid')
        $(this).removeClass('modal-form-error')
      }
    });
    if($("#valueEte_select_expr_isTS").val() != "null") {
      $("#valueEte_select_expr_isTS").addClass('modal-form-valid')
      $("#valueEte_int_expr_tau").val(0)
      $("#valueEte_int_expr_tauLog10").val(0)
      deValid++;
    }
    else {
      $("#valueEte_select_expr_isTS").removeClass('modal-form-valid')
    }

    if(deInvalid == 0 && deValid > 0) {
      $('#defineExpressionFilter').removeClass("btn-secondary")
      $('#defineExpressionFilter').addClass("btn-primary")
      $('#defineExpressionFilter').prop('disabled', false);
    }
    else {
      $('#defineExpressionFilter').removeClass("btn-primary")
      $('#defineExpressionFilter').addClass("btn-secondary")
      $('#defineExpressionFilter').prop('disabled', true);
    }
  });

  $('#defineExpressionFilter').click(function() {
    if(deInvalid == 0) {
      $("[id^='valueEte_med_']").each(function(){
        if($(this).val() == 0) {
          return true;
        }
        let filterItem = $(this).attr('id').split("_")[2];
        let filterIndex = $("button[id^='filterRemove_']:hidden").attr("id").split("_")[1]
        $('#filterSelect_' + filterIndex).val(filterItem)
        $('#filterSelect_' + filterIndex).selectpicker('refresh');
        $('#filterSelect_' + filterIndex).trigger("changed.bs.select");
        $('#filterOperator_' + filterIndex).val('>=');
        $('#filterOperator_' + filterIndex).selectpicker('refresh');
        $('#filterText_' + filterIndex).val($(this).val());
        $('#filterAdd_' + filterIndex).click();
      });
      $("[id^='valueEte_int_expr_']").each(function(){
        if($(this).val() == 0) {
          return true;
        }
        let filterItem = $(this).attr('id').replace("valueEte_int_", "");
        let filterIndex = $("button[id^='filterRemove_']:hidden").attr("id").split("_")[1]
        $('#filterSelect_' + filterIndex).val(filterItem)
        $('#filterSelect_' + filterIndex).selectpicker('refresh');
        $('#filterSelect_' + filterIndex).trigger("changed.bs.select");
        $('#filterOperator_' + filterIndex).val('>=');
        $('#filterOperator_' + filterIndex).selectpicker('refresh');
        $('#filterText_' + filterIndex).val($(this).val());
        $('#filterAdd_' + filterIndex).click();
      });
      $("[id^='valueEte_txt_expr_']").each(function(){
        if($(this).val() == '') {
          return true;
        }
        let filterItem = $(this).attr('id').replace("valueEte_txt_", "");
        let filterIndex = $("button[id^='filterRemove_']:hidden").attr("id").split("_")[1]
        $('#filterSelect_' + filterIndex).val(filterItem)
        $('#filterSelect_' + filterIndex).selectpicker('refresh');
        $('#filterSelect_' + filterIndex).trigger("changed.bs.select");
        $('#filterOperator_' + filterIndex).val('~');
        $('#filterOperator_' + filterIndex).selectpicker('refresh');
        $('#filterText_' + filterIndex).val($(this).val());
        $('#filterAdd_' + filterIndex).click();
      });
      if($("#valueEte_select_expr_isTS").val() != "null") {
        let isTSval = $("#valueEte_select_expr_isTS").val()
        let filterItem = "expr_isTS";
        let filterIndex = $("button[id^='filterRemove_']:hidden").attr("id").split("_")[1]
        $('#filterSelect_' + filterIndex).val(filterItem)
        $('#filterSelect_' + filterIndex).selectpicker('refresh');
        $('#filterSelect_' + filterIndex).trigger("changed.bs.select");
        $('#filterOperator_' + filterIndex).val('=');
        $('#filterOperator_' + filterIndex).selectpicker('refresh');
        $('#filterText_' + filterIndex).val(isTSval);
        $('#filterAdd_' + filterIndex).click();
      }
      $('#modalDefineExpression').modal('hide');
    }
  });


  // Define region of interest
  Highcharts.chart('drChart', {
    chart: {
      backgroundColor: 'white',
      height: '300px',
      events: {
        load: function () {
          // Draw the flow chart
          var ren = this.renderer,
              colors = Highcharts.getOptions().colors
          // Separator left/right
          ren.path(['M', 450, 22, 'L', 450, 245])
            .attr({
                'stroke-width': 2,
                stroke: 'silver',
                dashstyle: 'dash'
            })
            .add();
          // Headers left/right
          ren.label('Start position', 50, 30)
            .attr({
                fill: colors[0],
                stroke: 'white',
                'stroke-width': 0,
                padding: 8,
                r: 5
            })
            .css({
                fontWeight: 'bold',
                'letter-spacing': '2px',
                fontSize: '13px',
                color: 'white',
                opacity: 0.8
            })
            .add()
            .shadow(true);
          ren.label('End position', 725, 30)
            .attr({
                fill: colors[0],
                stroke: 'white',
                'stroke-width': 0,
                padding: 8,
                r: 5
            })
            .css({
                fontWeight: 'bold',
                'letter-spacing': '2px',
                fontSize: '13px',
                color: 'white',
                opacity: 0.8
            })
            .add()
            .shadow(true);
          //Left rect and label
          ren.label('', 30, 120)
            .attr({
              r: 2,
              width: 415,
              height: 13,
              fill: colors[1]
            })
            .add();
          ren.path(['M', 400, 118, 'L', 400, 140])
            .attr({
              'stroke-width': 2,
              stroke: 'white'
            })
            .add();
          ren.path(['M', 425, 118, 'L', 425, 140])
            .attr({
              'stroke-width': 2,
              stroke: 'white'
            })
            .add();
          ren.label('Chr. : ?', 30, 137)
            .css({
              fontWeight: 'bold',
              fontSize: '11px'
            })
            .attr({
              id: 'HCchrleftLabel'
            })
            .add();
          // Left Gene name
          ren.label('', 101, 120)
            .css({
              display: 'none'
            })
            .attr({
              r: 2,
              width: 75,
              height: 13,
              fill: '#910000',
              id: 'HCgeneleft'
            })
            .add();
          ren.label('?', 102, 91)
            .css({
              fontWeight: 'bold',
              fontSize: '11px',
              color: '#910000',
              display: 'none'
            })
            .attr({
              id: 'HCgeneleftLabel1'
            })
            .add();
          ren.label('? - ?', 102, 103)
            .css({
              fontWeight: 'bold',
              fontSize: '11px',
              color: '#910000',
              display: 'none'
            })
            .attr({
              id: 'HCgeneleftLabel2'
            })
            .add();
          // Right rect and label							
          ren.label('', 449, 120)
            .attr({
              r: 2,
              width: 415,
              height: 13,
              fill: colors[1],
              id: 'HCopacity-1'
            })
            .add();
          ren.path(['M', 475, 118, 'L', 475, 140])
            .attr({
              'stroke-width': 2,
              stroke: 'white'
            })
            .add();
          ren.path(['M', 500, 118, 'L', 500, 140])
            .attr({
              'stroke-width': 2,
              stroke: 'white'
            })
            .add();
          ren.path(['M', 450, 118, 'L', 450, 140])
            .attr({
              'stroke-width': 2,
              stroke: 'white'
            })
            .add();
          ren.path(['M', 452, 115, 'L', 442, 145])
            .attr({
              'stroke-width': 4,
              stroke: 'white',
              id: 'HCwrong-1'
            })
            .add();
          ren.path(['M', 455, 115, 'L', 445, 145])
            .attr({
              'stroke-width': 2,
              stroke: 'orange',
              id: 'HCwrong-2'
            })
            .add();
          ren.path(['M', 458, 115, 'L', 448, 145])
            .attr({
              'stroke-width': 4,
              stroke: 'white',
              id: 'HCwrong-3'
            })
            .add();
          ren.label('Chr. : ?', 452, 137)
            .css({
              fontWeight: 'bold',
              fontSize: '11px'
            })
            .attr({
              id: 'HCchrrightLabel'
            })
            .add();
          // Right Gene name
          ren.label('', 718, 120)
            .css({
              display: 'none'
            })
            .attr({
              r: 2,
              width: 75,
              height: 13,
              fill: '#910000',
              id: 'HCgeneright'
            })
            .add();
          ren.label('?', 719, 91)
            .css({
              fontWeight: 'bold',
              fontSize: '11px',
              color: '#910000',
              display: 'none'
            })
            .attr({
              id: 'HCgenerightLabel1'
            })
            .add();
            ren.label('?', 719, 103)
            .css({
              fontWeight: 'bold',
              fontSize: '11px',
              color: '#910000',
              display: 'none'
            })
            .attr({
              id: 'HCgenerightLabel2'
            })
            .add();
          // Region of interrest
          ren.path(['M', 101, 118, 'L', 101, 212])
            .attr({
              'stroke-width': 1,
              stroke: '#e20041',
              dashstyle: 'dash',
              id: 'HCposleft'
            })
            .add();
          ren.label('?', 95, 210)
            .css({
              fontWeight: 'bold',
              fontSize: '11px'
            })
            .attr({
              id: 'HCposleftLabel'
            })
            .add();
          ren.path(['M', 799, 118, 'L', 799, 212])
            .attr({
              'stroke-width': 1,
              stroke: '#e20041',
              dashstyle: 'dash',
              id: 'HCposright'
            })
            .add();
          ren.label('?', 793, 210)
            .css({
              fontWeight: 'bold',
              fontSize: '11px'
            })
            .attr({
              id: 'HCposrightLabel'
            })
            .add();
          // Offset left
          ren.path(['M', 201, 118, 'L', 201, 201])
            .attr({
              'stroke-width': 1,
              stroke: '#e20041',
              dashstyle: 'dash',
              id: 'HCoffset-posleft'
            })
            .css({ display: 'none'} )
            .add();
          ren.label('?', 195, 196)
            .css({
              fontWeight: 'bold',
              fontSize: '11px',
              display: 'none'
            })
            .attr({
              id: 'HCoffset-posleftLabel'
            })
            .add();
          // Offset right
          ren.path(['M', 699, 118, 'L', 699, 201])
            .attr({
              'stroke-width': 1,
              stroke: '#e20041',
              dashstyle: 'dash',
              id: 'HCoffset-posright'
            })
            .css({ display: 'none'} )
            .add();
          ren.label('?', 693, 196)
            .css({
              fontWeight: 'bold',
              fontSize: '11px',
              display: 'none'
            })
            .attr({
              id: 'HCoffset-posrightLabel'
            })
            .add();
          ren.label('Region of interest', 100, 180)
            .attr({
              fill: colors[0],
              'stroke-width': 2,
              r: 2,
              width: 345,
              height: 10
            })
            .css({
              color: 'white',
              fontSize: '11px'
            })
            .add();
          ren.label('', 449, 180)
            .attr({
              fill: colors[0],
              'stroke-width': 2,
              r: 2,
              width: 345,
              height: 10,
              id: 'HCopacity-2'
            })
            .add();
          ren.path(['M', 410, 178, 'L', 410, 200])
            .attr({
              'stroke-width': 2,
              stroke: 'white'
            })
            .add();
          ren.path(['M', 430, 178, 'L', 430, 200])
            .attr({
              'stroke-width': 2,
              stroke: 'white'
            })
            .add();
          ren.path(['M', 450, 178, 'L', 450, 200])
            .attr({
              'stroke-width': 2,
              stroke: 'white'
            })
            .add();
          ren.path(['M', 470, 178, 'L', 470, 200])
            .attr({
              'stroke-width': 2,
              stroke: 'white'
            })
            .add();
          ren.path(['M', 490, 178, 'L', 490, 200])
            .attr({
              'stroke-width': 2,
              stroke: 'white'
            })
            .add();
          // Warning != chr
          ren.label('&nbsp; &nbsp; INCONSISTENT REGION<br>Start and end positions must<br/>be on the same chromosome.', 344, 165)
            .attr({
              fill: 'orange',
              stroke: 'white',
              'stroke-width': 2,
              padding: 8,
              r: 5,
              id: 'HCwrong-chr'
            })
            .css({
              color: 'white',
              fontSize: '12px',
              fontWeight: 'bold',
              'letter-spacing': '1px',
              opacity: 0.9
            })
            .add()
            .shadow(true);
          // Warning start > end
          ren.label('&nbsp; &nbsp; INCONSISTENT REGION<br>Start position must be lower<br>&nbsp; &nbsp; &nbsp; &nbsp; than end position.', 345, 165)
            .attr({
              fill: 'orange',
              stroke: 'white',
              'stroke-width': 2,
              padding: 8,
              r: 5,
              id: 'HC-wrong-pos'
            })
            .css({
              color: 'white',
              fontSize: '12px',
              fontWeight: 'bold',
              'letter-spacing': '1px',
              opacity: 0.9
            })
            .add()
            .shadow(true);
        }
      }
    },
    title: {
        text: null
    },
    credits: { enabled: false },
    exporting: { enabled: false },
    accessibility: {
        typeDescription: 'Flowchart'
    }
  });
  // Events for Define Region modal
  var drVar = {
    'leftChr'    : '',
    'leftPos'    : 0,
    'leftOffset' : 0,
    'rightChr'   : '',
    'rightPos'   : 0,
    'rightOffset': 0
  }
  updateDefineRegion(drVar);
  $('[id^=drGet]').click(function() {
    // Use attr of this to define var
    var tbtnid = $(this).attr('id')
    var tfield = $(this).attr('data-field')
    var tinputval = $("#"+$(this).attr('data-link')).val()
    var tid = $(this).attr('data-link')
    var tstatusid = $(this).attr('data-link')+"Status"
    var tside = $(this).attr('data-side')
    // Clear all... left or right see data-side
    if(tside == 'left') {
      clearDefineRegionLeft()
      drVar.leftChr = ''
      drVar.leftPos = 0
      drVar.leftOffset = 0
    }
    else {
      clearDefineRegionRight()
      drVar.rightChr = ''
      drVar.rightPos = 0
      drVar.rightOffset = 0
    }
    // Get chr start end using field (data-field and this.value)
    if(/^[a-z0-9._#]+$/i.test(tinputval)) {
      $.ajax({
        type : "GET",
        url : window.location.origin + "/get_pos?field="+tfield+"&query="+tinputval,
        success: function(data){
          if (data.length >= 1) {
            $('#'+tstatusid).removeClass('badge-danger')
            $('#'+tstatusid).addClass('badge-primary')
            $('#'+tstatusid).text("Found")
            drVar[tside+'Chr'] = data[0].chr
            $('#HCchr'+tside+'Label').children().text("Chr. : "+drVar[tside+'Chr']);
            if(tfield != "chr") {
              if(tside == 'left') {
                drVar[tside+'Pos'] = parseInt(data[0].start)
              }
              else {
                drVar[tside+'Pos'] = parseInt(data[0].end)
              }
              $('#HCpos'+tside+'Label').children().text(drVar[tside+'Pos'].toLocaleString("fr-FR"));
              $('#HCgene'+tside).fadeIn(500)
              $('#HCgene'+tside+'Label1').children().text(tinputval);
              $('#HCgene'+tside+'Label1').fadeIn(500)
              $('#HCgene'+tside+'Label2').children().text(data[0].start.toLocaleString("fr-FR")+" - "+data[0].end.toLocaleString("fr-FR"));
              $('#HCgene'+tside+'Label2').fadeIn(500)
            }
            else {
              let p = $("#"+tid+"Pos").val()
              $("#"+tid+"Pos").addClass("form-error")
              if(p > 0) {
                drVar[tside+'Pos'] = parseInt(p)
                $('#HCpos'+tside+'Label').children().text(drVar[tside+'Pos'].toLocaleString("fr-FR"));
              }
              else {
                drVar[tside+'Pos'] = 0
                $('#HCpos'+tside+'Label').children().text("?")
              }
            }
          }
          else {
            drVar[tside+'Chr'] = ''
            drVar[tside+'Pos'] = 0
            $('#'+tstatusid).removeClass('badge-primary')
            $('#'+tstatusid).addClass('badge-danger')
            $('#'+tstatusid).text("Not found")
          }
        },
        complete : function() {
          drVar[tside+'Offset'] = 0
          updateDefineRegion(drVar);
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
    else {
      $('#'+tstatusid).removeClass('badge-primary')
      $('#'+tstatusid).addClass('badge-danger')
      $('#'+tstatusid).text("Invalid input")
    }
  });
  $('#drLeftOffsetBtn').click(function() {
    $('#drLeftOffsetStatus').fadeOut(500)
    $('[id^=HCposleft').hide()
    $('[id^=HCoffset-posleft]').hide()
    if(/^[0-9]+$/i.test($('#drLeftOffset').val())) {
      let offl = parseInt($('#drLeftOffset').val())
      if(offl > 0) {
        $('#HCposleftLabel').children().text((drVar['leftPos'] - offl).toLocaleString("fr-FR"))
        $('#HCgeneleft').attr('transform', 'translate(201,120)');
        $('#HCgeneleftLabel1').attr('transform', 'translate(202,91)');
        $('#HCgeneleftLabel2').attr('transform', 'translate(202,103)');
        $('[id^=HCoffset-posleft]').fadeIn(500);
        $('#HCoffset-posleftLabel').children().text(drVar['leftPos'].toLocaleString("fr-FR"));
        drVar.leftOffset = offl
      }
      else {
        $('#HCposleftLabel').children().text(drVar['leftPos'].toLocaleString("fr-FR"))
        $('#HCgeneleft').attr('transform', 'translate(101,120)');
        $('#HCgeneleftLabel1').attr('transform', 'translate(102,91)');
        $('#HCgeneleftLabel2').attr('transform', 'translate(102,103)');
        $('[id^=HCoffset-posleft]').fadeOut(500);
        drVar.leftOffset = 0
      }
      updateDefineRegion(drVar);
    }
    else {
      $('#drLeftOffsetStatus').fadeIn(500)
    }
    $('[id^=HCposleft').fadeIn(500)
  });
  $('#drRightOffsetBtn').click(function() {
    $('#drRightOffsetStatus').fadeOut(500)
    $('[id^=HCposright').hide()
    $('[id^=HCoffset-posright]').hide()
    if(/^[0-9]+$/i.test($('#drRightOffset').val())) {
      let offr = parseInt($('#drRightOffset').val())
      if(offr > 0) {
        $('#HCposrightLabel').children().text((drVar['rightPos'] + offr).toLocaleString("fr-FR"))
        $('#HCgeneright').attr('transform', 'translate(618,120)');
        $('#HCgenerightLabel1').attr('transform', 'translate(619,91)');
        $('#HCgenerightLabel2').attr('transform', 'translate(619,103)');
        $('[id^=HCoffset-posright]').fadeIn(500);
        $('#HCoffset-posrightLabel').children().text(drVar['rightPos'].toLocaleString("fr-FR"));
        drVar.rightOffset = offr
      }
      else {
        $('#HCposrightLabel').children().text(drVar['rightPos'].toLocaleString("fr-FR"))
        $('#HCgeneright').attr('transform', 'translate(718,120)');
        $('#HCgenerightLabel1').attr('transform', 'translate(719,91)');
        $('#HCgenerightLabel2').attr('transform', 'translate(719,103)');
        $('[id^=HCoffset-posright]').fadeOut(500);
        drVar.rightOffset = 0
      }
      updateDefineRegion(drVar);
    }
    else {
      $('#drRightOffsetStatus').fadeIn(500)
    }
    $('[id^=HCposright').fadeIn(500)
  });
  // Clear all Define Region
  $('#drClearAll').click(function() {
    $('#drLeftGeneName').val("")
    $('#drRightGeneName').val("")
    $('#drLeftGeneId').val("")
    $('#drRightGeneId').val("")
    $('#drLeftChr').val("")
    $('#drLeftChrPos').val("")
    $('#drRightChr').val("")
    $('#drRightChrPos').val("")
    $('#drChrOK').val("?")
    $('#drStartOK').val("?")
    $('#drEndOK').val("?")
    $('#defineRegionFilter').removeClass("btn-primary")
    $('#defineRegionFilter').addClass("btn-secondary")
    $('#defineRegionFilter').prop('disabled', true);
    clearDefineRegionLeft()
    clearDefineRegionRight()
    drVar.leftChr = ''
    drVar.leftPos = 0
    drVar.leftOffset = 0
    drVar.rightChr = ''
    drVar.rightPos = 0
    drVar.rightOffset = 0
  });
  // Define Region
  $('#defineRegionFilter').click(function() {
    let filterIndex = $("button[id^='filterRemove_']:hidden").attr("id").split("_")[1]
    $('#filterSelect_' + filterIndex).val('chr')
    $('#filterSelect_' + filterIndex).selectpicker('refresh');
    $('#filterSelect_' + filterIndex).trigger("changed.bs.select");
    $('#filterOperator_' + filterIndex).val('=');
    $('#filterOperator_' + filterIndex).selectpicker('refresh');
    $('#filterText_' + filterIndex).val($('#drChrOK').val());
    $('#filterAdd_' + filterIndex).click();
    filterIndex = $("button[id^='filterRemove_']:hidden").attr("id").split("_")[1]
    $('#filterSelect_' + filterIndex).val('start')
    $('#filterSelect_' + filterIndex).selectpicker('refresh');
    $('#filterSelect_' + filterIndex).trigger("changed.bs.select");
    $('#filterOperator_' + filterIndex).val('>=');
    $('#filterOperator_' + filterIndex).selectpicker('refresh');
    $('#filterText_' + filterIndex).val($('#drStartOK').val());
    $('#filterAdd_' + filterIndex).click();
    filterIndex = $("button[id^='filterRemove_']:hidden").attr("id").split("_")[1]
    $('#filterSelect_' + filterIndex).val('end')
    $('#filterSelect_' + filterIndex).selectpicker('refresh');
    $('#filterSelect_' + filterIndex).trigger("changed.bs.select");
    $('#filterOperator_' + filterIndex).val('<=');
    $('#filterOperator_' + filterIndex).selectpicker('refresh');
    $('#filterText_' + filterIndex).val($('#drEndOK').val());
    $('#filterAdd_' + filterIndex).click();
    $('#modalDefineRegion').modal('hide');
  });

  // Filter By GROUP ?
  // Build Show/Hide div (#showhideSelect options)
  // Build Filter div (#filterSelect_ options)
  $.ajax({
    type : "GET",
    url : window.location.origin + "/get_annotation_header_meta",
    success: function(data){
      filter_select_options = '</optgroup>' + filter_select_options;
      $.each(data, function(i, v) {
        if(v.visible != 0) {
          showhide_select_options += "<option value=\""+v.fields+"\" data-orderby=\""+v.orderby+"\" data-ordertype=\""+v.ordertype+"\">"+v.meta+"</option>";
        }
        if(v.searchable !== 0) {
          filter_select_options =  "<option value=\"" + v.fields +
            "\" data-type=\"string\" data-database=\"annotation\"> " + 
            v.meta+"</option>" + filter_select_options;
        }
      })
      $('#showhideSelect').empty().append(showhide_select_options);
      $('#showhideSelect option').filter(function() { return $(this).html() == 'Default view'; }).prop('selected', true);
      $('#showhideSelect').selectpicker('refresh');
      $('#showhideSelect').trigger('change');
      filter_select_options = '<optgroup label="Annotation multiple fields">' + filter_select_options;
      $('#filterSelect_1').empty().append(filter_select_options);
      $('#filterSelect_1').selectpicker('refresh');
    },
    error : function(e) {
      console.log("ERROR: ", e);
    }
  });

  // Get tissues info (short/long name and color)
  // Filter By Expression ?
  // Build Filter div (#filterSelect_ options)
  $.ajax({
    type : "GET",
    url : window.location.origin + "/get_tissues_info",
    success: function(data){
      filter_select_options += '<optgroup label="Tissue median expression">';

      $.each(data, function(i, v) {
        if(v.orthoHSA != 0) {
          tissues_info_ortho[v.shortName] = {
            name  : v.name,
            color : v.color,
            proximity : v.proximity,
            orthoHSA : v.orthoHSA,
            embryonic : v.embryonic,
            fctClassif1 : v.fctClassif1,
            comment : v.comment,
            nbSample : v.nbSample,
            nbProject : v.nbProject
          }
        }
        if(v.shortName != "lymp") {
          tissues_info[v.shortName] = {
            name  : v.name,
            color : v.color,
            proximity : v.proximity,
            orthoHSA : v.orthoHSA,
            embryonic : v.embryonic,
            fctClassif1 : v.fctClassif1,
            comment : v.comment,
            nbSample : v.nbSample,
            nbProject : v.nbProject
          }
          filter_select_options += "<option value=\"" + v.shortName +
              "\" data-type=\"int\" data-database=\"expression\"> " + 
              v.shortName +" median</option>";
          $('#intraselecttissue').append( "<option data-subtext=\"("+v.shortName+", x="+v.nbProject+")\" value=\""+v.shortName+"\">"+v.name+"</option>");
          $('#intraselecttissue2').append("<option data-subtext=\"("+v.shortName+", x="+v.nbProject+")\" value=\""+v.shortName+"\">"+v.name+"</option>");
          $('#intraselecttissue3').append("<option data-subtext=\"("+v.shortName+", x="+v.nbProject+")\" value=\""+v.shortName+"\">"+v.name+"</option>");

          // Modal Explore by tissue expression
          $('#eteMedian').append(`
            <div class="input-group input-group-sm mb-1 mr-1" style="float:left;width:32%">
              <div class="input-group-prepend">
                <span class="input-group-text" style="font-size:small;width:55px">` + v.shortName + `</span>
              </div>
              <input id="valueEte_med_` + v.shortName + `" type="text" class="form-control" value="0" style="border-top-right-radius:.2rem;border-bottom-right-radius:.2rem;" />
            </div>`);
        }
      });

      // Build an array of tissues name sorted by proximity
      tissues_sort_default = Object.keys(tissues_info);
      tissues_sort_proximity = Object.keys(tissues_info).sort((a, b) => tissues_info[a].proximity - tissues_info[b].proximity);
    
      $('#intraselecttissue').selectpicker('refresh');
      $('#intraselecttissue2').selectpicker('refresh');
      $('#intraselecttissue3').selectpicker('refresh');
      
      filter_select_options += '</optgroup>'
      $('#filterSelect_1').empty().append(filter_select_options);
      $('#filterSelect_1').selectpicker('refresh');
    },
    error : function(e) {
      console.log("ERROR: ", e);
    }
  });

  // Get project info 
  $.ajax({
    type : "GET",
    url : window.location.origin + "/get_projects_info",
    success: function(data){
      $.each(data, function(i, v) {
        projects_info[v.name] = {
          accession : v.accession,
          nbSample : v.nbSample,
          nbTissues : v.nbTissues,
          details : v.details,
          breed : v.breed,
          institution : v.institution,
          description : v.description,
          color : v.color
        }
      });
    },
    error : function(e) {
      console.log("ERROR: ", e);
    }
  });

  /*
    * Events for Show/Hide div
  */
  $('#showhideSelect').on('hidden.bs.select', function (e) {
    if ($('#showhideSelect').val() !== null && $('#showhideSelect').val().length > 0) {
      let annot_table_cur_visible_cols = [0];  // 0 for checkboxes in first col
      //let annot_table_cur_visible_cols = [];
      $.each($('#showhideSelect').val().split(","), function(i, v) {
        var t = annot_table_columnDefs.find(function(obj) { return obj.data === v; });
        annot_table_cur_visible_cols.push(t.targets)
      });
      annotationTable.columns().visible( false, false );
      annotationTable.columns( annot_table_cur_visible_cols ).visible( true, false );
      annotationTable.columns.adjust().draw( false );

      if ($('#showhideSelect').find("option:selected").text().includes("#")) {
        $('#customizeColsRemove').prop("disabled", false)
      }
      else {
        $('#customizeColsRemove').prop("disabled", true)
      }
    }
  });
  $('#customizeCols').click(function() {
    $('#modalCustomize').modal('show');
  });
  $("#coldescr").click(function() {
    $('#col_description').modal('show');
  })
  $('#colDescriptionTableFilter').on('change', function () {
    var filterValue = $(this).val();
    $('#colDescriptionTable').DataTable().column(3).search(filterValue, true, false).draw();
  });
  $('#customizeColsRemove').click(function() {
    $('[data-toggle="tooltip"]').tooltip('hide');
    $('#showhideSelect').find("option:selected").remove();
    $('#showhideSelect').selectpicker('val', []);
    $('#showhideSelect option').filter(function() { return $(this).html() == 'Default view'; }).prop('selected', true);
    $('#showhideSelect').selectpicker('refresh');
    $('#showhideSelect').trigger('hidden.bs.select');
    $('#customizeColsRemove').prop("disabled", true);
    if($('#showhideSelect option').filter(function() { return $(this).text().includes("#")}).length == 0) {
      $('#customizeColsClear').prop("disabled", true)
    }
  });
  $('#customizeColsClear').click(function() {
    $('[data-toggle="tooltip"]').tooltip('hide');
    $('#showhideSelect option').filter(function() { return $(this).text().includes("#")}).remove()
    $('#showhideSelect').selectpicker('val', []);
    $('#showhideSelect option').filter(function() { return $(this).html() == 'Default view'; }).prop('selected', true);
    $('#showhideSelect').selectpicker('refresh');
    $('#showhideSelect').trigger('hidden.bs.select');
    $('#customizeColsClear').prop("disabled", true)
  });

  /*
    * Events for Search div
  */
  $('#defineRegion').click(function() {
    $('#modalDefineRegion').modal('show');
  });
  $('#defineExpression').click(function() {
    $('#modalDefineExpression').modal('show');
  });
  $(document).on("click", ".filter-add" , function(e) {
    let i = parseInt($(this).attr('id').split("_")[1])
    if(isValidFilterForm(i)) {
      $("#filterAdd_"+i).hide()
      $("#filterUpdate_"+i).show()
      $("#filterRemove_"+i).show()
      i = i+1

      // Add a filter form row
      html = '<div class="row mt-2" id="filterRow_'+ i + '" class="filter-row" style="margin-bottom:-45px">'
        + '<div class="col-lg-4 col-md-4">'
        + '<div class="input-group input-group-sm">'
        + '<div class="input-group-prepend">'
        + '<span class="input-group-text"><i class="bx bx-filter"></i></span>'
        + '</div>'
        + '<select id="filterSelect_' + i + '" class="selectpicker" multiple data-width="84%" data-container="body" data-max-options="1" data-live-search="true" data-size="10"></select>'
        + '<small class="form-text text-muted mb-3">Select a field or a group.</small>'
        + '</div></div>'
        + '<div class="col-lg-3 col-md-3">'
        + '<select id="filterOperator_' + i + '" class="selectpicker radius" data-width="100%" data-container="body">'
        + '</select>'
        + '<small class="form-text text-muted mb-3">'
        + 'Choose an operator.'
        + '</small>'
        + '</div>'
        + '<div class="col-lg-3 col-md-3">'
        + '<input id="filterText_' + i + '" type="text" class="form-control form-control-sm filter-text" />'
        + '<span id="filterTextSearchIcon_' + i + '" class="filterautocompleteicon">'
        + '<i class="bx bx-arrow-from-left" style="color:#517ed8;margin-left:-6px"></i>'
        + '</span>'
        + '<span id="filterTextSearch_' + i + '" class="filterautocomplete" style="margin-right:1px"></span>'
        + '<small class="form-text text-muted mb-3">Enter a value.</small>'
        + '</div>'
        + '<div class="col-lg-2 col-md-2">'
        + '<button id="filterAdd_' + i + '" type="button" class="filter-add btn btn-secondary btn-sm"><i class="bx bx-plus"></i></button> '
        + '<button id="filterUpdate_' + i + '" style="display:none" type="button" class="filter-update btn btn-secondary btn-sm"><i class="bx bx-refresh"></i></button> '
        + '<button id="filterRemove_' + i + '" parentdivname="filterRow_' + i + '" style="display:none" type="button" class="filter-remove btn btn-secondary btn-sm"><i class="bx bx-trash"></i></button> '
        + '</div></div></div>';
      
      $('#filterForm').append(html);
      $('#filterSelect_'+i).append(filter_select_options);
      $('#filterSelect_'+i).selectpicker('refresh');
      $('#filterOperator_'+i).selectpicker('refresh');
      updateOperatorOnFilterEvent();
      annotationTable.ajax.reload();
    }
  });
  $(document).on("click", ".filter-remove" , function(e) {
    let rowid = $(this).attr('parentdivname')
    $('#'+rowid).remove()
    annotationTable.ajax.reload();
  });
  $(document).on("click", ".filter-update" , function(e) {
    if(isValidFilterForm(parseInt($(this).attr('id').split("_")[1]))) {
      annotationTable.ajax.reload();
    }
  });

  // Autocomplete Filter Search Box
  $(document).on("click", ".autocompitem", function (e) {
    let index = $(this).attr("data-index")
    let val = $('#filterText_'+index).val()
    if(val.includes(",")) {
      var regexp = new RegExp('^(.*,).*', 'gi');
      $('#filterText_'+index).val(val.replace(regexp, '$1')+$(this).text());
    }
    else {
      $('#filterText_'+index).val($(this).text());
    }
    $('#filterTextSearch_'+index).fadeOut(500)
    $('#filterTextSearchIcon_'+index).fadeOut(500)
  });

  $(document).on("keyup click", ".filter-text" , function(e) {
    let i = parseInt($(this).attr('id').split("_")[1])
    $("#filterTextSearch_"+i).html('<img src="/img/poule_blue.gif" style="width:60%;height:auto;margin:19px 10px 10px 50px">');
    if($('#filterSelect_'+i).val() != "" &&
        $('#filterSelect_'+i).find('option:selected').attr("data-type") === 'string' &&
        !$('#filterSelect_'+i).find('option:selected').is('[data-subtext]')  // Autocompletion not available for "multiple fields".. need POST
      ) {
      $('#filterTextSearch_'+i).fadeIn(500)
      $('#filterTextSearchIcon_'+i).fadeIn(500)
      var field = encodeURIComponent($("#filterSelect_"+i).val())
      var ope = encodeURIComponent("=")
      if( $("#filterOperator_"+i).val() === "~" || $("#filterOperator_"+i).val() === "!~") {
        ope = 'LIKE'
      } 
      var query = encodeURIComponent($(this).val().split(",").slice(-1))
      $.ajax({
        type : "GET",
        url : window.location.origin + "/get_distinct_annotation_from_field?field="+field+"&ope="+ope+"&query="+query,
        success: function(data){
          var html = '<ul class="list-group">';
          var nb = Math.min(20, data.length);
          if(data.length > 0) {
            for(var count = 0; count < nb; count++) {
              let regexp = new RegExp('('+query+')', 'gi');
              html += '<a class="list-group-item list-group-item-action py-1 autocompitem" style="white-space:nowrap;" data-index="'+i+'">'+data[count].res.toString().replace(regexp, '<span class="text-primary fw-bold">$1</span>')+'</a>';
            }
            if(nb<data.length) {
              html += '<a class="list-group-item list-group-item-action py-1">... '+(data.length-20)+' not shown</a>';
            }
          }
          else {
            html += '<a href="#" class="list-group-item list-group-item-action disabled py-1">No Data Found</a>';
          }
          html += '</ul>';
          $("#filterTextSearch_"+i).html(html);
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      })
    }
  });
  $(document).on("focusout", ".filter-text", function(e) {
    $('[id^=filterTextSearch_]').fadeOut(500)
    $('[id^=filterTextSearchIcon_').fadeOut(500)
  });

  
  // Autocomplete Define Region Modal Box
  $(document).on("click", ".drautocompitem", function (e) {
    let id = $(this).attr("data-id")
    $("#"+id).val($(this).text());
    $("#"+id+"Search").fadeOut(500)
    $("#"+id+"SearchIcon").fadeOut(500)
  });

  $(document).on("keyup click", ".drfilter-text" , function(e) {
    let curid = $(this).attr('id')
    let field = $(this).attr("data-field")
    $("#"+curid+"Search").html('<img src="/img/poule_blue.gif" style="width:60%;height:auto;margin:19px 10px 10px 50px">');
    $("#"+curid+"Search").fadeIn(500)
    $("#"+curid+"SearchIcon").fadeIn(500)
    var query = encodeURIComponent($(this).val())
    $.ajax({
      type : "GET",
      url : window.location.origin + "/get_distinct_annotation_from_field?field="+field+"&ope=contains&query="+query,
      success: function(data){
        var html = '<ul class="list-group">';
        var nb = Math.min(20, data.length);
        if(data.length > 0) {
          for(var count = 0; count < nb; count++) {
            let regexp = new RegExp('('+query+')', 'gi');
            html += '<a class="list-group-item list-group-item-action py-1 drautocompitem" style="white-space:nowrap;" data-id="'+curid+'">'+data[count].res.toString().replace(regexp, '<span class="text-primary fw-bold">$1</span>')+'</a>';
          }
          if(nb<data.length) {
            html += '<a class="list-group-item list-group-item-action py-1">... '+(data.length-20)+' not shown</a>';
          }
        }
        else {
          html += '<a href="#" class="list-group-item list-group-item-action disabled py-1">No Data Found</a>';
        }
        html += '</ul>';
        $("#"+curid+"Search").html(html);
      },
      error : function(e) {
        console.log("ERROR: ", e);
      }
    })
  });
  $(document).on("focusout", ".drfilter-text", function(e) {
    let curid = $(this).attr('id')
    $("#"+curid+"Search").fadeOut(500)
    $("#"+curid+"SearchIcon").fadeOut(500)
  });

  
  // Select Deselect buttons
  $('#annotTblSelectAll').click(function () {
    annotationTable.rows().select()
    $('[data-toggle="tooltip"]').tooltip('hide');
  });
  $('#annotTblDeselectAll').click(function () {
    annotationTable.rows().deselect()
    $('[data-toggle="tooltip"]').tooltip('hide');
  });

  // Copy buttons
  $("[id^=copycheck]").click(function() {
    $('[data-toggle="tooltip"]').tooltip('hide');
    let field   = $(this).attr("data-field")
    let display = $(this).attr("data-display")
    let selectedRows = annotationTable.rows({ selected: true }).data();
    let textToCopy = ''
    for (let i=0; i<selectedRows.length; i++) {
      if(field == "region") {
        textToCopy += selectedRows[i].chr + ':' +
                      selectedRows[i].start + '-' +
                      selectedRows[i].end + '\n';
      }
      else {
        if(i!=0) { textToCopy += ',' };
        textToCopy += selectedRows[i][field];
      }
    }
    let tmp = ''
    if(selectedRows.length>1) {
      tmp = 's'
    }
    navigator.clipboard.writeText(textToCopy)
    .then(function() {
      $('#toast_icon').html('<i class="bx bx-copy"></i>')
      $('#toast_title').html("<b>"+selectedRows.length+" "+display+tmp+"</b>")
      $('#toast_body').html("Copied to clipboard.")
      $('#toast').toast('show');
    })
    .catch(function(err) {
      $('#toast_icon').html('<i class="bx bx-copy"></i>')
      $('#toast_title').html("<b>Error</b>")
      $('#toast_body').html("Failed to copy: ", err)
      $('#toast').toast('show');
    });
  });
  $("[id^=copyall]").click(function() {
    $('[data-toggle="tooltip"]').tooltip('hide');
    let field   = $(this).attr("data-field")
    let display = $(this).attr("data-display")
    let fields = []
    if(field == "region") {
      fields = ["chr","start","end"]
    }
    else {
      let tmpTable = ""
      if($(this).attr("data-table") != "") { tmpTable = $(this).attr("data-table")+"."}
      fields = [tmpTable+field]
    }
    $.ajax({
      url : '/get_filtered_annotation_from_fields',
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({
        field: fields,
        usersearch: buildFilterArray(),
        tablesearch: $('.dataTables_filter input').val()
      }),
      beforeSend: function(){
        $('#toast_icon').html('<i class="bx bx-copy"></i>')
        $('#toast_title').html("<b>Copy "+display+"</b>")
        $('#toast_body').html('Request in progress...' +
          '<img src="/img/poule_blue.gif" style="display:block;margin:auto;width:30%;height:auto;">')
        $('#toast').toast('show');
      },
      success: function(d){
        let tmp = ''
        if(d.length>1) { tmp = 's' }
        let textToCopy = ''
        if(field == "region") {
          textToCopy = d.map(function(o) {
            return o.chr + ":" + o.start + "-" + o.end;
          }).join("\n");
        }
        else {
          textToCopy = d.map(v => `${v[field]}`).join(',')
        }
        navigator.clipboard.writeText(textToCopy)
        .then(function() {
          $('#toast_icon').html('<i class="bx bx-copy"></i>')
          $('#toast_title').html("<b>"+d.length+" "+display+tmp+"</b>")
          $('#toast_body').html("Copied to clipboard.")
          $('#toast').toast('show');
        })
        .catch(function(err) {
          $('#toast_icon').html('<i class="bx bx-copy"></i>')
          $('#toast_title').html("<b>Error</b>")
          $('#toast_body').html("Failed to copy: ", err)
          $('#toast').toast('show');
        });
      },
      error : function(e) {
        console.log("ERROR: ", e);
      }
    });
  });

  // Export buttons
  $("[id^=exportann]").click(function() {
    let f = [$(this).attr("data-field")];
    if(f == "") { f = $('#showhideSelect').val().split(",") }
    $.ajax({
      url : '/export_annotation2csv',
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({
        fields: f,
        usersearch: buildFilterArray(),
        tablesearch: $('.dataTables_filter input').val()
      }),
      dataType: 'text',
      beforeSend: function() {
        $('#toast_no_hide_icon').html('<i class="bx bx-download"></i>')
        $('#toast_no_hide_title').html("<b>Download CSV</b>")
        let html = 'Please wait...<br/>Export <b>' + 
          annotationTable_iTotalDisplayRecords + '</b> entries';
        if(annotationTable_iTotalRecords != annotationTable_iTotalDisplayRecords) {
          html += '<br/>(filtered from '+annotationTable_iTotalRecords+' total entries)'
        }
        html += '.<br/>Database request in progress...';
        html += '<img src="/img/poule_blue.gif" style="display:block;margin:auto;width:30%;height:auto;">'
        $('#toast_no_hide_body').html(html)
        $('#toast_no_hide').toast('show');
      },
      success: function(data) {
        $('#toast_no_hide_body').append("...done !<br/>")
        var a = document.createElement('a');
        var blob = new Blob([data], { type: 'text/csv' });
        var url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = 'GEGA_'+updateVersion+'_annotation_'+annotationTable_iTotalDisplayRecords+'.csv';
        a.click();
        window.URL.revokeObjectURL(url);
      },
      complete: function() {
        $('#toast_no_hide').toast('hide');
      },
      error : function(e) {
        console.log("ERROR: ", e);
      }
    });  
  });
  $("[id^=exportexp]").click(function() {
    $.ajax({
      url : '/export_expression2csv',
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({
        usersearch: buildFilterArray(),
        tablesearch: $('.dataTables_filter input').val()
      }),
      dataType: 'text',
      beforeSend: function() {
        $('#toast_no_hide_icon').html('<i class="bx bx-download"></i>')
        $('#toast_no_hide_title').html("<b>Download CSV</b>")
        let html = 'Please wait...<br/>Export <b>' + 
          annotationTable_iTotalDisplayRecords + '</b> entries';
        if(annotationTable_iTotalRecords != annotationTable_iTotalDisplayRecords) {
          html += '<br/>(filtered from '+annotationTable_iTotalRecords+' total entries)'
        }
        html += '.<br/>Database request in progress...';
        html += '<img src="/img/poule_blue.gif" style="display:block;margin:auto;width:30%;height:auto;">'
        $('#toast_no_hide_body').html(html)
        $('#toast_no_hide').toast('show');
      },
      success: function(data) {
        $('#toast_no_hide_body').append("...done !<br/>")
        var a = document.createElement('a');
        var blob = new Blob([data], { type: 'text/csv' });
        var url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = 'GEGA_'+updateVersion+'_expression_'+annotationTable_iTotalDisplayRecords+'.csv';
        a.click();
        window.URL.revokeObjectURL(url);
      },
      complete: function() {
        $('#toast_no_hide').toast('hide');
      },
      error : function(e) {
        console.log("ERROR: ", e);
      }
    });  
  });

  /*
  ** Graph panels
  */
  // Tissue description datatable
  var tdt = ""
  $("#tissuedescr").click(function() {
    if ( ! $.fn.dataTable.isDataTable( '#tissueDescriptionTable' ) ) {
      let tissuetable = []
      let c = 1

      $.ajax({
        type : "GET",
        url : window.location.origin + "/get_tissues_info",
        success: function(data){
          $.each(data, function(i, v) {
            tissuetable.push(
              [
                c,
                v.name,
                v.shortName,
                v.proximity,
                v.orthoHSA,
                v.embryonic,
                v.fctClassif1,
                v.comment,
                v.nbSample,
                v.nbProject,
                v.color
              ])
            c += 1;
          });
          tdt = $('#tissueDescriptionTable').DataTable({
            data: tissuetable,
            columns: [
              { title: "" },
              { title: "Long name" },
              { title: "Short name" },
              { title: "Proximity" },
              { title: "HSA long name" },
              { title: "Embryonic" },
              { title: "FctClassif1" },
              { title: "Comment" },
              { title: "Nb. of samples" },
              { title: "Nb. of projects" },
              { title: "Color" }
            ]          
          });
          tdt.rows().every(function() {
            var row = this.node();
            var color = $(row).find('td:eq(10)').text();
            $(row).find('td:eq(10)').css('background-color', color);
            $(row).find('td:eq(10)').css('color', color);
          });
          tdt.columns( [4,5,6,7] ).visible( false, false );
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
    $('#tissue_description').modal('show');
  })
  $("#tdtView").click(function() {
    if($("#tdtViewLabel").text() == 'Full view') {
      tdt.columns( [4,5,6,7] ).visible( false, false );
      $("#tdtViewLabel").text("Summary view")
    }
    else {
      tdt.columns( [4,5,6,7] ).visible( true, false );
      $("#tdtViewLabel").text("Full view")
    }
    tdt.columns.adjust().draw( false );
  });
  // Project description datatable
  var pdt = ""
  $("#projectdescr").click(function() {
    if ( ! $.fn.dataTable.isDataTable( '#projectDescriptionTable' ) ) {
      let projecttable = []
      let c = 1
      for(let p in projects_info) {
        projecttable.push(
          [
            c,
            projects_info[p].accession,
            projects_info[p].nbSample,
            projects_info[p].nbTissues,
            projects_info[p].details,
            p,
            projects_info[p].breed,
            projects_info[p].institution,
            projects_info[p].description,
            projects_info[p].color
          ])
        c += 1;
      }
      pdt = $('#projectDescriptionTable').DataTable({
        data: projecttable,
        columns: [
          { title: "" },
          { title: "Accession" },
          { title: "Nb. of samples" },
          { title: "Nb. of tissues" },
          { title: "Details" },
          { title: "Project name" },
          { title: "Breed" },
          { title: "Institution" },
          { title: "Description" },
          { title: "Color" },
        ]          
      });
      pdt.rows().every(function() {
        var row = this.node();
        var color = $(row).find('td:eq(9)').text();
        $(row).find('td:eq(9)').css('background-color', color);
        $(row).find('td:eq(9)').css('color', color);
      });
      pdt.columns( [4,8] ).visible( false, false );
    }
    $('#project_description').modal('show');
  })
  $("#pdtView").click(function() {
    if($("#pdtViewLabel").text() == 'Full view') {
      pdt.columns( [4,8] ).visible( false, false );
      $("#pdtViewLabel").text("Summary view")
    }
    else {
      pdt.columns( [4,8] ).visible( true, false );
      $("#pdtViewLabel").text("Full view")
    }
    pdt.columns.adjust().draw( false );
  });
  /*
  ** Graph Explore
  */
  $('#nav-tab a').on('show.bs.tab', function (e) {
    var tab = $(e.target).data('tab');
    if(tab === 'nav-expressionbyage') {
      $('#nav-tab-expressionbysex-select').hide()
      $('#nav-tab-expressionbyage-select').fadeIn(500)
    } 
    else if(tab === 'nav-expressionbysex') {
      $('#nav-tab-expressionbyage-select').hide()
      $('#nav-tab-expressionbysex-select').fadeIn(500)
    }
    else {
      $('#nav-tab-expressionbyage-select').hide()
      $('#nav-tab-expressionbysex-select').hide()
    }
  });
  
  $('.hidenext').click(function() {
    $(this).find(".bx").fadeToggle(50);
    $(this).next().slideToggle();
    $(this).toggleClass('inside-title-close');
  });

  $('#graphPieSelect').on('hidden.bs.select', function (e) {
    let field = $(this).val()[0]
    let field_displayName = $('option:selected', this).text()
    if(typeof field !== "undefined") {
      $.ajax({
        url : '/get_filtered_annotation_from_field_group',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          field: field,
          usersearch: buildFilterArray(),
          tablesearch: $('.dataTables_filter input').val()
        }),
        beforeSend: function(){
          $('#graphView').html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:30%;height:auto;">');
        },
        success: function(d){
          let data = []
          $.each(d, function(i, v) {
            data.push({name:v.val,y:v.count})
          });
          Highcharts.chart('graphView', {
            chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
            },
            credits: { enabled: false },
            title: {
              text: 'Proportion of each value for "' + field_displayName + '" field'
            },
            subtitle: {
              text: 'GEGA - Version ' + updateVersion
            },
            tooltip: {
              pointFormat: '<b>{point.y}</b>'
            },
            accessibility: {
              point: { valueSuffix: '%' }
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
              }
            },
            series: [{
              colorByPoint: true,
              data: data
            }]
          });
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
  });

  // Box plot function   
  $('#graphBoxPlotSelect').on('hidden.bs.select', function (e) {
    let categories = $(this).val()
    if(categories.length>0) {
      $.ajax({
        url : '/get_filtered_annotation_from_fields',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          field: categories,
          usersearch: buildFilterArray(),
          tablesearch: $('.dataTables_filter input').val()
        }),
        beforeSend: function(){
          $('#graphView').html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:30%;height:auto;">');
        },
        success: function(d){
          let data = []
          for (let i=0; i<categories.length; i++) {
            data.push(getBoxValues(d.map(v => `${v[categories[i]]}`).filter(v => isNumeric(v)).sort(numSort)))
          }
          Highcharts.chart('graphView', {
            chart: {
              type: 'boxplot'
            },
            credits: { enabled: false },
            title: {
              text: 'Box plot for selected field(s)'
            },
            subtitle: {
              text: 'GEGA - Version ' + updateVersion
            },
            legend: {
              enabled: false
            },
            xAxis: {
              categories: categories,
            },
            tooltip: {
              useHTML: true,
              formatter: function() {
                let text = '<span style="font-size:12px"><b>'+this.key+'</b></span><table style="width:100%">' +
                  '<tr><td style="padding-top:5px">Maximum : </td>' +
                  '<td style="padding-top:5px;text-align:right"> <b>'+this.point.high+'</b></td></tr>' +
                  '<tr><td style="padding:0">Upper quartile : </td>' +
                  '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.q3+'</b></td></tr>' +
                  '<tr><td style="padding:0">Median : </td>' +
                  '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.median+'</b></td></tr>' +
                  '<tr><td style="padding:0">Lower quartile : </td>' +
                  '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.q1+'</b></td></tr>' +
                  '<tr><td style="padding:0">Minimum : </td>' +
                  '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.low+'</b></td></tr>' +
                  '</table>';
                return text
              }
            },
            series: [{ data: data }]
          });
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });        
    }
  });

  // Scatter plot
  $('#graphScatterXSelect,#graphScatterYSelect').on('hidden.bs.select', function (e) {
    let x = $('#graphScatterXSelect').val()
    let y = $('#graphScatterYSelect').val()
    if(x != "" && y != "") {
      $.ajax({
        url : '/get_filtered_annotation_from_fields',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          field: ["gnId",x,y],
          usersearch: buildFilterArray(),
          tablesearch: $('.dataTables_filter input').val()
        }),
        beforeSend: function(){
          $('#graphView').html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:30%;height:auto;">');
        },
        success: function(d){
          let data = []
          let name  = d.map(v => `${v["gnId"]}`)
          let xdata = d.map(v => `${v[x]}`)
          let ydata = d.map(v => `${v[y]}`)
          // Limited to 5000 points
          for(let i=0; i<=xdata.length && i<5000; i++) {
            if(isNumeric(xdata[i]) && isNumeric(ydata[i])) {
              data.push({x:parseFloat(xdata[i]), y:parseFloat(ydata[i]), name:name[i]})
            }
          }
          Highcharts.chart('graphView', {
            chart: {
              type: 'scatter',
              zoomType: 'xy'
            },
            credits: { enabled: false },
            title: {
              text: x + ' versus '+ y + ' ('+ data.length + ' points)'
            },
            subtitle: {
              text: 'GEGA - Version ' + updateVersion
            },
            xAxis: {
              title: {
                enabled: true,
                text: x
              },
              startOnTick: true,
              endOnTick: true,
              showLastLabel: true,
              min: 0
            },
            yAxis: {
              title: {
                text: y
              }
            },
            legend: { enabled: false },
            plotOptions: {
              scatter: {
                marker: {
                  radius: 4,
                  states: {
                    hover: {
                      enabled: true,
                      lineColor: 'rgb(100,100,100)'
                    }
                  }
                },
                states: {
                  hover: {
                    marker: {
                      enabled: false
                    }
                  }
                },
                tooltip: {
                  headerFormat: '',
                  pointFormat: '<b>{point.name}</b><br>'+x+' : {point.x}<br>'+y+' : {point.y}'
                }
              },
              series: {
                turboThreshold: 6000
              }
            },
            series: [{
              name: 'Female',
              color: 'rgba(223, 83, 83, .5)',
              data: data
            }]
          });
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
  });

  /*
  ** Graph Compare
  */
  // Venn
  $('#vennAdd').click(function() {
    let f = $("#graphVennFieldSelect").val();
    let l = $("#graphVennListSelect").val();
    if(f.length == 0) { $("#graphVennFieldSelect").parent().parent().find('*').addClass("form-error"); }
    else              { $("#graphVennFieldSelect").parent().parent().find('*').removeClass("form-error"); }
    if(l.length == 0) { $("#graphVennListSelect").parent().parent().find('*').addClass("form-error"); }
    else              { $("#graphVennListSelect").parent().parent().find('*').removeClass("form-error"); }
    if(f.length>0 && l.length>0) {
      $.ajax({
        url : '/get_filtered_annotation_from_field_distinct',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          field: f,
          usersearch: buildFilterArray(),
          tablesearch: $('.dataTables_filter input').val()
        }),
        beforeSend: function() {
          $('#toast_no_hide_icon').html('<i class="bx bxs-cog"></i>')
          $('#toast_no_hide_title').html("<b>Build list "+l+"...</b>")
          let html = 'Please wait...<br/>Extract <b>' + 
            f + '</b> field from ';
          if(annotationTable_iTotalRecords != annotationTable_iTotalDisplayRecords) {
            html += 'filtered';
          }
          html += '<br/>annotation table in progress...';
          html += '<img src="/img/poule_blue.gif" style="display:block;margin:auto;width:30%;height:auto;">'
          $('#toast_no_hide_body').html(html)
          $('#toast_no_hide').toast('show');
        },
        success: function(d){
          let data = []
          $.each(d, function(i, v) {
            data.push(v.val)
          });
          $("#venn"+l+"-ta").val(data.join("\n"))
          updateJvenn();
        },
        complete: function() {
          $('#toast_no_hide').toast('hide');
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
  });
  $("[id^=venn]").change(function() {
    updateJvenn();
  });
  $('[id^=vennrm_]').click(function() {
    let i = $(this).attr('id').split("_")[1]
    $("#venn"+i+"-ta").val("")
    $("#venn"+i+"-name").val("List "+i)
    updateJvenn();
  });
  updateJvenn();

  /*
  ** Graph Expression intra tissues
  */
  // Heatmap
  $('#intraselecttissue').on('hidden.bs.select', function (e) {
    let tissue = $(this).val()[0]
    if(typeof tissue !== "undefined") {
      $.ajax({
        url : '/get_expression_for_filtered_gnid',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          tissue: tissue,
          usersearch: buildFilterArray(),
          tablesearch: $('.dataTables_filter input').val()
        }),
        beforeSend: function(){
          $('#intraexpressionView').html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:30%;height:auto;">');
          $('#expIntraListOfgnIds').text("")
          $('#expIntraTissue').text("...")
        },
        success: function(d){
          // Y axis
          let gnIds = []
          // Save values by key (key = gnId+"_"+tissue)
          let val = {}
          // Save d only for selected tissue to order by median the gnIds
          let dForSelectedTissue = []
          d.forEach(function(v){
            if(v.tissue === tissue) { dForSelectedTissue.push(v) }
            val[v.gnId+"_"+v.tissue] = v.median
          });
          dForSelectedTissue.sort(function (a, b) { return a.median < b.median; });
          dForSelectedTissue.forEach(function(g) {
            // gnIds ordered by median desc of the selected tissue
            gnIds.push(g.gnId)
          })
          let data = []
          let gi = 0;
          gnIds.forEach(function(g){
            let ti = 0;
            for(let t in tissues_info) {
              data.push([ti, gi, val[g+"_"+t]]);
              ti++;
            }
            gi++;
          });
          function getPointCategoryName(point, dimension) {
            var series = point.series,
              isY = dimension === 'y',
              axis = series[isY ? 'yAxis' : 'xAxis'];
            return axis.categories[point[isY ? 'y' : 'x']];
          }

          var expchart = Highcharts.chart('intraexpressionView', {
            chart: {
              type: 'heatmap',
              marginTop: 100,
              marginBottom: 60,
              plotBorderWidth: 1
            },
            title: {
              text: 'Expression profiles across tissues for the '+ gnIds.length +' most expressed genes in ' + tissue
            },
            subtitle: {
              text: 'GEGA - Version ' + updateVersion
            },
            credits: { enabled: false },
            plotOptions: {
              series: {
                cursor: 'pointer',
                point: {
                  events: {
                    click: function () {
                      var g = this.series.chart.series[0].yAxis.categories[this.y];
                      navigator.clipboard.writeText(g)
                        .then(function() {
                          $('#toast_icon').html('<i class="bx bx-copy"></i>')
                          $('#toast_title').html("<b>"+g+"</b>")
                          $('#toast_body').html("Copied to clipboard.")
                          $('#toast').toast('show');
                        })
                        .catch(function(err) {
                          $('#toast_icon').html('<i class="bx bx-copy"></i>')
                          $('#toast_title').html("<b>Error</b>")
                          $('#toast_body').html("Failed to copy: ", err)
                          $('#toast').toast('show');
                        });
                    }
                  }
                }
              }
            },
            xAxis: [{
              categories: Object.keys(tissues_info),
              labels: {
                step: 1,
                formatter: function () {
                  if(this.value == tissue) {
                    return '<span style="color:rgb(125, 182, 236)"><b>' + this.value + '</b></span>'
                  }
                  else {
                    return this.value
                  }
                }
              }
            },
            {
              categories: Object.keys(tissues_info),
              labels: {
                step: 1,
                formatter: function () {
                  if(this.value == tissue) {
                    return '<span style="color:rgb(125, 182, 236)"><b>' + this.value + '</b></span>'
                  }
                  else {
                    return this.value
                  }
                }
              },
              reversed: true,
              opposite: true,
              linkedTo: 0
            }],
            yAxis: {
              categories: gnIds,
              title: null,
              reversed: true
            },
            accessibility: {
              point: {
                descriptionFormatter: function (point) {
                  var ix = point.index + 1,
                    xName = getPointCategoryName(point, 'x'),
                    yName = getPointCategoryName(point, 'y'),
                    val = point.value;
                  return ix + '. ' + xName + ' sales ' + yName + ', ' + val + '.';
                }
              }
            },
            colorAxis: {
              min: 0,
              minColor: '#FFFFFF',
              maxColor: Highcharts.getOptions().colors[0]
            },
            legend: {
              align: 'right',
              layout: 'vertical',
              margin: 0,
              verticalAlign: 'top',
              y: 63,
              symbolHeight: 280
            },
            tooltip: {
              formatter: function () {
                return 'Median expression of <b>' + getPointCategoryName(this.point, 'y') + '</b> <br>in <b>' +
                  getPointCategoryName(this.point, 'x') + '</b> : <b>' + this.point.value + '</b>';
              }
            },
            series: [{
              name: '',
              borderWidth: 1,
              data: data,
              turboThreshold: 0
            }],
            responsive: {
              rules: [{
                condition: {
                  maxWidth: 500
                },
                chartOptions: {
                  yAxis: {
                    labels: {
                      formatter: function () {
                        return this.value.charAt(0);
                      }
                    }
                  }
                }
              }]
            }
          });
          if(gnIds.length == 1) {
            expchart.setTitle({ text: 'Expression profiles across tissues for the most expressed gene in ' + tissue })
          }
          $('#expIntraListOfgnIds').val(gnIds.join('\n'));
          for (var i = 0; i < gnIds.length; i++) {
            var lineNb = i + 1 + ".";
            var divLineNb = $('<div>').text(lineNb);
              $('#expIntraListOfgnIdsLineNb').append(divLineNb);
          }
          $('#expIntraListOfgnIds').scroll(function() {
            $('#expIntraListOfgnIdsLineNb').scrollTop($('#expIntraListOfgnIds').scrollTop());
          });

          $('#expIntraTissue').text(tissue)
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
  });
  
  // Intra tissues Box plot (expression)
  // Display jitter
  $('#intraboxexpjitter').on('hidden.bs.select', function (e) {
    var intraboxexpgraphids = []
    $('[id^=intraboxexpressionView_]').each(function(){
      intraboxexpgraphids.push($(this).attr("id"))
    });
    async function processIntraBoxExpJitter(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var series = $('#'+item).highcharts().series;
        $.each(series, function(index, serie) {
          if (serie.name === 'scatterjitter') {
            $("#intraboxexpjitter").val() == "true" ? serie.setVisible(true) : serie.setVisible(false)
            return false; // Sortir de la boucle après avoir trouvé la série
          }
        });
      }
    }
    processIntraBoxExpJitter(intraboxexpgraphids);
  });
  // Set same max Y
  $('#intraboxexpsetmax').on('hidden.bs.select', function (e) {
    var intraboxexpgraphids = []
    $('[id^=intraboxexpressionView_]').each(function(){
      intraboxexpgraphids.push($(this).attr("id"))
    });
    async function processIntraBoxExpSetMax(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.yAxis[0].update({
          max: $("#intraboxexpsetmax").val() == "true" ? parseFloat($("#intraboxexpsetmax").attr('data-maxy')) : null
        });
      }
    }
    processIntraBoxExpSetMax(intraboxexpgraphids);
  });
  // Display export button
  $('#intraboxexpviewexport').on('hidden.bs.select', function (e) {
    var intraboxexpgraphids = []
    $('[id^=intraboxexpressionView_]').each(function(){
      intraboxexpgraphids.push($(this).attr("id"))
    });
    async function processIntraBoxExpButtons(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ exporting: { enabled: $("#intraboxexpviewexport").val() == "true" ? true : false } })
      }
    }
    processIntraBoxExpButtons(intraboxexpgraphids);
  });
  // Display tooltips
  $('#intraboxexpviewtooltips').on('hidden.bs.select', function (e) {
    var intraboxexpgraphids = []
    $('[id^=intraboxexpressionView_]').each(function(){
      intraboxexpgraphids.push($(this).attr("id"))
    });
    async function processIntraBoxExpTooltips(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ tooltip: { enabled: $("#intraboxexpviewtooltips").val() == "true" ? true : false } })
      }
    }
    processIntraBoxExpTooltips(intraboxexpgraphids);
  });
  // Build boxplot
  $('#intraboxplotgnId').keypress(function(event){
    $(this).removeClass("form-error");
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13' && $('#intraselecttissue2').val().length != 0) {
      var geneId = $(this).val().split(/[\s,]+/)
      let h = 825
      switch(geneId.length) {
        case 1:
          h = 825
          break
        case 2:
          h = 410
          break
        case 3:
          h = 275
          break
        default:
          h = 200
          break
      }
      let html = '<div class="row ml-0 mr-0">'
      for(let i=0; i<geneId.length; i++) {
        html += '<div class="col-lg-12 pl-0 pr-0" id="intraboxexpressionView_'+i+'" style="height:'+h+'px"></div>'
      }
      html += '</div>'
      $('#intraexpressionView').html(html)
      $('#intraexpressionView').css("overflow","auto")

      $.ajax({
        url : '/get_expressions_for_gnids_boxplot_all_project',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          geneIds: $('#intraboxplotgnId').val(),
          tissue: $('#intraselecttissue2').val()
        }),
        beforeSend: function(){
          for(let i=0; i<geneId.length; i++) {
            $('#intraboxexpressionView_'+i).html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:22%;height:auto;">');
          }
        },
        success: function(d){
          // Save values by key (key = gnId+"_"+age)
          let vals = {}
          let projects = []
          let maxY = 0
          let ytitles = {} // (key = gnId)
          d.forEach(function(v){
            vals[v.gnId+"_"+v.project] = v.vals
            if(!projects.includes(v.project)) {
              projects.push(v.project)
            }
            maxY = Math.max(Math.max(...v.vals.split(";")), maxY)
            ytitles[v.gnId] = v.gnId + "<br>in "+$('#intraselecttissue2').val()[0]+" (" +
              (v.gnName === v.gnId ? "-" : v.gnName) +
              " / " + v.gnSimpleBiotype +")";
          });
          // Save MaxY 
          $('#intraboxexpsetmax').attr('data-maxy', maxY)
          let gnIds = $('#intraboxplotgnId').val().split(/[\s,]+/)
          async function processBuildIntraBoxExpPlot(items) {
            for(let indexDiv=0; indexDiv<items.length; indexDiv++) {
              await new Promise(resolve => setTimeout(resolve, 0));
              // Container for graph
              let graphDiv = "intraboxexpressionView_"+indexDiv
              let boxplotData = [];
              let scatterData = [];
              let nbofvalues  = [];
              let i = 0;
              if(items[indexDiv] in ytitles) {
                for(let p of projects) {
                  let tmparr = vals[items[indexDiv]+"_"+p].split(";")
                  let tmpboxplot = getBoxValues(tmparr.sort((a, b) => a - b))
                  tmpboxplot.x = i
                  tmpboxplot.color = projects_info[p].color
                  tmpboxplot.fillColor = hexToRgba(projects_info[p].color, 0.1)
                  boxplotData.push(tmpboxplot)
                  nbofvalues[p] = 0
                  tmparr.forEach(function(m){
                    scatterData.push([i, parseFloat(m)])
                    nbofvalues[p] += 1
                  });
                  i+=1;
                }
                let titletext = null
                let subtitletext = null
                if(items.length === 1) {
                  titletext = 'Expression profiles in ' + $('#intraselecttissue2').val()[0]
                  subtitletext = 'GEGA - Version ' + updateVersion
                }
                Highcharts.chart(graphDiv, {
                  chart: {
                    zoomType: 'x',
                    events: {
                      click: function(event) {
                        navigator.clipboard.writeText(items[indexDiv])
                          .then(function() {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>"+items[indexDiv]+"</b>")
                            $('#toast_body').html("Copied to clipboard.")
                            $('#toast').toast('show');
                          })
                          .catch(function(err) {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>Error</b>")
                            $('#toast_body').html("Failed to copy: ", err)
                            $('#toast').toast('show');
                          });
                      }
                    }
                  },
                  title: {
                    text: titletext
                  },
                  subtitle: {
                    text: subtitletext
                  },
                  credits: { enabled: false },
                  legend: { enabled: false },
                  exporting:{ enabled: $("#intraboxexpviewexport").val() == "true" ? true : false },
                  xAxis: {
                    categories: projects,
                    labels: {
                      step: 1,
                      formatter: function () {
                        return '<span style="color:'+projects_info[this.value].color+'">' + this.value + '</span>'
                      }                      
                    },
                    events: {
                      afterSetExtremes: function(e) {
                        // Same zoom for all boxplot
                        let intraboxexpgraphids = []
                        $('[id^=intraboxexpressionView_]').each(function(){
                          intraboxexpgraphids.push($(this).attr("id"))
                        });
                        async function processIntraBoxExpZoom(items) {
                          for (const item of items) {
                            await new Promise(resolve => setTimeout(resolve, 0));
                            var chart = $('#'+item).highcharts();
                            let curextr = chart.xAxis[0].getExtremes()
                            if(curextr.min != e.min || curextr.max != e.max) {
                              chart.xAxis[0].setExtremes(e.min, e.max);
                            }
                          }
                        }
                        processIntraBoxExpZoom(intraboxexpgraphids);
                      }
                    }
                  },
                  yAxis: {
                    title: { text: ytitles[items[indexDiv]] },
                    max: $("#intraboxexpsetmax").val() == "true" ? maxY : null
                  },
                  plotOptions: {
                    series: {
                      pointPadding: 0.1, groupPadding: 0
                    }
                  },
                  tooltip: {
                    enabled: $("#intraboxexpviewtooltips").val() == "true" ? true : false,
                    useHTML: true,
                    formatter: function() {
                      let text = '<span style="font-size:12px"><b>'+this.key+'</b></span><table style="width:100%">' +
                        '<tr style="border-bottom:1px solid '+this.color+';"><td style="padding-bottom:5px;">Number of values : </td>' +
                        '<td style="padding-bottom:5px;text-align:right"> <b>'+nbofvalues[this.key]+'</b></td></tr>' +
                        '<tr><td style="padding-top:5px">Maximum : </td>' +
                        '<td style="padding-top:5px;text-align:right"> <b>'+this.point.high.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Upper quartile : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.q3.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Median : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.median.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Lower quartile : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.q1.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Minimum : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.low.toFixed(2)+'</b></td></tr>' +
                        '</table>';
                      return text
                    }
                  },
                  series: [
                    {
                      type: 'boxplot',
                      data: boxplotData
                    },
                    {
                      name: 'scatterjitter',
                      type: 'scatter',
                      data: scatterData,
                      jitter: { x: 0.15 },
                      marker: { radius: 1 },
                      color: 'rgba(100, 100, 100, 0.6)',
                      enableMouseTracking: false,
                      visible: $("#intraboxexpjitter").val() == "true" ? true : false
                    }
                  ]
                });
              }
              else {
                $("#intraboxplotgnId").addClass("form-error");
                let html = `
                  <div class="row" style="display:flex;height:250px;margin:auto">
                    <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                      <i class="bx bx-error" style="color:red;"></i>
                    </div>
                    <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                      <small><b>INPUT ERROR</b><br/>`
                html += "\""+ items[indexDiv] + "\" not found ! <br/>"
                html += '</small></div></div>'
                $('#'+graphDiv).html(html)
              }
            }
          }
          processBuildIntraBoxExpPlot(gnIds)
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
  });

  // Intra co expression plot
  // Linear / Log
  $('#intracoexplog').on('hidden.bs.select', function (e) {
    if($("#intracoexplog").val() == 'logarithmic') {
      $('#intracoexpviewlinereg').val("false");
      $('#intracoexpviewlinereg').prop('disabled', true);
      $('#intracoexpviewlinereg').selectpicker('refresh');
      $('#intracoexpviewlinereg').trigger('hidden.bs.select');
    }
    else {
      $('#intracoexpviewlinereg').val("true");
      $('#intracoexpviewlinereg').prop('disabled', false);
      $('#intracoexpviewlinereg').selectpicker('refresh');
      $('#intracoexpviewlinereg').trigger('hidden.bs.select');
    }
    var intracoexpgraphids = []
    $('[id^=intracoexpressionView_]').each(function(){
      intracoexpgraphids.push($(this).attr("id"))
    });
    async function processIntraCoExpLog(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.xAxis[0].update({
          type: $("#intracoexplog").val() == "linear" ? 'linear' : 'logarithmic',
          min:  $("#intracoexplog").val() == "linear" ? 0 : null
        });
        chart.yAxis[0].update({
          type: $("#intracoexplog").val() == "linear" ? 'linear' : 'logarithmic',
          min:  $("#intracoexplog").val() == "linear" ? 0 : null
        });
      }
    }
    processIntraCoExpLog(intracoexpgraphids);
  });
  // Display linear regression
  $('#intracoexpviewlinereg').on('hidden.bs.select', function (e) {
    var intracoexpgraphids = []
    $('[id^=intracoexpressionView_]').each(function(){
      intracoexpgraphids.push($(this).attr("id"))
    });
    async function processIntraCoExpRegression(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        $("#intracoexpviewlinereg").val() == "true" ? chart.series[1].show() : chart.series[1].hide()
      }
    }
    processIntraCoExpRegression(intracoexpgraphids);
  });
  // Display Labels
  $('#intracoexpviewlabels').on('hidden.bs.select', function (e) {
    var intracoexpgraphids = []
    $('[id^=intracoexpressionView_]').each(function(){
      intracoexpgraphids.push($(this).attr("id"))
    });
    async function processIntraCoExpLabels(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        var data = chart.series[0].options.data;
        $.each(data, function(i,point){
          point.dataLabels.enabled = $("#intracoexpviewlabels").val() == "true" ? true : false
        })
        chart.series[0].update(data);
      }
    }
    processIntraCoExpLabels(intracoexpgraphids);
  });
  // Display export button
  $('#intracoexpviewexport').on('hidden.bs.select', function (e) {
    var intracoexpgraphids = []
    $('[id^=intracoexpressionView_]').each(function(){
      intracoexpgraphids.push($(this).attr("id"))
    });
    async function processIntraCoExpButtons(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ exporting: { enabled: $("#intracoexpviewexport").val() == "true" ? true : false } })
      }
    }
    processIntraCoExpButtons(intracoexpgraphids);
  });
  // Display tooltips
  $('#intracoexpviewtooltips').on('hidden.bs.select', function (e) {
    var intracoexpgraphids = []
    $('[id^=intracoexpressionView_]').each(function(){
      intracoexpgraphids.push($(this).attr("id"))
    });
    async function processIntraCoExpTooltips(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ tooltip: { enabled: $("#intracoexpviewtooltips").val() == "true" ? true : false } })
      }
    }
    processIntraCoExpTooltips(intracoexpgraphids);
  });
  // Sort plot by input or by correlation
  $('#intracoexpsort').on('hidden.bs.select', function (e) {
    // Select all div with attr data-corr
    var divs = $('[id^=intracoexpressionView_]');
    // Sort div using $('#coexpsort').val()
    divs.sort(function(a, b) {
      var corrA = parseFloat($(a).attr($('#intracoexpsort').val()));
      var corrB = parseFloat($(b).attr($('#intracoexpsort').val()));
      return corrB - corrA; // sort desc
    });
    // Add sorted div to container
    $.each(divs, function(index, element) {
      $(element).animate({opacity: 0}, 0);
      $(element).appendTo($(element).parent())
        .delay(50 * index) // Add delay
        .animate({opacity: 1}, {
          duration: 1000,
          queue: true,
          complete: function() {
              $(this).dequeue();
          }
        });
    });
  });
  $('#intracoexphidecorr').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13') {
      // For each div with attr data-corr
      let nbvisible = 0
      $.each($('[id^=intracoexpressionView_]'), function(index, div) {
        const dataCorr   = parseFloat($(this).attr('data-corr'));
        const inputValue = parseFloat($('#intracoexphidecorr').val());
        if (dataCorr < inputValue) {
          $(div).fadeOut(500);
        }
        else {
          $(div).fadeIn(500);
          nbvisible += 1
        }
      });
      $('#intracoexpvisible').text(nbvisible)
    }
  });
  // Build plot(s)
  $('#intracoexpgnIdX,#intracoexpgnIdY').keypress(function(event){
    $(this).removeClass("form-error");
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13' && $('#intraselecttissue3').val().length != 0 && $('#intracoexpgnIdX').val() !== "" && $('#intracoexpgnIdY').val() !== "") {
      var otherGenes = $('#intracoexpgnIdY').val().split(/[\s,]+/)
      // By default sort by input
      $('#intracoexpsort').val("data-input")
      $('#intracoexpsort').selectpicker('refresh');
      $('#intracoexpvisible').text(otherGenes.length)
      $('#intracoexpnbplot').text(otherGenes.length);
      let h
      let collg
      if(otherGenes.length == 1) {
        h = 825
        collg = 12
      }
      else if(otherGenes.length <=6 ) {
        h = 400
        collg = 6
      }
      else {
        h = 250
        collg = 4
      }
      let html = '<div class="row ml-0 mr-0">'
      for(let i=0; i<otherGenes.length; i++) {
        html += '<div class="col-lg-'+collg+' pl-0 pr-0" id="intracoexpressionView_'+i+'" data-input="'+(otherGenes.length-i)+'" style="height:'+h+'px"></div>'
      }
      html += '</div>'
      $('#intraexpressionView').html(html)
      $('#intraexpressionView').css("overflow","auto")
      
      // Container for graph
      var graphDiv = ""
      var graphCompleted = 0
      var promisebuildintrascatter = new Promise(function(resolve, reject) {
        $.each(otherGenes, function(indexDiv, otherGeneId) {
          graphDiv = "intracoexpressionView_"+indexDiv
          $.ajax({
            url : '/get_expression_for_gnids_all_project',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({
              geneIdsX: $('#intracoexpgnIdX').val(),
              geneIdsY: otherGeneId,
              tissue: $('#intraselecttissue3').val(),
              graphDiv: graphDiv
            }),
            beforeSend: function(){
              $('#'+graphDiv).html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:30%;height:auto;">');
            },
            success: function(d){
              // Save values by key (key = gnId+"_"+project)
              let valX = {}
              let projects = []
              d.X.forEach(function(v){
                if(typeof valX[v.gnId+"_"+v.project] === "undefined") {
                  valX[v.gnId+"_"+v.project] = { 'val': [] }
                }
                let tmpval = v.vals.split(';')
                for(let i=0; i<tmpval.length; i++) {
                  valX[v.gnId+"_"+v.project]['val'].push(tmpval[i])
                }
                if(!projects.includes(v.project)) {
                  projects.push(v.project)
                }
              });
              let valY = {}
              let ytitle = {}
              d.Y.forEach(function(v){
                if(typeof valY[v.gnId+"_"+v.project] === "undefined") {
                  valY[v.gnId+"_"+v.project] = { 'val': [] }
                }
                let tmpval = v.vals.split(';')
                for(let i=0; i<tmpval.length; i++) {
                  valY[v.gnId+"_"+v.project]['val'].push(tmpval[i])
                }
                ytitle[v.gnId] = v.gnId + "<br>in " + $('#intraselecttissue3').val() + " (" +
                  (v.gnName === v.gnId ? "-" : v.gnName) +
                  " / " + v.gnSimpleBiotype +")";
              });
              // If gnIdX and gnIdY found
              if(d.X.length>0 && d.Y.length>0) {
                let gnIdX = d.gnIdX
                let gnIdY = d.gnIdY
                let data = []
                let arrayOfarray = []
                // To set x and y axis
                let maxX = -Infinity
                let maxY = -Infinity
                let minX = Infinity
                let minY = Infinity
                let i = 0;
                for(let p of projects) {
                  for(let j=0; j<valX[gnIdX+"_"+p]['val'].length; j++) {
                    data.push({
                      x:parseFloat(valX[gnIdX+"_"+p]['val'][j]),
                      y:parseFloat(valY[gnIdY+"_"+p]['val'][j]),
                      name: p,
                      color: hexToRgba(projects_info[p].color, 0.5),
                      dataLabels: {
                        enabled: $("#intracoexpviewlabels").val() == "true" ? true : false,
                        format: '{point.name}',
                        color: hexToRgba(projects_info[p].color, 0.8),
                        style: {
                          textOutline: 'none' 
                        }
                      }
                    })
                    arrayOfarray.push([parseFloat(valX[gnIdX+"_"+p]['val'][j]),parseFloat(valY[gnIdY+"_"+p]['val'][j])])
                    maxX = Math.max(maxX,parseFloat(valX[gnIdX+"_"+p]['val'][j]))
                    maxY = Math.max(maxY,parseFloat(valY[gnIdY+"_"+p]['val'][j]))
                    if(valX[gnIdX+"_"+p]['val'][j] != 0) {minX = Math.min(minX,parseFloat(valX[gnIdX+"_"+p]['val'][j]))}
                    if(valY[gnIdY+"_"+p]['val'][j] != 0) {minY = Math.min(minY,parseFloat(valY[gnIdY+"_"+p]['val'][j]))}
                  }
                  i += 1;
                }
                // Points for linear regression 
                let x = [Math.min(minX, minY)]
                /*for(var k=Math.min(minX, minY)/10; k<Math.max(maxX, maxY)*10; k*=2) {
                  x.push(Math.min(minX, minY)*k)
                }*/
                x.push(Math.max(maxX, maxY))
                let titletext = null
                let subtitletext = null
                if(otherGenes.length === 1) {
                  titletext = 'Co-expression by project in ' + $('#intraselecttissue3').val()
                  subtitletext = 'GEGA - Version ' + updateVersion
                }
                // Get data to plot linearRegression
                $.ajax({
                  url : '/get_linearRegression',
                  type: "POST",
                  contentType: "application/json",
                  data: JSON.stringify({
                    aa : arrayOfarray,
                    x : x
                  }),
                  success: function(d2){
                    // Save correlation in attr of graph container
                    $('#'+d.gd).attr("data-corr", d2.corr)
                    if(d2.corr < parseFloat($('#intracoexphidecorr').val())) {
                      $('#'+d.gd).fadeOut(500)
                    }
                    Highcharts.chart(d.gd, {
                      chart: {
                        type: 'scatter',
                        zoomType: 'xy',
                        events: {
                          click: function(event) {
                            navigator.clipboard.writeText(gnIdY)
                              .then(function() {
                                $('#toast_icon').html('<i class="bx bx-copy"></i>')
                                $('#toast_title').html("<b>"+gnIdY+"</b>")
                                $('#toast_body').html("Copied to clipboard.")
                                $('#toast').toast('show');
                              })
                              .catch(function(err) {
                                $('#toast_icon').html('<i class="bx bx-copy"></i>')
                                $('#toast_title').html("<b>Error</b>")
                                $('#toast_body').html("Failed to copy: ", err)
                                $('#toast').toast('show');
                              });
                          }
                        }
                      },
                      credits:  { enabled: false },
                      title:    { text: titletext },
                      subtitle: { text: subtitletext },
                      exporting:{ enabled: $("#intracoexpviewexport").val() == "true" ? true : false },
                      tooltip:  { enabled: $("#intracoexpviewtooltips").val() == "true" ? true : false },
                      xAxis: {
                        title: {
                          enabled: true,
                          text: gnIdX
                        },
                        startOnTick: true,
                        endOnTick: true,
                        showLastLabel: true,
                        type: $("#intracoexplog").val() == "linear" ? 'linear' : 'logarithmic',
                        min:  minX, //$("#intracoexplog").val() == "linear" ? 0 : minX,
                        max:  maxX
                      },
                      yAxis: {
                        title: { text: ytitle[gnIdY] },
                        type: $("#intracoexplog").val() == "linear" ? 'linear' : 'logarithmic',
                        min:  minY, //$("#intracoexplog").val() == "linear" ? 0 : minY,
                        max:  maxY
                      },
                      legend: { enabled: false },
                      plotOptions: {
                        scatter: {
                          marker: {
                            radius: 4,
                            states: {
                              hover: {
                                enabled: true,
                                lineColor: 'rgb(100,100,100)'
                              }
                            }
                          },
                          states: {
                            hover: {
                              marker: {
                                enabled: false
                              }
                            }
                          },
                          tooltip: {
                            headerFormat: '',
                            pointFormat: '<b>{point.name}</b><br>'+gnIdX+' : {point.x}<br>'+gnIdY+' : {point.y}'
                          }
                        }
                      },
                      series: [{
                        data: data
                      },
                      {
                        type: 'spline',
                        label: 'Linear regression',
                        data: d2.xy,
                        marker: {
                          enabled: false
                        },
                        states: {
                          hover: {
                            lineWidth: 0
                          }
                        },
                        enableMouseTracking: false,
                        dashStyle: 'dash',
                        opacity: 0.8,
                        visible: $("#intracoexpviewlinereg").val() == "true" ? true : false
                      }],
                      labels: {
                        items: [{
                          html: '&rho; = '+ d2.corr,
                          style: {
                            left: '10px',
                            top: '10px',
                            fontWeight: 'bold'
                          }
                        }]
                      }
                    });
                  },
                  error : function(e) {
                    console.log("ERROR: ", e);
                  }
                });
              }
              // Else gnIdX or gnIdY not found
              else {
                let html = `
                  <div class="row" style="display:flex;height:250px;margin:auto">
                    <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                      <i class="bx bx-error" style="color:red;"></i>
                    </div>
                    <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                      <small><b>INPUT ERROR</b><br/>`
                if(d.X.length==0 || typeof d.X[0] === "undefined") {
                  html += "\""+ d.gnIdX + "\" not found ! <br/>"
                  $("#intracoexpgnIdX").addClass("form-error");
                }
                if(d.Y.length==0 && typeof d.Y[0] === "undefined") {
                  html += "\""+ d.gnIdY +"\" not found !"
                  $("#intracoexpgnIdY").addClass("form-error");
                }
                html += '</small></div></div>'
                $('#'+d.gd).html(html)
              }
            },
            complete : function(){
              graphCompleted += 1
              if (graphCompleted === otherGenes.length) {
                setTimeout(resolve, 1000);
              }
            },
            error : function(e) {
              console.log("ERROR: ", e);
            }
          });
        })
      })
      promisebuildintrascatter.then(function() {
        let nbvisible = 0
        $.each($('[id^=intracoexpressionView_]'), function(index, div) {
          const dataCorr   = parseFloat($(this).attr('data-corr'));
          const inputValue = parseFloat($('#intracoexphidecorr').val());
          if (dataCorr < inputValue) {
            $(div).fadeOut(500);
          }
          else {
            $(div).fadeIn(500);
            nbvisible += 1
          }
        });
        $('#intracoexpvisible').text(nbvisible)
      });
    }
  });


  /*
  ** Graph Expression across tissues
  */
  // Correlation plot
  $('#corrGnId,#corrDistance').keypress(function(event){
    $('#corrGnId').removeClass("form-error");
    $('#corrDistance').removeClass("form-error");
    let valid = true
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
      if ($('#corrGnId').val().length<5) {
        $('#corrGnId').addClass("form-error");
        valid = false
      }
      if(!/^\d+$/.test($('#corrDistance').val())) {
        $('#corrDistance').addClass("form-error");
        valid = false
      }
      if (valid) {
        $.ajax({
          url : '/get_mostCorrGene',
          type: "POST",
          contentType: "application/json",
          data: JSON.stringify({
            geneId: $('#corrGnId').val()
          }),
          beforeSend: function(){
            $('#expressionView').html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:30%;height:auto;">');
            $('#corrListOfgnIds').text("")
          },
          success: function(d){
            let gnIds = []
            let categories = []
            let corrs = []
            let geneOfInt = d.filter(objet => objet.gnId === $('#corrGnId').val())[0]
            d.forEach(function(v){
              if(v.gnId !== $('#corrGnId').val()) {
                gnIds.push(v.gnId)
                categories.push(v.gnId + " (" +
                  (v.gnName === v.gnId ? "-" : v.gnName) +
                  " / " + v.gnSimpleBiotype +")");
                let color = "#7cb5ec"
                let distance = ''
                if(v.chr !== geneOfInt.chr) {
                  distance = "-"
                  color = "#434348"
                }
                else {
                  distance = geneDistance(geneOfInt.start, geneOfInt.end, v.start, v.end)
                  if(distance < parseInt($('#corrDistance').val())) {
                  color = "#90ed7d";
                  }
                }
                corrs.push({y:v.corr, color:color, chr:v.chr, start:v.start, end:v.end, distance:distance})
              }
            });
            if(gnIds.length > 0) {
              Highcharts.chart('expressionView', {
                chart: {
                  type: 'bar'
                },
                title: {
                  text: "The " + gnIds.length +' most correlated genes with ' + $('#corrGnId').val()
                },
                subtitle: {
                  text: 'GEGA - Version ' + updateVersion
                },
                legend:  { enabled: false },
                credits: { enabled: false },
                tooltip: {
                  useHTML: true,
                  formatter: function() {
                    let text = '<span style="font-size:12px"><b>'+this.point.category.split(" ")[0]+'</b></span><table style="width:100%">' +
                      '<tr><td>Chr. : </td>' +
                      '<td style="text-align:right"> <b>'+this.point.chr+'</b></td></tr>' +
                      '<tr><td>Start : </td>' +
                      '<td style="text-align:right"> <b>'+this.point.start+'</b></td></tr>' +
                      '<tr style="border-bottom:1px solid '+this.color+';"><td style="padding-bottom:5px">End : </td>' +
                      '<td style="padding-bottom:5px;text-align:right"> <b>'+this.point.end+'</b></td></tr>' +
                      '<tr><td style="padding-top:5px">Correlation (&rho;) : </td>' +
                      '<td style="padding-top:5px;text-align:right"> <b>'+this.y+'</b></td></tr>' +
                      '<tr><td>Distance : </td>' +
                      '<td style="text-align:right"> <b>'+this.point.distance+'</b></td></tr>'
                      '</table>';
                    return text
                  }
                },
                xAxis: {
                  categories: categories
                },
                yAxis: {
                  max: 1,
                  title: {
                    text: 'Correlation (&rho;)'
                  },
                  labels: {
                    overflow: 'justify'
                  }
                },
                plotOptions: {
                  series: {
                    pointPadding: 0,
                    groupPadding: 0,
                    cursor: 'pointer',
                    point: {
                      events: {
                        click: function () {
                          let c = this.category.split(" ")[0]
                          navigator.clipboard.writeText(c)
                            .then(function() {
                              $('#toast_icon').html('<i class="bx bx-copy"></i>')
                              $('#toast_title').html("<b>"+c+"</b>")
                              $('#toast_body').html("Copied to clipboard.")
                              $('#toast').toast('show');
                            })
                            .catch(function(err) {
                              $('#toast_icon').html('<i class="bx bx-copy"></i>')
                              $('#toast_title').html("<b>Error</b>")
                              $('#toast_body').html("Failed to copy: ", err)
                              $('#toast').toast('show');
                            });
                        }
                      }
                    }
                  },
                  bar: {
                    dataLabels: {
                      enabled: true,
                      inside: true,
                      align: 'right'
                    }
                  }
                },
                series: [{data:corrs}]
              });

              $('#corrListOfgnIds').val(gnIds.join('\n'));
              for (var i = 0; i < gnIds.length; i++) {
                var lineNb = i + 1 + ".";
                var divLineNb = $('<div>').text(lineNb);
                  $('#corrListOfgnIdsLineNb').append(divLineNb);
              }
              $('#corrListOfgnIds').scroll(function() {
                $('#corrListOfgnIdsLineNb').scrollTop($('#corrListOfgnIds').scrollTop());
              });
            }
            else {
              $("#corrGnId").addClass("form-error");
              let html = `
                <div class="row" style="display:flex;height:250px;margin:auto">
                  <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                    <i class="bx bx-error" style="color:red;"></i>
                  </div>
                  <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                    <small><b>INPUT ERROR</b><br/>`
              html += "\""+ $('#corrGnId').val() + "\" not found ! <br/>"
              html += '</small></div></div>'
              $('#expressionView').html(html)
            }
          },
          error : function(e) {
            console.log("ERROR: ", e);
          }
        });
      }
    }
  });

  // Bar plot
  // Linear / Log
  $('#barexplog').on('hidden.bs.select', function (e) {
    var barexpgraphids = []
    $('[id^=barexpressionView_]').each(function(){
      barexpgraphids.push($(this).attr("id"))
    });
    async function processBarExpLog(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.yAxis[0].update({
          type: $("#barexplog").val() == "linear" ? 'linear' : 'logarithmic',
          min:  $("#barexplog").val() == "linear" ? 0 : 0.1
        });
      }
    }
    processBarExpLog(barexpgraphids);
  });
  // Set same max Y
  $('#barexpsetmax').on('hidden.bs.select', function (e) {
    var barexpgraphids = []
    $('[id^=barexpressionView_]').each(function(){
      barexpgraphids.push($(this).attr("id"))
    });
    async function processBarExpSetMax(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.yAxis[0].update({
          max: $("#barexpsetmax").val() == "true" ? parseFloat($("#barexpsetmax").attr('data-maxy')) : null
        });
      }
    }
    processBarExpSetMax(barexpgraphids);
  });
  // Display export button
  $('#barexpviewexport').on('hidden.bs.select', function (e) {
    var barexpgraphids = []
    $('[id^=barexpressionView_]').each(function(){
      barexpgraphids.push($(this).attr("id"))
    });
    async function processBarExpButtons(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ exporting: { enabled: $("#barexpviewexport").val() == "true" ? true : false } })
      }
    }
    processBarExpButtons(barexpgraphids);
  });
  // Display tooltips
  $('#barexpviewtooltips').on('hidden.bs.select', function (e) {
    var barexpgraphids = []
    $('[id^=barexpressionView_]').each(function(){
      barexpgraphids.push($(this).attr("id"))
    });
    async function processBarExpTooltips(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ tooltip: { enabled: $("#barexpviewtooltips").val() == "true" ? true : false } })
      }
    }
    processBarExpTooltips(barexpgraphids);
  });
  // Sort categories by tissues proximity
  $('#barexpviewsort').on('hidden.bs.select', function (e) {
    var barexpgraphids = []
    $('[id^=barexpressionView_]').each(function(){
      barexpgraphids.push($(this).attr("id"))
    });
    async function processBarExpSort(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        if($("#barexpviewsort").val() == "true" & chart.xAxis[0].categories.join() !== tissues_sort_proximity.join()) {
          chart.xAxis[0].update({ categories: tissues_sort_proximity });
          var sortedData = tissues_sort_proximity.map(function (category) {
            var index = tissues_sort_default.indexOf(category);
            var originalDataPoint = chart.series[0].data[index];
            return {
              y: originalDataPoint.y,
              color: originalDataPoint.color
            };
          });
          chart.series[0].setData(sortedData);
        }
        else if($("#barexpviewsort").val() == "false" & chart.xAxis[0].categories.join() !== tissues_sort_default.join()) {
          chart.xAxis[0].update({ categories: tissues_sort_default });
          var sortedData = tissues_sort_default.map(function (category) {
            var index = tissues_sort_proximity.indexOf(category);
            var originalDataPoint = chart.series[0].data[index];
            return {
              y: originalDataPoint.y,
              color: originalDataPoint.color
            };
          });
          chart.series[0].setData(sortedData);
        }
      }
    }
    processBarExpSort(barexpgraphids);
  });
  // Build bar plot(s)
  $('#barplotgnId').keypress(function(event){
    $(this).removeClass("form-error");
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
      var geneId = $(this).val().split(/[\s,]+/)
      let h = 825
      switch(geneId.length) {
        case 1:
          h = 825
          break
        case 2:
          h = 410
          break
        case 3:
          h = 275
          break
        default:
          h = 200
          break
      }
      let html = '<div class="row ml-0 mr-0">'
      for(let i=0; i<geneId.length; i++) {
        html += '<div class="col-lg-12 pl-0 pr-0" id="barexpressionView_'+i+'" style="height:'+h+'px"></div>'
      }
      html += '</div>'
      $('#expressionView').html(html)
      $('#expressionView').css("overflow","auto")

      $.ajax({
        url : '/get_expression_for_gnids',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          geneIdsX: $('#barplotgnId').val(),
          geneIdsY: "none"
        }),
        beforeSend: function(){
          for(let i=0; i<geneId.length; i++) {
            $('#barexpressionView_'+i).html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:22%;height:auto;">');
          }
        },
        success: function(d){
          // Save values by key (key = gnId+"_"+tissue)
          let val = {}
          let maxY = 0
          let ytitles = {} // (key = gnId)
          d.X.forEach(function(v){
            val[v.gnId+"_"+v.tissue] = v.median
            maxY = Math.max(maxY, v.median)
            ytitles[v.gnId] = v.gnId + "<br>(" +
              (v.gnName === v.gnId ? "-" : v.gnName) +
              " / " + v.gnSimpleBiotype +")";
          });
          // Save MaxY 
          $('#barexpsetmax').attr('data-maxy', maxY)
          let gnIds = $('#barplotgnId').val().split(/[\s,]+/)
          async function processBuildBarExpPlot(items) {
            for(let indexDiv=0; indexDiv<items.length; indexDiv++) {
              await new Promise(resolve => setTimeout(resolve, 0));
              // Container for graph
              let graphDiv = "barexpressionView_"+indexDiv
              let d = [];
              if(items[indexDiv] in ytitles) {
                // Init with tissues sorted by default or proximity
                if($("#barexpviewsort").val() == "true") {
                  for(let t of tissues_sort_proximity ) {
                    d.push({y:val[items[indexDiv]+"_"+t], color:tissues_info[t].color})
                  }
                }
                else {
                  for(let t of tissues_sort_default) {
                    d.push({y:val[items[indexDiv]+"_"+t], color:tissues_info[t].color})
                  }
                }
                let data = [{name:items[indexDiv],data:d}]
                let titletext = null
                let subtitletext = null
                if(items.length === 1) {
                  titletext = 'Expression profiles across tissues'
                  subtitletext = 'GEGA - Version ' + updateVersion
                }
                Highcharts.chart(graphDiv, {
                  chart: {
                    type: 'column',
                    events: {
                      click: function(event) {
                        navigator.clipboard.writeText(items[indexDiv])
                          .then(function() {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>"+items[indexDiv]+"</b>")
                            $('#toast_body').html("Copied to clipboard.")
                            $('#toast').toast('show');
                          })
                          .catch(function(err) {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>Error</b>")
                            $('#toast_body').html("Failed to copy: ", err)
                            $('#toast').toast('show');
                          });
                      }
                    }
                  },
                  title: {
                    text: titletext
                  },
                  subtitle: {
                    text: subtitletext
                  },
                  credits: { enabled: false },
                  legend: { enabled: false },
                  exporting:{ enabled: $("#barexpviewexport").val() == "true" ? true : false },
                  xAxis: {
                    categories: $("#barexpviewsort").val() == "true" ? tissues_sort_proximity : tissues_sort_default,
                    labels: {
                      step: 1,
                      formatter: function () {
                        return '<span style="color:'+tissues_info[this.value].color+'">' + this.value + '</span>'
                      }
                    }
                  },
                  yAxis: {
                    title: { text: ytitles[items[indexDiv]] },
                    type: $("#barexplog").val() == "linear" ? 'linear' : 'logarithmic',
                    min:  $("#barexplog").val() == "linear" ? 0 : 0.1,
                    max: $("#barexpsetmax").val() == "true" ? maxY : null
                  },
                  plotOptions: {
                    series: {
                      colorByPoint: true,
                      pointPadding: 0, groupPadding: 0
                    }
                  },
                  tooltip: {
                    headerFormat: '<span style="font-size:12px"><b>{point.key}</b></span><table>',
                    pointFormat: '<tr><td style="padding:0">{series.name} : </td>' +
                      '<td style="padding:0;padding-left:5px;text-align:right"> <b>{point.y:.2f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true,
                    enabled: $("#barexpviewtooltips").val() == "true" ? true : false
                  },
                  series: data
                });
              }
              else {
                $("#barplotgnId").addClass("form-error");
                let html = `
                  <div class="row" style="display:flex;height:250px;margin:auto">
                    <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                      <i class="bx bx-error" style="color:red;"></i>
                    </div>
                    <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                      <small><b>INPUT ERROR</b><br/>`
                html += "\""+ items[indexDiv] + "\" not found ! <br/>"
                html += '</small></div></div>'
                $('#'+graphDiv).html(html)
              }
            }
          }
          processBuildBarExpPlot(gnIds)
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
  });

  // Box plot (expression)
  // Display jitter
  $('#boxexpjitter').on('hidden.bs.select', function (e) {
    var boxexpgraphids = []
    $('[id^=boxexpressionView_]').each(function(){
      boxexpgraphids.push($(this).attr("id"))
    });
    async function processBoxExpJitter(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var series = $('#'+item).highcharts().series;
        $.each(series, function(index, serie) {
          if (serie.name === 'scatterjitter') {
            $("#boxexpjitter").val() == "true" ? serie.setVisible(true) : serie.setVisible(false)
            return false; // Sortir de la boucle après avoir trouvé la série
          }
        });
      }
    }
    processBoxExpJitter(boxexpgraphids);
  });
  // Set same max Y
  $('#boxexpsetmax').on('hidden.bs.select', function (e) {
    var boxexpgraphids = []
    $('[id^=boxexpressionView_]').each(function(){
      boxexpgraphids.push($(this).attr("id"))
    });
    async function processBoxExpSetMax(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.yAxis[0].update({
          max: $("#boxexpsetmax").val() == "true" ? parseFloat($("#boxexpsetmax").attr('data-maxy')) : null
        });
      }
    }
    processBoxExpSetMax(boxexpgraphids);
  });
  // Display export button
  $('#boxexpviewexport').on('hidden.bs.select', function (e) {
    var boxexpgraphids = []
    $('[id^=boxexpressionView_]').each(function(){
      boxexpgraphids.push($(this).attr("id"))
    });
    async function processBoxExpButtons(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ exporting: { enabled: $("#boxexpviewexport").val() == "true" ? true : false } })
      }
    }
    processBoxExpButtons(boxexpgraphids);
  });
  // Display tooltips
  $('#boxexpviewtooltips').on('hidden.bs.select', function (e) {
    var boxexpgraphids = []
    $('[id^=boxexpressionView_]').each(function(){
      boxexpgraphids.push($(this).attr("id"))
    });
    async function processBoxExpTooltips(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ tooltip: { enabled: $("#boxexpviewtooltips").val() == "true" ? true : false } })
      }
    }
    processBoxExpTooltips(boxexpgraphids);
  });
  // Sort categories by tissues proximity
  $('#boxexpviewsort').on('hidden.bs.select', function (e) {
    var boxexpgraphids = []
    $('[id^=boxexpressionView_]').each(function(){
      boxexpgraphids.push($(this).attr("id"))
    });
    async function processBoxExpSort(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        if($("#boxexpviewsort").val() == "true" & chart.xAxis[0].categories.join() !== tissues_sort_proximity.join()) {
          chart.xAxis[0].update({ categories: tissues_sort_proximity });
          var boxplotData = tissues_sort_proximity.map(function (category, i) {
            var index = tissues_sort_default.indexOf(category);
            var originalDataPoint = chart.series[0].data[index];
            return {
              x: i,
              color: originalDataPoint.color,
              fillColor: originalDataPoint.fillColor,
              high: originalDataPoint.high,
              low: originalDataPoint.low,
              median: originalDataPoint.median,
              q1: originalDataPoint.q1,
              q3: originalDataPoint.q3
            };
          });
          var scatterMedianData = tissues_sort_proximity.map(function (category, i) {
            var index = tissues_sort_default.indexOf(category);
            var originalDataPoint = chart.series[2].data[index];
            return {
              x: i,
              y: originalDataPoint.y,
              color: originalDataPoint.color
            };
          });
          var scatterData = chart.series[1].data.map(function (pair) {
            var index = tissues_sort_proximity.indexOf(tissues_sort_default[pair.x]);
            return [index, pair.y ]
          });
          chart.series[0].setData(boxplotData);
          chart.series[1].setData(scatterData);
          chart.series[2].setData(scatterMedianData);
        }
        else if($("#boxexpviewsort").val() == "false" & chart.xAxis[0].categories.join() !== tissues_sort_default.join()) {
          chart.xAxis[0].update({ categories: tissues_sort_default });
          var boxplotData = tissues_sort_default.map(function (category, i) {
            var index = tissues_sort_proximity.indexOf(category);
            var originalDataPoint = chart.series[0].data[index];
            return {
              x: i,
              color: originalDataPoint.color,
              fillColor: originalDataPoint.fillColor,
              high: originalDataPoint.high,
              low: originalDataPoint.low,
              median: originalDataPoint.median,
              q1: originalDataPoint.q1,
              q3: originalDataPoint.q3
            };
          });
          var scatterMedianData = tissues_sort_default.map(function (category, i) {
            var index = tissues_sort_proximity.indexOf(category);
            var originalDataPoint = chart.series[2].data[index];
            return {
              x: i,
              y: originalDataPoint.y,
              color: originalDataPoint.color
            };
          });
          var scatterData = chart.series[1].data.map(function (pair) {
            var index = tissues_sort_default.indexOf(tissues_sort_proximity[pair.x]);
            return [ index, pair.y ]
          });
          chart.series[0].setData(boxplotData);
          chart.series[1].setData(scatterData);
          chart.series[2].setData(scatterMedianData);
        }
      }
    }
    processBoxExpSort(boxexpgraphids);
  });
  // Build boxplot
  $('#boxplotgnId').keypress(function(event){
    $(this).removeClass("form-error");
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
      var geneId = $(this).val().split(/[\s,]+/)
      let h = 825
      switch(geneId.length) {
        case 1:
          h = 825
          break
        case 2:
          h = 410
          break
        case 3:
          h = 275
          break
        default:
          h = 200
          break
      }
      let html = '<div class="row ml-0 mr-0">'
      for(let i=0; i<geneId.length; i++) {
        html += '<div class="col-lg-12 pl-0 pr-0" id="boxexpressionView_'+i+'" style="height:'+h+'px"></div>'
      }
      html += '</div>'
      $('#expressionView').html(html)
      $('#expressionView').css("overflow","auto")

      $.ajax({
        url : '/get_expressions_for_gnids_boxplot',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          geneIds: $('#boxplotgnId').val()
        }),
        beforeSend: function(){
          for(let i=0; i<geneId.length; i++) {
            $('#boxexpressionView_'+i).html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:22%;height:auto;">');
          }
        },
        success: function(d){
          // Save values by key (key = gnId+"_"+tissue)
          let medianvalues = {}
          let medianvalue = {}
          let maxY = 0
          let ytitles = {} // (key = gnId)
          d.forEach(function(v){
            medianvalues[v.gnId+"_"+v.tissue] = v.median_values
            medianvalue[v.gnId+"_"+v.tissue] = v.median
            maxY = Math.max(Math.max(...v.median_values.split(";")), maxY)
            ytitles[v.gnId] = v.gnId + "<br>(" +
              (v.gnName === v.gnId ? "-" : v.gnName) +
              " / " + v.gnSimpleBiotype +")";
          });
          // Save MaxY 
          $('#boxexpsetmax').attr('data-maxy', maxY)
          let gnIds = $('#boxplotgnId').val().split(/[\s,]+/)
          async function processBuildBoxExpPlot(items) {
            for(let indexDiv=0; indexDiv<items.length; indexDiv++) {
              await new Promise(resolve => setTimeout(resolve, 0));
              // Container for graph
              let graphDiv = "boxexpressionView_"+indexDiv
              let boxplotData = [];
              let scatterData = [];
              let nbofvalues  = [];
              let scatterMedianData = [];
              let medofmed    = [];
              let i = 0;
              if(items[indexDiv] in ytitles) {
                // Init with tissues sorted by default or proximity
                var tmpcat = tissues_sort_default;
                if($("#boxexpviewsort").val() == "true") {
                  tmpcat = tissues_sort_proximity
                }
                for(let t of tmpcat) {
                  let tmparr = medianvalues[items[indexDiv]+"_"+t].split(";")
                  let tmpboxplot = getBoxValues(tmparr)
                  tmpboxplot.x = i
                  tmpboxplot.color = tissues_info[t].color
                  tmpboxplot.fillColor = hexToRgba(tissues_info[t].color, 0.1)
                  boxplotData.push(tmpboxplot)
                  nbofvalues[t] = 0
                  tmparr.forEach(function(m){
                    scatterData.push([i, parseFloat(m)])
                    nbofvalues[t] += 1
                  });
                  scatterMedianData.push({
                      x: i,
                      y: medianvalue[items[indexDiv]+"_"+t],
                      color: tissues_info[t].color
                  })
                  medofmed[t] = medianvalue[items[indexDiv]+"_"+t].toFixed(2)
                  i+=1;
                }
                let titletext = null
                let subtitletext = null
                if(items.length === 1) {
                  titletext = 'Expression profiles across tissues'
                  subtitletext = 'GEGA - Version ' + updateVersion
                }
                Highcharts.chart(graphDiv, {
                  chart: {
                    zoomType: 'x',
                    events: {
                      click: function(event) {
                        navigator.clipboard.writeText(items[indexDiv])
                          .then(function() {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>"+items[indexDiv]+"</b>")
                            $('#toast_body').html("Copied to clipboard.")
                            $('#toast').toast('show');
                          })
                          .catch(function(err) {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>Error</b>")
                            $('#toast_body').html("Failed to copy: ", err)
                            $('#toast').toast('show');
                          });
                      }
                    }
                  },
                  title: {
                    text: titletext
                  },
                  subtitle: {
                    text: subtitletext
                  },
                  credits: { enabled: false },
                  legend: { enabled: false },
                  exporting:{ enabled: $("#boxexpviewexport").val() == "true" ? true : false },
                  xAxis: {
                    categories: $("#boxexpviewsort").val() == "true" ? tissues_sort_proximity : tissues_sort_default,
                    labels: {
                      step: 1,
                      formatter: function () {
                        return '<span style="color:'+tissues_info[this.value].color+'">' + this.value + '</span>'
                      }
                    },
                    events: {
                      afterSetExtremes: function(e) {
                        // Same zoom for all boxplot
                        let boxexpgraphids = []
                        $('[id^=boxexpressionView_]').each(function(){
                          boxexpgraphids.push($(this).attr("id"))
                        });
                        async function processBoxExpZoom(items) {
                          for (const item of items) {
                            await new Promise(resolve => setTimeout(resolve, 0));
                            var chart = $('#'+item).highcharts();
                            let curextr = chart.xAxis[0].getExtremes()
                            if(curextr.min != e.min || curextr.max != e.max) {
                              chart.xAxis[0].setExtremes(e.min, e.max);
                            }
                          }
                        }
                        processBoxExpZoom(boxexpgraphids);
                      }
                    }
                  },
                  yAxis: {
                    title: { text: ytitles[items[indexDiv]] },
                    max: $("#boxexpsetmax").val() == "true" ? maxY : null
                  },
                  plotOptions: {
                    series: {
                      pointPadding: 0.1, groupPadding: 0
                    }
                  },
                  tooltip: {
                    enabled: $("#boxexpviewtooltips").val() == "true" ? true : false,
                    useHTML: true,
                    formatter: function() {
                      let text = '<span style="font-size:12px"><b>'+tissues_info[this.key].name+' ('+this.key+')</b></span><table style="width:100%">' +
                        '<tr><td>Number of values : </td>' +
                        '<td style="text-align:right"> <b>'+nbofvalues[this.key]+'</b></td></tr>' +
                        '<tr style="border-bottom:1px solid '+this.color+';"><td style="padding-bottom:5px">Median of medians : </td>' +
                        '<td style="padding-bottom:5px;text-align:right"> <b>'+medofmed[this.key]+'</b></td></tr>' +
                        '<tr><td style="padding-top:5px">Maximum : </td>' +
                        '<td style="padding-top:5px;text-align:right"> <b>'+this.point.high.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Upper quartile : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.q3.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Median : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.median.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Lower quartile : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.q1.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Minimum : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.low.toFixed(2)+'</b></td></tr>' +
                        '</table>';
                      return text
                    }
                  },
                  series: [
                    {
                      type: 'boxplot',
                      data: boxplotData
                    },
                    {
                      name: 'scatterjitter',
                      type: 'scatter',
                      data: scatterData,
                      jitter: { x: 0.15 },
                      marker: { radius: 1 },
                      color: 'rgba(100, 100, 100, 0.6)',
                      enableMouseTracking: false,
                      visible: $("#boxexpjitter").val() == "true" ? true : false
                    },
                    {
                      name: 'Median of median',
                      type: 'scatter',
                      data: scatterMedianData,
                      enableMouseTracking: false
                    }
                  ]
                });
              }
              else {
                $("#boxplotgnId").addClass("form-error");
                let html = `
                  <div class="row" style="display:flex;height:250px;margin:auto">
                    <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                      <i class="bx bx-error" style="color:red;"></i>
                    </div>
                    <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                      <small><b>INPUT ERROR</b><br/>`
                html += "\""+ items[indexDiv] + "\" not found ! <br/>"
                html += '</small></div></div>'
                $('#'+graphDiv).html(html)
              }
            }
          }
          processBuildBoxExpPlot(gnIds)
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
  });

  // Co expression plot
  // Linear / Log
  $('#coexplog').on('hidden.bs.select', function (e) {
    if($("#coexplog").val() == 'logarithmic') {
      $('#coexpviewlinereg').val("false");
      $('#coexpviewlinereg').prop('disabled', true);
      $('#coexpviewlinereg').selectpicker('refresh');
      $('#coexpviewlinereg').trigger('hidden.bs.select');
    }
    else {
      $('#coexpviewlinereg').val("true");
      $('#coexpviewlinereg').prop('disabled', false);
      $('#coexpviewlinereg').selectpicker('refresh');
      $('#coexpviewlinereg').trigger('hidden.bs.select');
    }
    var coexpgraphids = []
    $('[id^=coexpressionView_]').each(function(){
      coexpgraphids.push($(this).attr("id"))
    });
    async function processCoExpLog(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.xAxis[0].update({
          type: $("#coexplog").val() == "linear" ? 'linear' : 'logarithmic',
          min:  $("#coexplog").val() == "linear" ? 0 : null
        });
        chart.yAxis[0].update({
          type: $("#coexplog").val() == "linear" ? 'linear' : 'logarithmic',
          min:  $("#coexplog").val() == "linear" ? 0 : null
        });
      }
    }
    processCoExpLog(coexpgraphids);
  });
  // Display linear regression
  $('#coexpviewlinereg').on('hidden.bs.select', function (e) {
    var coexpgraphids = []
    $('[id^=coexpressionView_]').each(function(){
      coexpgraphids.push($(this).attr("id"))
    });
    async function processCoExpRegression(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        $("#coexpviewlinereg").val() == "true" ? chart.series[1].show() : chart.series[1].hide()
      }
    }
    processCoExpRegression(coexpgraphids);
  });
  // Display Labels
  $('#coexpviewlabels').on('hidden.bs.select', function (e) {
    var coexpgraphids = []
    $('[id^=coexpressionView_]').each(function(){
      coexpgraphids.push($(this).attr("id"))
    });
    async function processCoExpLabels(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        var data = chart.series[0].options.data;
        $.each(data, function(i,point){
          point.dataLabels.enabled = $("#coexpviewlabels").val() == "true" ? true : false
        })
        chart.series[0].update(data);
      }
    }
    processCoExpLabels(coexpgraphids);
  });
  // Display export button
  $('#coexpviewexport').on('hidden.bs.select', function (e) {
    var coexpgraphids = []
    $('[id^=coexpressionView_]').each(function(){
      coexpgraphids.push($(this).attr("id"))
    });
    async function processCoExpButtons(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ exporting: { enabled: $("#coexpviewexport").val() == "true" ? true : false } })
      }
    }
    processCoExpButtons(coexpgraphids);
  });
  // Display tooltips
  $('#coexpviewtooltips').on('hidden.bs.select', function (e) {
    var coexpgraphids = []
    $('[id^=coexpressionView_]').each(function(){
      coexpgraphids.push($(this).attr("id"))
    });
    async function processCoExpTooltips(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ tooltip: { enabled: $("#coexpviewtooltips").val() == "true" ? true : false } })
      }
    }
    processCoExpTooltips(coexpgraphids);
  });
  // Sort plot by input or by correlation
  $('#coexpsort').on('hidden.bs.select', function (e) {
    // Select all div with attr data-corr
    var divs = $('[id^=coexpressionView_]');
    // Sort div using $('#coexpsort').val()
    divs.sort(function(a, b) {
      var corrA = parseFloat($(a).attr($('#coexpsort').val()));
      var corrB = parseFloat($(b).attr($('#coexpsort').val()));
      return corrB - corrA; // sort desc
    });
    // Add sorted div to container
    $.each(divs, function(index, element) {
      $(element).animate({opacity: 0}, 0);
      $(element).appendTo($(element).parent())
        .delay(50 * index) // Add delay
        .animate({opacity: 1}, {
          duration: 1000,
          queue: true,
          complete: function() {
              $(this).dequeue();
          }
        });
    });
  });
  $('#coexphidecorr').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13') {
      // For each div with attr data-corr
      let nbvisible = 0
      $.each($('[id^=coexpressionView_]'), function(index, div) {
        const dataCorr   = parseFloat($(this).attr('data-corr'));
        const inputValue = parseFloat($('#coexphidecorr').val());
        if (dataCorr < inputValue) {
          $(div).fadeOut(500);
        }
        else {
          $(div).fadeIn(500);
          nbvisible += 1
        }
      });
      $('#coexpvisible').text(nbvisible)
    }
  });
  // Build plot(s)
  $('#coexpgnIdX,#coexpgnIdY').keypress(function(event){
    $(this).removeClass("form-error");
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13' && $('#coexpgnIdX').val() !== "" && $('#coexpgnIdY').val() !== "") {
      var otherGenes = $('#coexpgnIdY').val().split(/[\s,]+/)
      // By default sort by input
      $('#coexpsort').val("data-input")
      $('#coexpsort').selectpicker('refresh');
      $('#coexpvisible').text(otherGenes.length)
      $('#coexpnbplot').text(otherGenes.length);
      let h
      let collg
      if(otherGenes.length == 1) {
        h = 825
        collg = 12
      }
      else if(otherGenes.length <=6 ) {
        h = 400
        collg = 6
      }
      else {
        h = 250
        collg = 4
      }
      let html = '<div class="row ml-0 mr-0">'
      for(let i=0; i<otherGenes.length; i++) {
        html += '<div class="col-lg-'+collg+' pl-0 pr-0" id="coexpressionView_'+i+'" data-input="'+(otherGenes.length-i)+'" style="height:'+h+'px"></div>'
      }
      html += '</div>'
      $('#expressionView').html(html)
      $('#expressionView').css("overflow","auto")
      
      // Container for graph
      var graphDiv = ""
      var graphCompleted = 0
      var promisebuildscatter = new Promise(function(resolve, reject) {
        $.each(otherGenes, function(indexDiv, otherGeneId) {
          graphDiv = "coexpressionView_"+indexDiv
          $.ajax({
            url : '/get_expression_for_gnids',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({
              geneIdsX: $('#coexpgnIdX').val(),
              geneIdsY: otherGeneId,
              graphDiv: graphDiv
            }),
            beforeSend: function(){
              $('#'+graphDiv).html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:30%;height:auto;">');
            },
            success: function(d){
              // Save values by key (key = gnId+"_"+tissue)
              let valX = {}
              d.X.forEach(function(v){
                valX[v.gnId+"_"+v.tissue] = v.median
              });
              let valY = {}
              let ytitle = {}
              d.Y.forEach(function(v){
                valY[v.gnId+"_"+v.tissue] = v.median
                ytitle[v.gnId] = v.gnId + "<br>(" +
                  (v.gnName === v.gnId ? "-" : v.gnName) +
                  " / " + v.gnSimpleBiotype +")";
              });
              // If gnIdX and gnIdY found
              if(d.X.length>0 && d.Y.length>0) {
                let gnIdX = d.gnIdX
                let gnIdY = d.gnIdY             
                let data = []
                let arrayOfarray = []
                // To set x and y axis
                let maxX = -Infinity
                let maxY = -Infinity
                let minX = Infinity
                let minY = Infinity
                for(let t in tissues_info) {
                  data.push({
                    x:parseFloat(valX[gnIdX+"_"+t]),
                    y:parseFloat(valY[gnIdY+"_"+t]),
                    name: t,
                    color: tissues_info[t].color,
                    dataLabels: {
                      enabled: $("#coexpviewlabels").val() == "true" ? true : false,
                      format: '{point.name}',
                      color: tissues_info[t].color,
                      style: {
                        textOutline: 'none' 
                      }
                    }
                  })
                  arrayOfarray.push([parseFloat(valX[gnIdX+"_"+t]),parseFloat(valY[gnIdY+"_"+t])])
                  maxX = Math.max(maxX,parseFloat(valX[gnIdX+"_"+t]))
                  maxY = Math.max(maxY,parseFloat(valY[gnIdY+"_"+t]))
                  if(valX[gnIdX+"_"+t] != 0) {minX = Math.min(minX,parseFloat(valX[gnIdX+"_"+t]))}
                  if(valY[gnIdY+"_"+t] != 0) {minY = Math.min(minY,parseFloat(valY[gnIdY+"_"+t]))}
                }
                // Points for linear regression 
                let x = [Math.min(minX, minY)]
                /*for(var k=Math.min(minX, minY)/10; k<Math.max(maxX, maxY)*10; k*=2) {
                  x.push(Math.min(minX, minY)*k)
                }*/
                x.push(Math.max(maxX, maxY))
                let titletext = null
                let subtitletext = null
                if(otherGenes.length === 1) {
                  titletext = 'Co-expression across tissues'
                  subtitletext = 'GEGA - Version ' + updateVersion
                }
                // Get data to plot linearRegression
                $.ajax({
                  url : '/get_linearRegression',
                  type: "POST",
                  contentType: "application/json",
                  data: JSON.stringify({
                    aa : arrayOfarray,
                    x : x
                  }),
                  success: function(d2){
                    // Save correlation in attr of graph container
                    $('#'+d.gd).attr("data-corr", d2.corr)
                    if(d2.corr < parseFloat($('#coexphidecorr').val())) {
                      $('#'+d.gd).fadeOut(500)
                    }
                    Highcharts.chart(d.gd, {
                      chart: {
                        type: 'scatter',
                        zoomType: 'xy',
                        events: {
                          click: function(event) {
                            navigator.clipboard.writeText(gnIdY)
                              .then(function() {
                                $('#toast_icon').html('<i class="bx bx-copy"></i>')
                                $('#toast_title').html("<b>"+gnIdY+"</b>")
                                $('#toast_body').html("Copied to clipboard.")
                                $('#toast').toast('show');
                              })
                              .catch(function(err) {
                                $('#toast_icon').html('<i class="bx bx-copy"></i>')
                                $('#toast_title').html("<b>Error</b>")
                                $('#toast_body').html("Failed to copy: ", err)
                                $('#toast').toast('show');
                              });
                          }
                        }
                      },
                      credits:  { enabled: false },
                      title:    { text: titletext },
                      subtitle: { text: subtitletext },
                      exporting:{ enabled: $("#coexpviewexport").val() == "true" ? true : false },
                      tooltip:  { enabled: $("#coexpviewtooltips").val() == "true" ? true : false },
                      xAxis: {
                        title: {
                          enabled: true,
                          text: gnIdX
                        },
                        startOnTick: true,
                        endOnTick: true,
                        showLastLabel: true,
                        type: $("#coexplog").val() == "linear" ? 'linear' : 'logarithmic',
                        min:  minX, //$("#coexplog").val() == "linear" ? 0 : minX,
                        max:  maxX
                      },
                      yAxis: {
                        title: { text: ytitle[gnIdY] },
                        type: $("#coexplog").val() == "linear" ? 'linear' : 'logarithmic',
                        min:  minY, //$("#coexplog").val() == "linear" ? 0 : minY,
                        max:  maxY
                      },
                      legend: { enabled: false },
                      plotOptions: {
                        scatter: {
                          marker: {
                            radius: 4,
                            states: {
                              hover: {
                                enabled: true,
                                lineColor: 'rgb(100,100,100)'
                              }
                            }
                          },
                          states: {
                            hover: {
                              marker: {
                                enabled: false
                              }
                            }
                          },
                          tooltip: {
                            headerFormat: '',
                            pointFormat: '<b>{point.name}</b><br>'+gnIdX+' : {point.x}<br>'+gnIdY+' : {point.y}'
                          }
                        }
                      },
                      series: [{
                        data: data
                      },
                      {
                        type: 'spline',
                        label: 'Linear regression',
                        data: d2.xy,
                        marker: {
                          enabled: false
                        },
                        states: {
                          hover: {
                            lineWidth: 0
                          }
                        },
                        enableMouseTracking: false,
                        dashStyle: 'dash',
                        opacity: 0.8,
                        visible: $("#coexpviewlinereg").val() == "true" ? true : false
                      }],
                      labels: {
                        items: [{
                          html: '&rho; = '+ d2.corr,
                          style: {
                            left: '10px',
                            top: '10px',
                            fontWeight: 'bold'
                          }
                        }]
                      }
                    });
                  },
                  error : function(e) {
                    console.log("ERROR: ", e);
                  }
                });
              }
              // Else gnIdX or gnIdY not found
              else {
                let html = `
                  <div class="row" style="display:flex;height:250px;margin:auto">
                    <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                      <i class="bx bx-error" style="color:red;"></i>
                    </div>
                    <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                      <small><b>INPUT ERROR</b><br/>`
                if(d.X.length==0 || typeof d.X[0] === "undefined") {
                  html += "\""+ d.gnIdX + "\" not found ! <br/>"
                  $("#coexpgnIdX").addClass("form-error");
                }
                if(d.Y.length==0 && typeof d.Y[0] === "undefined") {
                  html += "\""+ d.gnIdY +"\" not found !"
                  $("#coexpgnIdY").addClass("form-error");
                }
                html += '</small></div></div>'
                $('#'+d.gd).html(html)
              }
            },
            complete : function(){
              graphCompleted += 1
              if (graphCompleted === otherGenes.length) {
                setTimeout(resolve, 1000);
              }
            },
            error : function(e) {
              console.log("ERROR: ", e);
            }
          });
        })
      })
      promisebuildscatter.then(function() {
        let nbvisible = 0
        $.each($('[id^=coexpressionView_]'), function(index, div) {
          const dataCorr   = parseFloat($(this).attr('data-corr'));
          const inputValue = parseFloat($('#coexphidecorr').val());
          if (dataCorr < inputValue) {
            $(div).fadeOut(500);
          }
          else {
            $(div).fadeIn(500);
            nbvisible += 1
          }
        });
        $('#coexpvisible').text(nbvisible)
      });
    }
  });
  

  /*
  ** Expression by AGE
  */
  var ages_info = {}
  // RirF1
  ages_info['09d'] = { color : Highcharts.getOptions().colors[0] }
  ages_info['10w'] = { color : Highcharts.getOptions().colors[1] }
  ages_info['35w'] = { color : Highcharts.getOptions().colors[2] }
  ages_info['45w'] = { color : Highcharts.getOptions().colors[3] }
  //Kaessmann
  ages_info['10e'] = { color : Highcharts.getOptions().colors[0] }
  ages_info['12e'] = { color : Highcharts.getOptions().colors[1] }
  ages_info['14e'] = { color : Highcharts.getOptions().colors[2] }
  ages_info['17e'] = { color : Highcharts.getOptions().colors[3] }
  ages_info['00d'] = { color : Highcharts.getOptions().colors[4] }
  ages_info['07d'] = { color : Highcharts.getOptions().colors[5] }
  ages_info['35d'] = { color : Highcharts.getOptions().colors[6] }
  ages_info['70d'] = { color : Highcharts.getOptions().colors[7] }
  ages_info['22w'] = { color : Highcharts.getOptions().colors[9] }
  // Change project => update tissue options
  $('#selectexpbyage').on('hidden.bs.select', function (e) {
    $('#ageselecttissue option').remove();
    $('#ageselecttissue').selectpicker('refresh');
    $('#ageselecttissue2 option').remove();
    $('#ageselecttissue2').selectpicker('refresh');
    $('#ageselecttissue3 option').remove();
    $('#ageselecttissue3').selectpicker('refresh');
    let tissueoptions = ""
    if($('#selectexpbyage').val()[0] === "Kaessmann") {
      tissueoptions = '<option value="brai" data-content="'+ tissues_info["brai"].name+ ' (brai)"></option>'
        + '<option value="crbl" data-content="'+ tissues_info["crbl"].name+ ' (crbl)"></option>'
        + '<option value="hert" data-content="'+ tissues_info["hert"].name+ ' (hert)"></option>'
        + '<option value="kdny" data-content="'+ tissues_info["kdny"].name+ ' (kdny)"></option>'
        + '<option value="livr" data-content="'+ tissues_info["livr"].name+ ' (livr)"></option>'
        + '<option value="ovry" data-content="'+ tissues_info["ovry"].name+ ' (ovry)"></option>'
        + '<option value="test" data-content="'+ tissues_info["test"].name+ ' (test)"></option>'
    }
    else if($('#selectexpbyage').val()[0] === "RirF1") {
      tissueoptions = //'<option value="adip" data-content="'+ tissues_info["adip"].name+ ' (adip)"></option>'
        //+ '<option value="blod" data-content="'+ tissues_info["blod"].name+ ' (blod)"></option>'
        //+ '<option value="hypt" data-content="'+ tissues_info["hypt"].name+ ' (hypt)"></option>'
        '<option value="livr" data-content="'+ tissues_info["livr"].name+ ' (livr)"></option>'
    }
    $('#ageselecttissue').append(tissueoptions);
    $('#ageselecttissue').selectpicker('refresh');
    $('#ageselecttissue2').append(tissueoptions);
    $('#ageselecttissue2').selectpicker('refresh');
    $('#ageselecttissue3').append(tissueoptions);
    $('#ageselecttissue3').selectpicker('refresh');
  });
  // Age Bar plot
  // Linear / Log
  $('#agebarexplog').on('hidden.bs.select', function (e) {
    var agebarexpgraphids = []
    $('[id^=agebarexpressionView_]').each(function(){
      agebarexpgraphids.push($(this).attr("id"))
    });
    async function processAgeBarExpLog(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.yAxis[0].update({
          type: $("#agebarexplog").val() == "linear" ? 'linear' : 'logarithmic',
          min:  $("#agebarexplog").val() == "linear" ? 0 : 0.1
        });
      }
    }
    processAgeBarExpLog(agebarexpgraphids);
  });
  // Set same max Y
  $('#agebarexpsetmax').on('hidden.bs.select', function (e) {
    var agebarexpgraphids = []
    $('[id^=agebarexpressionView_]').each(function(){
      agebarexpgraphids.push($(this).attr("id"))
    });
    async function processAgeBarExpSetMax(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.yAxis[0].update({
          max: $("#agebarexpsetmax").val() == "true" ? parseFloat($("#agebarexpsetmax").attr('data-maxy')) : null
        });
      }
    }
    processAgeBarExpSetMax(agebarexpgraphids);
  });
  // Display export button
  $('#agebarexpviewexport').on('hidden.bs.select', function (e) {
    var agebarexpgraphids = []
    $('[id^=agebarexpressionView_]').each(function(){
      agebarexpgraphids.push($(this).attr("id"))
    });
    async function processAgeBarExpButtons(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ exporting: { enabled: $("#agebarexpviewexport").val() == "true" ? true : false } })
      }
    }
    processAgeBarExpButtons(agebarexpgraphids);
  });
  // Display tooltips
  $('#agebarexpviewtooltips').on('hidden.bs.select', function (e) {
    var agebarexpgraphids = []
    $('[id^=agebarexpressionView_]').each(function(){
      agebarexpgraphids.push($(this).attr("id"))
    });
    async function processAgeBarExpTooltips(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ tooltip: { enabled: $("#agebarexpviewtooltips").val() == "true" ? true : false } })
      }
    }
    processAgeBarExpTooltips(agebarexpgraphids);
  });
  // Build age bar plot(s)
  $('#agebarplotgnId').keypress(function(event){
    $(this).removeClass("form-error");
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13' && $('#ageselecttissue').val().length != 0) {
      var geneId = $(this).val().split(/[\s,]+/)
      let h = 825
      switch(geneId.length) {
        case 1:
          h = 825
          break
        case 2:
          h = 410
          break
        case 3:
          h = 275
          break
        default:
          h = 200
          break
      }
      let html = '<div class="row ml-0 mr-0">'
      for(let i=0; i<geneId.length; i++) {
        html += '<div class="col-lg-12 pl-0 pr-0" id="agebarexpressionView_'+i+'" style="height:'+h+'px"></div>'
      }
      html += '</div>'
      $('#ageexpressionView').html(html)
      $('#ageexpressionView').css("overflow","auto")

      $.ajax({
        url : '/get_expression_for_gnids_by_project',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          geneIdsX: $('#agebarplotgnId').val(),
          geneIdsY: "none",
          tissue: $('#ageselecttissue').val(),
          project: $('#selectexpbyage').val()[0]
        }),
        beforeSend: function(){
          for(let i=0; i<geneId.length; i++) {
            $('#agebarexpressionView_'+i).html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:22%;height:auto;">');
          }
        },
        success: function(d){
          // Save values by key (key = gnId+"_"+age)
          let val = {}
          let ages = []
          let maxY = 0
          let ytitles = {} // (key = gnId)
          d.X.forEach(function(v){
            if(typeof val[v.gnId+"_"+v.age] === "undefined") {
              val[v.gnId+"_"+v.age] = { 'val': [], 'sex': [], 'conditions': [], 'id': []}
            }
            val[v.gnId+"_"+v.age]['val'].push(v.val)
            val[v.gnId+"_"+v.age]['sex'].push(v.sex)
            val[v.gnId+"_"+v.age]['conditions'].push(v.conditions)
            if(!ages.includes(v.age)) {
              ages.push(v.age)
            }
            maxY = Math.max(maxY, v.val)
            ytitles[v.gnId] = v.gnId + "<br>in " + $('#ageselecttissue').val() + " (" +
              (v.gnName === v.gnId ? "-" : v.gnName) +
              " / " + v.gnSimpleBiotype +")";
          });
          // Save MaxY 
          $('#agebarexpsetmax').attr('data-maxy', maxY)
          let gnIds = $('#agebarplotgnId').val().split(/[\s,]+/)
          async function processBuildAgeBarExpPlot(items) {
            for(let indexDiv=0; indexDiv<items.length; indexDiv++) {
              await new Promise(resolve => setTimeout(resolve, 0));
              // Container for graph
              let graphDiv = "agebarexpressionView_"+indexDiv
              let d = [];
              if(items[indexDiv] in ytitles) {
                let i = 0
                for(let a of ages) {
                  let d2 = []
                  for(let j=0; j<val[items[indexDiv]+"_"+a]['val'].length; j++) {
                    d2.push({
                      x : i,
                      y :   val[items[indexDiv]+"_"+a]['val'][j],
                      sex : val[items[indexDiv]+"_"+a]['sex'][j],
                      conditions : val[items[indexDiv]+"_"+a]['conditions'][j],
                      id : val[items[indexDiv]+"_"+a]['id'][j],
                      color : ages_info[a].color
                    })
                  }
                  d.push(d2)
                  i += 1
                }
                let dtranspose = d.reduce((acc, ligne) => { ligne.forEach((element, index) => {if (!acc[index]) {acc[index] = [];}acc[index] = acc[index].concat(element);});return acc;}, [])
                let data = []
                for (let i in dtranspose) {
                  data.push({data: dtranspose[i]})
                }
                let titletext = null
                let subtitletext = null
                if(items.length === 1) {
                  titletext = 'Expression profiles by age in ' + $('#ageselecttissue').val() + ' (project ' + $('#selectexpbyage').val()[0] + ')'
                  subtitletext = 'GEGA - Version ' + updateVersion
                }
                Highcharts.chart(graphDiv, {
                  chart: {
                    type: 'column',
                    events: {
                      click: function(event) {
                        navigator.clipboard.writeText(items[indexDiv])
                          .then(function() {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>"+items[indexDiv]+"</b>")
                            $('#toast_body').html("Copied to clipboard.")
                            $('#toast').toast('show');
                          })
                          .catch(function(err) {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>Error</b>")
                            $('#toast_body').html("Failed to copy: ", err)
                            $('#toast').toast('show');
                          });
                      }
                    }
                  },
                  title: {
                    text: titletext
                  },
                  subtitle: {
                    text: subtitletext
                  },
                  credits: { enabled: false },
                  legend: { enabled: false },
                  exporting:{ enabled: $("#agebarexpviewexport").val() == "true" ? true : false },
                  xAxis: {
                    categories: ages,
                    labels: {
                      step: 1,
                      formatter: function () {
                        return '<span style="color:'+ages_info[this.value].color+'">' + this.value + '</span>'
                      }
                    }
                  },
                  yAxis: {
                    title: { text: ytitles[items[indexDiv]] },
                    type: $("#agebarexplog").val() == "linear" ? 'linear' : 'logarithmic',
                    min: $("#agebarexplog").val() == "linear" ? 0 : 0.1,
                    max: $("#agebarexpsetmax").val() == "true" ? maxY : null
                  },
                  plotOptions: {
                    series: {
                      pointPadding: 0 // groupPadding: 0
                    },
                    column: {
                      centerInCategory: true
                    }
                  },
                  tooltip: {
                    headerFormat: '<span style="font-size:12px"><b>{point.key}</b></span><table>' +
                      '<tr style="text-align:center"><td><em>Sex</em></td><td style="padding-left:5px;padding-right:5px"><em>Condition</em></td><td><em>Value</em></td></tr>',
                    pointFormat: '<tr>' +
                      '<td style="padding:0;text-align:center">{point.sex}</td>' +
                      '<td style="padding:0;text-align:center">{point.conditions}</td>' +
                      '<td style="padding:0;padding-left:5px;text-align:right"> <b>{point.y:.2f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true,
                    enabled: $("#agebarexpviewtooltips").val() == "true" ? true : false
                  },
                  series: data
                });
              }
              else {
                $("#agebarplotgnId").addClass("form-error");
                let html = `
                  <div class="row" style="display:flex;height:250px;margin:auto">
                    <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                      <i class="bx bx-error" style="color:red;"></i>
                    </div>
                    <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                      <small><b>INPUT ERROR</b><br/>`
                html += "\""+ items[indexDiv] + "\" not found ! <br/>"
                html += '</small></div></div>'
                $('#'+graphDiv).html(html)
              }
            }
          }
          processBuildAgeBarExpPlot(gnIds)
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
  });

  // Age Box plot (expression)
  // Change group by "tissue"
  $("#ageboxplotorderby").on('hidden.bs.select', function () {
    $('#ageboxplotgnId').trigger($.Event('keypress', { keyCode: 13 }));
  });
  // Set same max Y
  $('#ageboxexpsetmax').on('hidden.bs.select', function (e) {
    var ageboxexpgraphids = []
    $('[id^=ageboxexpressionView_]').each(function(){
      ageboxexpgraphids.push($(this).attr("id"))
    });
    async function processAgeBoxExpSetMax(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.yAxis[0].update({
          max: $("#ageboxexpsetmax").val() == "true" ? parseFloat($("#ageboxexpsetmax").attr('data-maxy')) : null
        });
      }
    }
    processAgeBoxExpSetMax(ageboxexpgraphids);
  });
  // Display export button
  $('#ageboxexpviewexport').on('hidden.bs.select', function (e) {
    var ageboxexpgraphids = []
    $('[id^=ageboxexpressionView_]').each(function(){
      ageboxexpgraphids.push($(this).attr("id"))
    });
    async function processAgeBoxExpButtons(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ exporting: { enabled: $("#ageboxexpviewexport").val() == "true" ? true : false } })
      }
    }
    processAgeBoxExpButtons(ageboxexpgraphids);
  });
  // Display tooltips
  $('#ageboxexpviewtooltips').on('hidden.bs.select', function (e) {
    var ageboxexpgraphids = []
    $('[id^=ageboxexpressionView_]').each(function(){
      ageboxexpgraphids.push($(this).attr("id"))
    });
    async function processAgeBoxExpTooltips(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ tooltip: { enabled: $("#ageboxexpviewtooltips").val() == "true" ? true : false } })
      }
    }
    processAgeBoxExpTooltips(ageboxexpgraphids);
  });
  // Build boxplot
  $('#ageboxplotgnId').keypress(function(event){
    $(this).removeClass("form-error");
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13' && $('#ageselecttissue2').val().length != 0) {
      var geneId = $(this).val().split(/[\s,]+/)
      let h = 825
      switch(geneId.length) {
        case 1:
          h = 825
          break
        case 2:
          h = 410
          break
        case 3:
          h = 275
          break
        default:
          h = 200
          break
      }
      let html = '<div class="row ml-0 mr-0">'
      for(let i=0; i<geneId.length; i++) {
        html += '<div class="col-lg-12 pl-0 pr-0" id="ageboxexpressionView_'+i+'" style="height:'+h+'px"></div>'
      }
      html += '</div>'
      $('#ageexpressionView').html(html)
      $('#ageexpressionView').css("overflow","auto")

      $.ajax({
        url : '/get_expressions_for_gnids_boxplot_by_project',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          geneIds: $('#ageboxplotgnId').val(),
          tissue: $('#ageselecttissue2').val(),
          project: $('#selectexpbyage').val()[0],
          orderby: $("#ageboxplotorderby").val() == "true" ? 'tissueage' : 'agetissue'
        }),
        beforeSend: function(){
          for(let i=0; i<geneId.length; i++) {
            $('#ageboxexpressionView_'+i).html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:22%;height:auto;">');
          }
        },
        success: function(d){
          // Save values by key (key = gnId+"_"+age+"_"+tissue)
          let vals = {}
          let ages = []
          let maxY = 0
          let ytitles = {} // (key = gnId)
          d.forEach(function(v){
            vals[v.gnId+"_"+v.age+"_"+v.tissue] = v.vals
            if(!ages.includes(v.age+"_"+v.tissue)) {
              ages.push(v.age+"_"+v.tissue)
            }
            maxY = Math.max(Math.max(...v.vals.split(";")), maxY)
            ytitles[v.gnId] = v.gnId + "<br>(" +
              (v.gnName === v.gnId ? "-" : v.gnName) +
              " / " + v.gnSimpleBiotype +")";
          });
          // Save MaxY 
          $('#ageboxexpsetmax').attr('data-maxy', maxY)
          let gnIds = $('#ageboxplotgnId').val().split(/[\s,]+/)
          async function processBuildAgeBoxExpPlot(items) {
            for(let indexDiv=0; indexDiv<items.length; indexDiv++) {
              await new Promise(resolve => setTimeout(resolve, 0));
              // Container for graph
              let graphDiv = "ageboxexpressionView_"+indexDiv
              let boxplotData = [];
              let scatterData = [];
              let nbofvalues  = [];
              let i = 0;
              if(items[indexDiv] in ytitles) {
                for(let a of ages) {
                  let tmparr = vals[items[indexDiv]+"_"+a].split(";")
                  let tmpboxplot = getBoxValues(tmparr.sort((a, b) => a - b))
                  tmpboxplot.x = i
                  tmpboxplot.color = ages_info[a.split("_")[0]].color
                  tmpboxplot.fillColor = hexToRgba(ages_info[a.split("_")[0]].color, 0.1)
                  boxplotData.push(tmpboxplot)
                  nbofvalues[a] = 0
                  tmparr.forEach(function(m){
                    scatterData.push([i, parseFloat(m)])
                    nbofvalues[a] += 1
                  });
                  i+=1;
                }
                let titletext = null
                let subtitletext = null
                if(items.length === 1) {
                  titletext = 'Expression profiles by age (project ' + $('#selectexpbyage').val()[0] + ')'
                  subtitletext = 'GEGA - Version ' + updateVersion
                }
                Highcharts.chart(graphDiv, {
                  chart: {
                    zoomType: 'x',
                    events: {
                      click: function(event) {
                        navigator.clipboard.writeText(items[indexDiv])
                          .then(function() {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>"+items[indexDiv]+"</b>")
                            $('#toast_body').html("Copied to clipboard.")
                            $('#toast').toast('show');
                          })
                          .catch(function(err) {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>Error</b>")
                            $('#toast_body').html("Failed to copy: ", err)
                            $('#toast').toast('show');
                          });
                      }
                    }
                  },
                  title: {
                    text: titletext
                  },
                  subtitle: {
                    text: subtitletext
                  },
                  credits: { enabled: false },
                  legend: { enabled: false },
                  exporting:{ enabled: $("#ageboxexpviewexport").val() == "true" ? true : false },
                  xAxis: {
                    categories: ages,
                    labels: {
                      step: 1,
                      formatter: function () {
                        let a = this.value.split("_")[0]
                        let t = this.value.split("_")[1]
                        return '<span style="color:'+ages_info[a].color+'">' + a + ' ' + t + '</span>'
                      }
                    },
                    events: {
                      afterSetExtremes: function(e) {
                        // Same zoom for all boxplot
                        let ageboxexpgraphids = []
                        $('[id^=ageboxexpressionView_]').each(function(){
                          ageboxexpgraphids.push($(this).attr("id"))
                        });
                        async function processAgeBoxExpZoom(items) {
                          for (const item of items) {
                            await new Promise(resolve => setTimeout(resolve, 0));
                            var chart = $('#'+item).highcharts();
                            let curextr = chart.xAxis[0].getExtremes()
                            if(curextr.min != e.min || curextr.max != e.max) {
                              chart.xAxis[0].setExtremes(e.min, e.max);
                            }
                          }
                        }
                        processAgeBoxExpZoom(ageboxexpgraphids);
                      }
                    }
                  },
                  yAxis: {
                    title: { text: ytitles[items[indexDiv]] },
                    max: $("#ageboxexpsetmax").val() == "true" ? maxY : null
                  },
                  plotOptions: {
                    series: {
                      pointPadding: 0.1, groupPadding: 0
                    }
                  },
                  tooltip: {
                    enabled: $("#ageboxexpviewtooltips").val() == "true" ? true : false,
                    useHTML: true,
                    formatter: function() {
                      let text = '<span style="font-size:12px"><b>'+this.key+'</b></span><table style="width:100%">' +
                        '<tr style="border-bottom:1px solid '+this.color+';"><td style="padding-bottom:5px;">Number of values : </td>' +
                        '<td style="padding-bottom:5px;text-align:right"> <b>'+nbofvalues[this.key]+'</b></td></tr>' +
                        '<tr><td style="padding-top:5px">Maximum : </td>' +
                        '<td style="padding-top:5px;text-align:right"> <b>'+this.point.high.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Upper quartile : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.q3.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Median : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.median.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Lower quartile : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.q1.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Minimum : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.low.toFixed(2)+'</b></td></tr>' +
                        '</table>';
                      return text
                    }
                  },
                  series: [
                    {
                      type: 'boxplot',
                      data: boxplotData
                    },
                    {
                      name: 'Value',
                      type: 'scatter',
                      data: scatterData,
                      jitter: { x: 0.15 },
                      marker: { radius: 1 },
                      color: 'rgba(100, 100, 100, 0.6)',
                      enableMouseTracking: false
                    }
                  ]
                });
              }
              else {
                $("#ageboxplotgnId").addClass("form-error");
                let html = `
                  <div class="row" style="display:flex;height:250px;margin:auto">
                    <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                      <i class="bx bx-error" style="color:red;"></i>
                    </div>
                    <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                      <small><b>INPUT ERROR</b><br/>`
                html += "\""+ items[indexDiv] + "\" not found ! <br/>"
                html += '</small></div></div>'
                $('#'+graphDiv).html(html)
              }
            }
          }
          processBuildAgeBoxExpPlot(gnIds)
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
  });

  // Age co expression plot
  // Linear / Log
  $('#agecoexplog').on('hidden.bs.select', function (e) {
    if($("#agecoexplog").val() == 'logarithmic') {
      $('#agecoexpviewlinereg').val("false");
      $('#agecoexpviewlinereg').prop('disabled', true);
      $('#agecoexpviewlinereg').selectpicker('refresh');
      $('#agecoexpviewlinereg').trigger('hidden.bs.select');
    }
    else {
      $('#agecoexpviewlinereg').val("true");
      $('#agecoexpviewlinereg').prop('disabled', false);
      $('#agecoexpviewlinereg').selectpicker('refresh');
      $('#agecoexpviewlinereg').trigger('hidden.bs.select');
    }
    var agecoexpgraphids = []
    $('[id^=agecoexpressionView_]').each(function(){
      agecoexpgraphids.push($(this).attr("id"))
    });
    async function processAgeCoExpLog(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.xAxis[0].update({
          type: $("#agecoexplog").val() == "linear" ? 'linear' : 'logarithmic',
          min:  $("#agecoexplog").val() == "linear" ? 0 : null
        });
        chart.yAxis[0].update({
          type: $("#agecoexplog").val() == "linear" ? 'linear' : 'logarithmic',
          min:  $("#agecoexplog").val() == "linear" ? 0 : null
        });
      }
    }
    processAgeCoExpLog(agecoexpgraphids);
  });
  // Display linear regression
  $('#agecoexpviewlinereg').on('hidden.bs.select', function (e) {
    var agecoexpgraphids = []
    $('[id^=agecoexpressionView_]').each(function(){
      agecoexpgraphids.push($(this).attr("id"))
    });
    async function processAgeCoExpRegression(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        $("#agecoexpviewlinereg").val() == "true" ? chart.series[1].show() : chart.series[1].hide()
      }
    }
    processAgeCoExpRegression(agecoexpgraphids);
  });
  // Display Labels
  $('#agecoexpviewlabels').on('hidden.bs.select', function (e) {
    var agecoexpgraphids = []
    $('[id^=agecoexpressionView_]').each(function(){
      agecoexpgraphids.push($(this).attr("id"))
    });
    async function processAgeCoExpLabels(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        var data = chart.series[0].options.data;
        $.each(data, function(i,point){
          point.dataLabels.enabled = $("#agecoexpviewlabels").val() == "true" ? true : false
        })
        chart.series[0].update(data);
      }
    }
    processAgeCoExpLabels(agecoexpgraphids);
  });
  // Display export button
  $('#agecoexpviewexport').on('hidden.bs.select', function (e) {
    var agecoexpgraphids = []
    $('[id^=agecoexpressionView_]').each(function(){
      agecoexpgraphids.push($(this).attr("id"))
    });
    async function processAgeCoExpButtons(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ exporting: { enabled: $("#agecoexpviewexport").val() == "true" ? true : false } })
      }
    }
    processAgeCoExpButtons(agecoexpgraphids);
  });
  // Display tooltips
  $('#agecoexpviewtooltips').on('hidden.bs.select', function (e) {
    var agecoexpgraphids = []
    $('[id^=agecoexpressionView_]').each(function(){
      agecoexpgraphids.push($(this).attr("id"))
    });
    async function processAgeCoExpTooltips(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ tooltip: { enabled: $("#agecoexpviewtooltips").val() == "true" ? true : false } })
      }
    }
    processAgeCoExpTooltips(agecoexpgraphids);
  });
  // Sort plot by input or by correlation
  $('#agecoexpsort').on('hidden.bs.select', function (e) {
    // Select all div with attr data-corr
    var divs = $('[id^=agecoexpressionView_]');
    // Sort div using $('#coexpsort').val()
    divs.sort(function(a, b) {
      var corrA = parseFloat($(a).attr($('#agecoexpsort').val()));
      var corrB = parseFloat($(b).attr($('#agecoexpsort').val()));
      return corrB - corrA; // sort desc
    });
    // Add sorted div to container
    $.each(divs, function(index, element) {
      $(element).animate({opacity: 0}, 0);
      $(element).appendTo($(element).parent())
        .delay(50 * index) // Add delay
        .animate({opacity: 1}, {
          duration: 1000,
          queue: true,
          complete: function() {
              $(this).dequeue();
          }
        });
    });
  });
  $('#agecoexphidecorr').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13') {
      // For each div with attr data-corr
      let nbvisible = 0
      $.each($('[id^=agecoexpressionView_]'), function(index, div) {
        const dataCorr   = parseFloat($(this).attr('data-corr'));
        const inputValue = parseFloat($('#agecoexphidecorr').val());
        if (dataCorr < inputValue) {
          $(div).fadeOut(500);
        }
        else {
          $(div).fadeIn(500);
          nbvisible += 1
        }
      });
      $('#agecoexpvisible').text(nbvisible)
    }
  });
  // Build plot(s)
  $('#agecoexpgnIdX,#agecoexpgnIdY').keypress(function(event){
    $(this).removeClass("form-error");
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13' && $('#ageselecttissue3').val().length != 0 && $('#agecoexpgnIdX').val() !== "" && $('#agecoexpgnIdY').val() !== "") {
      var otherGenes = $('#agecoexpgnIdY').val().split(/[\s,]+/)
      // By default sort by input
      $('#agecoexpsort').val("data-input")
      $('#agecoexpsort').selectpicker('refresh');
      $('#agecoexpvisible').text(otherGenes.length)
      $('#agecoexpnbplot').text(otherGenes.length);
      let h
      let collg
      if(otherGenes.length == 1) {
        h = 825
        collg = 12
      }
      else if(otherGenes.length <=6 ) {
        h = 400
        collg = 6
      }
      else {
        h = 250
        collg = 4
      }
      let html = '<div class="row ml-0 mr-0">'
      for(let i=0; i<otherGenes.length; i++) {
        html += '<div class="col-lg-'+collg+' pl-0 pr-0" id="agecoexpressionView_'+i+'" data-input="'+(otherGenes.length-i)+'" style="height:'+h+'px"></div>'
      }
      html += '</div>'
      $('#ageexpressionView').html(html)
      $('#ageexpressionView').css("overflow","auto")
      
      // Container for graph
      var graphDiv = ""
      var graphCompleted = 0
      var promisebuildagescatter = new Promise(function(resolve, reject) {
        $.each(otherGenes, function(indexDiv, otherGeneId) {
          graphDiv = "agecoexpressionView_"+indexDiv
          $.ajax({
            url : '/get_expression_for_gnids_by_project',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({
              geneIdsX: $('#agecoexpgnIdX').val(),
              geneIdsY: otherGeneId,
              tissue: $('#ageselecttissue3').val(),
              project: $('#selectexpbyage').val()[0],
              graphDiv: graphDiv
            }),
            beforeSend: function(){
              $('#'+graphDiv).html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:30%;height:auto;">');
            },
            success: function(d){
              // Save values by key (key = gnId+"_"+age)
              let valX = {}
              let ages = []
              d.X.forEach(function(v){
                if(typeof valX[v.gnId+"_"+v.age] === "undefined") {
                  valX[v.gnId+"_"+v.age] = { 'val': [] }
                }
                valX[v.gnId+"_"+v.age]['val'].push(v.val)
                if(!ages.includes(v.age)) {
                  ages.push(v.age)
                }
              });
              let valY = {}
              let ytitle = {}
              d.Y.forEach(function(v){
                if(typeof valY[v.gnId+"_"+v.age] === "undefined") {
                  valY[v.gnId+"_"+v.age] = { 'val': [] }
                }
                valY[v.gnId+"_"+v.age]['val'].push(v.val)
                ytitle[v.gnId] = v.gnId + "<br>in " + $('#ageselecttissue3').val() + " (" +
                  (v.gnName === v.gnId ? "-" : v.gnName) +
                  " / " + v.gnSimpleBiotype +")";
              });
              // If gnIdX and gnIdY found
              if(d.X.length>0 && d.Y.length>0) {
                let gnIdX = d.gnIdX
                let gnIdY = d.gnIdY             
                let data = []
                let arrayOfarray = []
                // To set x and y axis
                let maxX = -Infinity
                let maxY = -Infinity
                let minX = Infinity
                let minY = Infinity
                for(let a of ages) {
                  for(let j=0; j<valX[gnIdX+"_"+a]['val'].length; j++) {
                    data.push({
                      x:parseFloat(valX[gnIdX+"_"+a]['val'][j]),
                      y:parseFloat(valY[gnIdY+"_"+a]['val'][j]),
                      name: a,
                      color: ages_info[a].color,
                      dataLabels: {
                        enabled: $("#agecoexpviewlabels").val() == "true" ? true : false,
                        format: '{point.name}',
                        color: ages_info[a].color,
                        style: {
                          textOutline: 'none' 
                        }
                      }
                    })
                    arrayOfarray.push([parseFloat(valX[gnIdX+"_"+a]['val'][j]),parseFloat(valY[gnIdY+"_"+a]['val'][j])])
                    maxX = Math.max(maxX,parseFloat(valX[gnIdX+"_"+a]['val'][j]))
                    maxY = Math.max(maxY,parseFloat(valY[gnIdY+"_"+a]['val'][j]))
                    if(valX[gnIdX+"_"+a] != 0) {minX = Math.min(minX,parseFloat(valX[gnIdX+"_"+a]['val'][j]))}
                    if(valY[gnIdY+"_"+a] != 0) {minY = Math.min(minY,parseFloat(valY[gnIdY+"_"+a]['val'][j]))}
                  }
                }
                // Points for linear regression 
                let x = [Math.min(minX, minY)]
                /*for(var k=Math.min(minX, minY)/10; k<Math.max(maxX, maxY)*10; k*=2) {
                  x.push(Math.min(minX, minY)*k)
                }*/
                x.push(Math.max(maxX, maxY))
                let titletext = null
                let subtitletext = null
                if(otherGenes.length === 1) {
                  titletext = 'Co-expression by age in ' + $('#ageselecttissue3').val() + ' (project ' + $('#selectexpbyage').val()[0] + ')'
                  subtitletext = 'GEGA - Version ' + updateVersion
                }
                // Get data to plot linearRegression
                $.ajax({
                  url : '/get_linearRegression',
                  type: "POST",
                  contentType: "application/json",
                  data: JSON.stringify({
                    aa : arrayOfarray,
                    x : x
                  }),
                  success: function(d2){
                    // Save correlation in attr of graph container
                    $('#'+d.gd).attr("data-corr", d2.corr)
                    if(d2.corr < parseFloat($('#agecoexphidecorr').val())) {
                      $('#'+d.gd).fadeOut(500)
                    }
                    Highcharts.chart(d.gd, {
                      chart: {
                        type: 'scatter',
                        zoomType: 'xy',
                        events: {
                          click: function(event) {
                            navigator.clipboard.writeText(gnIdY)
                              .then(function() {
                                $('#toast_icon').html('<i class="bx bx-copy"></i>')
                                $('#toast_title').html("<b>"+gnIdY+"</b>")
                                $('#toast_body').html("Copied to clipboard.")
                                $('#toast').toast('show');
                              })
                              .catch(function(err) {
                                $('#toast_icon').html('<i class="bx bx-copy"></i>')
                                $('#toast_title').html("<b>Error</b>")
                                $('#toast_body').html("Failed to copy: ", err)
                                $('#toast').toast('show');
                              });
                          }
                        }
                      },
                      credits:  { enabled: false },
                      title:    { text: titletext },
                      subtitle: { text: subtitletext },
                      exporting:{ enabled: $("#agecoexpviewexport").val() == "true" ? true : false },
                      tooltip:  { enabled: $("#agecoexpviewtooltips").val() == "true" ? true : false },
                      xAxis: {
                        title: {
                          enabled: true,
                          text: gnIdX
                        },
                        startOnTick: true,
                        endOnTick: true,
                        showLastLabel: true,
                        type: $("#agecoexplog").val() == "linear" ? 'linear' : 'logarithmic',
                        min:  minX, //$("#agecoexplog").val() == "linear" ? 0 : minX,
                        max:  maxX
                      },
                      yAxis: {
                        title: { text: ytitle[gnIdY] },
                        type: $("#agecoexplog").val() == "linear" ? 'linear' : 'logarithmic',
                        min:  minY, //$("#agecoexplog").val() == "linear" ? 0 : minY,
                        max:  maxY
                      },
                      legend: { enabled: false },
                      plotOptions: {
                        scatter: {
                          marker: {
                            radius: 4,
                            states: {
                              hover: {
                                enabled: true,
                                lineColor: 'rgb(100,100,100)'
                              }
                            }
                          },
                          states: {
                            hover: {
                              marker: {
                                enabled: false
                              }
                            }
                          },
                          tooltip: {
                            headerFormat: '',
                            pointFormat: '<b>{point.name}</b><br>'+gnIdX+' : {point.x}<br>'+gnIdY+' : {point.y}'
                          }
                        }
                      },
                      series: [{
                        data: data
                      },
                      {
                        type: 'spline',
                        label: 'Linear regression',
                        data: d2.xy,
                        marker: {
                          enabled: false
                        },
                        states: {
                          hover: {
                            lineWidth: 0
                          }
                        },
                        enableMouseTracking: false,
                        dashStyle: 'dash',
                        opacity: 0.8,
                        visible: $("#agecoexpviewlinereg").val() == "true" ? true : false
                      }],
                      labels: {
                        items: [{
                          html: '&rho; = '+ d2.corr,
                          style: {
                            left: '10px',
                            top: '10px',
                            fontWeight: 'bold'
                          }
                        }]
                      }
                    });
                  },
                  error : function(e) {
                    console.log("ERROR: ", e);
                  }
                });
              }
              // Else gnIdX or gnIdY not found
              else {
                let html = `
                  <div class="row" style="display:flex;height:250px;margin:auto">
                    <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                      <i class="bx bx-error" style="color:red;"></i>
                    </div>
                    <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                      <small><b>INPUT ERROR</b><br/>`
                if(d.X.length==0 || typeof d.X[0] === "undefined") {
                  html += "\""+ d.gnIdX + "\" not found ! <br/>"
                  $("#agecoexpgnIdX").addClass("form-error");
                }
                if(d.Y.length==0 && typeof d.Y[0] === "undefined") {
                  html += "\""+ d.gnIdY +"\" not found !"
                  $("#agecoexpgnIdY").addClass("form-error");
                }
                html += '</small></div></div>'
                $('#'+d.gd).html(html)
              }
            },
            complete : function(){
              graphCompleted += 1
              if (graphCompleted === otherGenes.length) {
                setTimeout(resolve, 1000);
              }
            },
            error : function(e) {
              console.log("ERROR: ", e);
            }
          });
        })
      })
      promisebuildagescatter.then(function() {
        let nbvisible = 0
        $.each($('[id^=agecoexpressionView_]'), function(index, div) {
          const dataCorr   = parseFloat($(this).attr('data-corr'));
          const inputValue = parseFloat($('#agecoexphidecorr').val());
          if (dataCorr < inputValue) {
            $(div).fadeOut(500);
          }
          else {
            $(div).fadeIn(500);
            nbvisible += 1
          }
        });
        $('#agecoexpvisible').text(nbvisible)
      });
    }
  });


  /*
  ** Expression by SEX
  */
  var sexs_info = {}
  sexs_info['F'] = { color : Highcharts.getOptions().colors[5] }
  sexs_info['M'] = { color : Highcharts.getOptions().colors[0] }
  // Change project => update tissue options
  $('#selectexpbysex').on('hidden.bs.select', function (e) {
    $('#sexselecttissue option').remove();
    $('#sexselecttissue').selectpicker('refresh');
    $('#sexselecttissue2 option').remove();
    $('#sexselecttissue2').selectpicker('refresh');
    $('#sexselecttissue3 option').remove();
    $('#sexselecttissue3').selectpicker('refresh');
    let tissueoptions = ""
    if($('#selectexpbysex').val()[0] === "FrAgENCODE") {
      tissueoptions = '<option value="crbl" data-content="'+ tissues_info["crbl"].name+ ' (crbl)"></option>'
        + '<option value="duod" data-content="'+ tissues_info["duod"].name+ ' (duod)"></option>'
        + '<option value="gizz" data-content="'+ tissues_info["gizz"].name+ ' (gizz)"></option>'
        + '<option value="ileu" data-content="'+ tissues_info["ileu"].name+ ' (ileu)"></option>'
        + '<option value="kdny" data-content="'+ tissues_info["kdny"].name+ ' (kdny)"></option>'
        + '<option value="livr" data-content="'+ tissues_info["livr"].name+ ' (livr)"></option>'
        + '<option value="lung" data-content="'+ tissues_info["lung"].name+ ' (lung)"></option>'
        + '<option value="lymT" data-content="'+ tissues_info["lymT"].name+ ' (lymT)"></option>'
        + '<option value="mscB" data-content="'+ tissues_info["mscB"].name+ ' (mscB)"></option>'
        + '<option value="test" data-content="'+ tissues_info["test"].name+ ' (test)"></option>'
    }
    else if($('#selectexpbysex').val()[0] === "RirF1") {
      tissueoptions = '<option value="adip" data-content="'+ tissues_info["adip"].name+ ' (adip)"></option>'
        + '<option value="blod" data-content="'+ tissues_info["blod"].name+ ' (blod)"></option>'
        + '<option value="hypt" data-content="'+ tissues_info["hypt"].name+ ' (hypt)"></option>'
        + '<option value="livr" data-content="'+ tissues_info["livr"].name+ ' (livr)"></option>'
    }
    $('#sexselecttissue').append(tissueoptions);
    $('#sexselecttissue').selectpicker('refresh');
    $('#sexselecttissue2').append(tissueoptions);
    $('#sexselecttissue2').selectpicker('refresh');
    $('#sexselecttissue3').append(tissueoptions);
    $('#sexselecttissue3').selectpicker('refresh');
  });
  // Sex Bar plot
  // Linear / Log
  $('#sexbarexplog').on('hidden.bs.select', function (e) {
    var sexbarexpgraphids = []
    $('[id^=sexbarexpressionView_]').each(function(){
      sexbarexpgraphids.push($(this).attr("id"))
    });
    async function processSexBarExpLog(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.yAxis[0].update({
          type: $("#sexbarexplog").val() == "linear" ? 'linear' : 'logarithmic',
          min:  $("#sexbarexplog").val() == "linear" ? 0 : 0.1
        });
      }
    }
    processSexBarExpLog(sexbarexpgraphids);
  });
  // Set same max Y
  $('#sexbarexpsetmax').on('hidden.bs.select', function (e) {
    var sexbarexpgraphids = []
    $('[id^=sexbarexpressionView_]').each(function(){
      sexbarexpgraphids.push($(this).attr("id"))
    });
    async function processSexBarExpSetMax(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.yAxis[0].update({
          max: $("#sexbarexpsetmax").val() == "true" ? parseFloat($("#sexbarexpsetmax").attr('data-maxy')) : null
        });
      }
    }
    processSexBarExpSetMax(sexbarexpgraphids);
  });
  // Display export button
  $('#sexbarexpviewexport').on('hidden.bs.select', function (e) {
    var sexbarexpgraphids = []
    $('[id^=sexbarexpressionView_]').each(function(){
      sexbarexpgraphids.push($(this).attr("id"))
    });
    async function processSexBarExpButtons(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ exporting: { enabled: $("#sexbarexpviewexport").val() == "true" ? true : false } })
      }
    }
    processSexBarExpButtons(sexbarexpgraphids);
  });
  // Display tooltips
  $('#sexbarexpviewtooltips').on('hidden.bs.select', function (e) {
    var sexbarexpgraphids = []
    $('[id^=sexbarexpressionView_]').each(function(){
      sexbarexpgraphids.push($(this).attr("id"))
    });
    async function processSexBarExpTooltips(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ tooltip: { enabled: $("#sexbarexpviewtooltips").val() == "true" ? true : false } })
      }
    }
    processSexBarExpTooltips(sexbarexpgraphids);
  });
  // Build sex bar plot(s)
  $('#sexbarplotgnId').keypress(function(event){
    $(this).removeClass("form-error");
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13' && $('#sexselecttissue').val().length != 0) {
      var geneId = $(this).val().split(/[\s,]+/)
      let h = 825
      switch(geneId.length) {
        case 1:
          h = 825
          break
        case 2:
          h = 410
          break
        case 3:
          h = 275
          break
        default:
          h = 200
          break
      }
      let html = '<div class="row ml-0 mr-0">'
      for(let i=0; i<geneId.length; i++) {
        html += '<div class="col-lg-12 pl-0 pr-0" id="sexbarexpressionView_'+i+'" style="height:'+h+'px"></div>'
      }
      html += '</div>'
      $('#sexexpressionView').html(html)
      $('#sexexpressionView').css("overflow","auto")

      $.ajax({
        url : '/get_expression_for_gnids_by_project',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          geneIdsX: $('#sexbarplotgnId').val(),
          geneIdsY: "none",
          tissue: $('#sexselecttissue').val(),
          project: $('#selectexpbysex').val()[0]
        }),
        beforeSend: function(){
          for(let i=0; i<geneId.length; i++) {
            $('#sexbarexpressionView_'+i).html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:22%;height:auto;">');
          }
        },
        success: function(d){
            // Save values by key (key = gnId+"_"+sex)
          let val = {}
          let sexs = []
          let maxY = 0
          let ytitles = {} // (key = gnId)
          d.X.forEach(function(v){
            if(typeof val[v.gnId+"_"+v.sex] === "undefined") {
              val[v.gnId+"_"+v.sex] = { 'val': [], 'ages': [], 'conditions': []}
            }
            val[v.gnId+"_"+v.sex]['val'].push(v.val)
            val[v.gnId+"_"+v.sex]['ages'].push(v.age)
            val[v.gnId+"_"+v.sex]['conditions'].push(v.conditions)
            if(!sexs.includes(v.sex)) {
              sexs.push(v.sex)
            }
            maxY = Math.max(maxY, v.val)
            ytitles[v.gnId] = v.gnId + "<br>in " + $('#sexselecttissue').val() + " (" +
              (v.gnName === v.gnId ? "-" : v.gnName) +
              " / " + v.gnSimpleBiotype +")";
          });
          // Save MaxY 
          $('#sexbarexpsetmax').attr('data-maxy', maxY)
          let gnIds = $('#sexbarplotgnId').val().split(/[\s,]+/)
          async function processBuildSexBarExpPlot(items) {
            for(let indexDiv=0; indexDiv<items.length; indexDiv++) {
              await new Promise(resolve => setTimeout(resolve, 0));
              // Container for graph
              let graphDiv = "sexbarexpressionView_"+indexDiv
              let d = [];
              if(items[indexDiv] in ytitles) {
                let i = 0
                for(let s of sexs) {
                  let d2 = []
                  for(let j=0; j<val[items[indexDiv]+"_"+s]['val'].length; j++) {
                    d2.push({
                      x : i,
                      y :   val[items[indexDiv]+"_"+s]['val'][j],
                      age : val[items[indexDiv]+"_"+s]['ages'][j],
                      conditions : val[items[indexDiv]+"_"+s]['conditions'][j],
                      color : sexs_info[s].color
                    })
                  }
                  d.push(d2)
                  i += 1
                }
                let dtranspose = d.reduce((acc, ligne) => { ligne.forEach((element, index) => {if (!acc[index]) {acc[index] = [];}acc[index] = acc[index].concat(element);});return acc;}, [])
                let data = []
                for (let i in dtranspose) {
                  data.push({data: dtranspose[i]})
                }
                let titletext = null
                let subtitletext = null
                if(items.length === 1) {
                  titletext = 'Expression profiles by sex in ' + $('#sexselecttissue').val() + ' (project ' + $('#selectexpbysex').val()[0] + ')'
                  subtitletext = 'GEGA - Version ' + updateVersion
                }
                Highcharts.chart(graphDiv, {
                  chart: {
                    type: 'column',
                    events: {
                      click: function(event) {
                        navigator.clipboard.writeText(items[indexDiv])
                          .then(function() {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>"+items[indexDiv]+"</b>")
                            $('#toast_body').html("Copied to clipboard.")
                            $('#toast').toast('show');
                          })
                          .catch(function(err) {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>Error</b>")
                            $('#toast_body').html("Failed to copy: ", err)
                            $('#toast').toast('show');
                          });
                      }
                    }
                  },
                  title: {
                    text: titletext
                  },
                  subtitle: {
                    text: subtitletext
                  },
                  credits: { enabled: false },
                  legend: { enabled: false },
                  exporting:{ enabled: $("#sexbarexpviewexport").val() == "true" ? true : false },
                  xAxis: {
                    categories: sexs,
                    labels: {
                      step: 1,
                      formatter: function () {
                        return '<span style="color:'+sexs_info[this.value].color+'">' + this.value + '</span>'
                      }
                    }
                  },
                  yAxis: {
                    title: { text: ytitles[items[indexDiv]] },
                    type: $("#sexbarexplog").val() == "linear" ? 'linear' : 'logarithmic',
                    min: $("#sexbarexplog").val() == "linear" ? 0 : 0.1,
                    max: $("#sexbarexpsetmax").val() == "true" ? maxY : null
                  },
                  plotOptions: {
                    series: {
                      pointPadding: 0 // groupPadding: 0
                    },
                    column: {
                      centerInCategory: true
                    }
                  },
                  tooltip: {
                    headerFormat: '<span style="font-size:12px"><b>{point.key}</b></span><table>' +
                      '<tr style="text-align:center"><td><em>Age</em></td><td style="padding-left:5px;padding-right:5px"><em>Condition</em></td><td><em>Value</em></td></tr>',
                    pointFormat: '<tr>' +
                      '<td style="padding:0;text-align:center">{point.age}</td>' +
                      '<td style="padding:0;text-align:center">{point.conditions}</td>' +
                      '<td style="padding:0;padding-left:5px;text-align:right"> <b>{point.y:.2f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true,
                    enabled: $("#sexbarexpviewtooltips").val() == "true" ? true : false
                  },
                  series: data
                });
              }
              else {
                $("#sexbarplotgnId").addClass("form-error");
                let html = `
                  <div class="row" style="display:flex;height:250px;margin:auto">
                    <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                      <i class="bx bx-error" style="color:red;"></i>
                    </div>
                    <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                      <small><b>INPUT ERROR</b><br/>`
                html += "\""+ items[indexDiv] + "\" not found ! <br/>"
                html += '</small></div></div>'
                $('#'+graphDiv).html(html)
              }
            }
          }
          processBuildSexBarExpPlot(gnIds)
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
  });

  // Sex Box plot (expression)
  // Change group by "tissue"
  $("#sexboxplotorderby").on('hidden.bs.select', function () {
    $('#sexboxplotgnId').trigger($.Event('keypress', { keyCode: 13 }));
  });
  // Set same max Y
  $('#sexboxexpsetmax').on('hidden.bs.select', function (e) {
    var sexboxexpgraphids = []
    $('[id^=sexboxexpressionView_]').each(function(){
      sexboxexpgraphids.push($(this).attr("id"))
    });
    async function processSexBoxExpSetMax(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.yAxis[0].update({
          max: $("#sexboxexpsetmax").val() == "true" ? parseFloat($("#sexboxexpsetmax").attr('data-maxy')) : null
        });
      }
    }
    processSexBoxExpSetMax(sexboxexpgraphids);
  });
  // Display export button
  $('#sexboxexpviewexport').on('hidden.bs.select', function (e) {
    var sexboxexpgraphids = []
    $('[id^=sexboxexpressionView_]').each(function(){
      sexboxexpgraphids.push($(this).attr("id"))
    });
    async function processSexBoxExpButtons(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ exporting: { enabled: $("#sexboxexpviewexport").val() == "true" ? true : false } })
      }
    }
    processSexBoxExpButtons(sexboxexpgraphids);
  });
  // Display tooltips
  $('#sexboxexpviewtooltips').on('hidden.bs.select', function (e) {
    var sexboxexpgraphids = []
    $('[id^=sexboxexpressionView_]').each(function(){
      sexboxexpgraphids.push($(this).attr("id"))
    });
    async function processSexBoxExpTooltips(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ tooltip: { enabled: $("#sexboxexpviewtooltips").val() == "true" ? true : false } })
      }
    }
    processSexBoxExpTooltips(sexboxexpgraphids);
  });
  // Build boxplot
  $('#sexboxplotgnId').keypress(function(event){
    $(this).removeClass("form-error");
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13' && $('#sexselecttissue2').val().length != 0) {
      var geneId = $(this).val().split(/[\s,]+/)
      let h = 825
      switch(geneId.length) {
        case 1:
          h = 825
          break
        case 2:
          h = 410
          break
        case 3:
          h = 275
          break
        default:
          h = 200
          break
      }
      let html = '<div class="row ml-0 mr-0">'
      for(let i=0; i<geneId.length; i++) {
        html += '<div class="col-lg-12 pl-0 pr-0" id="sexboxexpressionView_'+i+'" style="height:'+h+'px"></div>'
      }
      html += '</div>'
      $('#sexexpressionView').html(html)
      $('#sexexpressionView').css("overflow","auto")

      $.ajax({
        url : '/get_expressions_for_gnids_boxplot_by_project',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          geneIds: $('#sexboxplotgnId').val(),
          tissue: $('#sexselecttissue2').val(),
          project: $('#selectexpbysex').val()[0],
          orderby: $("#sexboxplotorderby").val() == "true" ? 'tissuesex' : 'sextissue'
        }),
        beforeSend: function(){
          for(let i=0; i<geneId.length; i++) {
            $('#sexboxexpressionView_'+i).html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:22%;height:auto;">');
          }
        },
        success: function(d){
          // Save values by key (key = gnId+"_"+sex)
          let vals = {}
          let sexs = []
          let maxY = 0
          let ytitles = {} // (key = gnId)
          d.forEach(function(v){
            vals[v.gnId+"_"+v.sex+"_"+v.tissue] = v.vals
            if(!sexs.includes(v.sex+"_"+v.tissue)) {
              sexs.push(v.sex+"_"+v.tissue)
            }
            maxY = Math.max(Math.max(...v.vals.split(";")), maxY)
            ytitles[v.gnId] = v.gnId + "<br>(" +
              (v.gnName === v.gnId ? "-" : v.gnName) +
              " / " + v.gnSimpleBiotype +")";
          });
          // Save MaxY 
          $('#sexboxexpsetmax').attr('data-maxy', maxY)
          let gnIds = $('#sexboxplotgnId').val().split(/[\s,]+/)
          async function processBuildSexBoxExpPlot(items) {
            for(let indexDiv=0; indexDiv<items.length; indexDiv++) {
              await new Promise(resolve => setTimeout(resolve, 0));
              // Container for graph
              let graphDiv = "sexboxexpressionView_"+indexDiv
              let boxplotData = [];
              let scatterData = [];
              let nbofvalues  = [];
              let i = 0;
              if(items[indexDiv] in ytitles) {
                for(let s of sexs) {
                  let tmparr = vals[items[indexDiv]+"_"+s].split(";")
                  let tmpboxplot = getBoxValues(tmparr.sort((a, b) => a - b))
                  tmpboxplot.x = i
                  tmpboxplot.color = sexs_info[s.split("_")[0]].color
                  tmpboxplot.fillColor = hexToRgba(sexs_info[s.split("_")[0]].color, 0.1)
                  boxplotData.push(tmpboxplot)
                  nbofvalues[s] = 0
                  tmparr.forEach(function(m){
                    scatterData.push([i, parseFloat(m)])
                    nbofvalues[s] += 1
                  });
                  i+=1;
                }
                let titletext = null
                let subtitletext = null
                if(items.length === 1) {
                  titletext = 'Expression profiles by sex (project ' + $('#selectexpbysex').val()[0] + ')'
                  subtitletext = 'GEGA - Version ' + updateVersion
                }
                Highcharts.chart(graphDiv, {
                  chart: {
                    zoomType: 'x',
                    events: {
                      click: function(event) {
                        navigator.clipboard.writeText(items[indexDiv])
                          .then(function() {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>"+items[indexDiv]+"</b>")
                            $('#toast_body').html("Copied to clipboard.")
                            $('#toast').toast('show');
                          })
                          .catch(function(err) {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>Error</b>")
                            $('#toast_body').html("Failed to copy: ", err)
                            $('#toast').toast('show');
                          });
                      }
                    }
                  },
                  title: {
                    text: titletext
                  },
                  subtitle: {
                    text: subtitletext
                  },
                  credits: { enabled: false },
                  legend: { enabled: false },
                  exporting:{ enabled: $("#sexboxexpviewexport").val() == "true" ? true : false },
                  xAxis: {
                    categories: sexs,
                    labels: {
                      step: 1,
                      formatter: function () {
                        let a = this.value.split("_")[0]
                        let t = this.value.split("_")[1]
                        return '<span style="color:'+sexs_info[a].color+'">' + a + ' ' + t + '</span>'
                      }
                    },
                    events: {
                      afterSetExtremes: function(e) {
                        // Same zoom for all boxplot
                        let sexboxexpgraphids = []
                        $('[id^=sexboxexpressionView_]').each(function(){
                          sexboxexpgraphids.push($(this).attr("id"))
                        });
                        async function processSexBoxExpZoom(items) {
                          for (const item of items) {
                            await new Promise(resolve => setTimeout(resolve, 0));
                            var chart = $('#'+item).highcharts();
                            let curextr = chart.xAxis[0].getExtremes()
                            if(curextr.min != e.min || curextr.max != e.max) {
                              chart.xAxis[0].setExtremes(e.min, e.max);
                            }
                          }
                        }
                        processSexBoxExpZoom(sexboxexpgraphids);
                      }
                    }
                  },
                  yAxis: {
                    title: { text: ytitles[items[indexDiv]] },
                    max: $("#sexboxexpsetmax").val() == "true" ? maxY : null
                  },
                  plotOptions: {
                    series: {
                      pointPadding: 0.1, groupPadding: 0
                    }
                  },
                  tooltip: {
                    enabled: $("#sexboxexpviewtooltips").val() == "true" ? true : false,
                    useHTML: true,
                    formatter: function() {
                      let text = '<span style="font-size:12px"><b>'+this.key+'</b></span><table style="width:100%">' +
                        '<tr style="border-bottom:1px solid '+this.color+';"><td style="padding-bottom:5px;">Number of values : </td>' +
                        '<td style="padding-bottom:5px;text-align:right"> <b>'+nbofvalues[this.key]+'</b></td></tr>' +
                        '<tr><td style="padding-top:5px">Maximum : </td>' +
                        '<td style="padding-top:5px;text-align:right"> <b>'+this.point.high.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Upper quartile : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.q3.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Median : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.median.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Lower quartile : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.q1.toFixed(2)+'</b></td></tr>' +
                        '<tr><td style="padding:0">Minimum : </td>' +
                        '<td style="padding:0;padding-left:5px;text-align:right"> <b>'+this.point.low.toFixed(2)+'</b></td></tr>' +
                        '</table>';
                      return text
                    }
                  },
                  series: [
                    {
                      type: 'boxplot',
                      data: boxplotData
                    },
                    {
                      name: 'Value',
                      type: 'scatter',
                      data: scatterData,
                      jitter: { x: 0.15 },
                      marker: { radius: 1 },
                      color: 'rgba(100, 100, 100, 0.6)',
                      enableMouseTracking: false
                    }
                  ]
                });
              }
              else {
                $("#sexboxplotgnId").addClass("form-error");
                let html = `
                  <div class="row" style="display:flex;height:250px;margin:auto">
                    <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                      <i class="bx bx-error" style="color:red;"></i>
                    </div>
                    <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                      <small><b>INPUT ERROR</b><br/>`
                html += "\""+ items[indexDiv] + "\" not found ! <br/>"
                html += '</small></div></div>'
                $('#'+graphDiv).html(html)
              }
            }
          }
          processBuildSexBoxExpPlot(gnIds)
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
  });

  // Sex co expression plot
  // Linear / Log
  $('#sexcoexplog').on('hidden.bs.select', function (e) {
    if($("#sexcoexplog").val() == 'logarithmic') {
      $('#sexcoexpviewlinereg').val("false");
      $('#sexcoexpviewlinereg').prop('disabled', true);
      $('#sexcoexpviewlinereg').selectpicker('refresh');
      $('#sexcoexpviewlinereg').trigger('hidden.bs.select');
    }
    else {
      $('#sexcoexpviewlinereg').val("true");
      $('#sexcoexpviewlinereg').prop('disabled', false);
      $('#sexcoexpviewlinereg').selectpicker('refresh');
      $('#sexcoexpviewlinereg').trigger('hidden.bs.select');
    }
    var sexcoexpgraphids = []
    $('[id^=sexcoexpressionView_]').each(function(){
      sexcoexpgraphids.push($(this).attr("id"))
    });
    async function processSexCoExpLog(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.xAxis[0].update({
          type: $("#sexcoexplog").val() == "linear" ? 'linear' : 'logarithmic',
          min:  $("#sexcoexplog").val() == "linear" ? 0 : null
        });
        chart.yAxis[0].update({
          type: $("#sexcoexplog").val() == "linear" ? 'linear' : 'logarithmic',
          min:  $("#sexcoexplog").val() == "linear" ? 0 : null
        });
      }
    }
    processSexCoExpLog(sexcoexpgraphids);
  });
  // Display linear regression
  $('#sexcoexpviewlinereg').on('hidden.bs.select', function (e) {
    var sexcoexpgraphids = []
    $('[id^=sexcoexpressionView_]').each(function(){
      sexcoexpgraphids.push($(this).attr("id"))
    });
    async function processSexCoExpRegression(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        $("#sexcoexpviewlinereg").val() == "true" ? chart.series[1].show() : chart.series[1].hide()
      }
    }
    processSexCoExpRegression(sexcoexpgraphids);
  });
  // Display Labels
  $('#sexcoexpviewlabels').on('hidden.bs.select', function (e) {
    var sexcoexpgraphids = []
    $('[id^=sexcoexpressionView_]').each(function(){
      sexcoexpgraphids.push($(this).attr("id"))
    });
    async function processSexCoExpLabels(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        var data = chart.series[0].options.data;
        $.each(data, function(i,point){
          point.dataLabels.enabled = $("#sexcoexpviewlabels").val() == "true" ? true : false
        })
        chart.series[0].update(data);
      }
    }
    processSexCoExpLabels(sexcoexpgraphids);
  });
  // Display export button
  $('#sexcoexpviewexport').on('hidden.bs.select', function (e) {
    var sexcoexpgraphids = []
    $('[id^=sexcoexpressionView_]').each(function(){
      sexcoexpgraphids.push($(this).attr("id"))
    });
    async function processSexCoExpButtons(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ exporting: { enabled: $("#sexcoexpviewexport").val() == "true" ? true : false } })
      }
    }
    processSexCoExpButtons(sexcoexpgraphids);
  });
  // Display tooltips
  $('#sexcoexpviewtooltips').on('hidden.bs.select', function (e) {
    var sexcoexpgraphids = []
    $('[id^=sexcoexpressionView_]').each(function(){
      sexcoexpgraphids.push($(this).attr("id"))
    });
    async function processSexCoExpTooltips(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        chart.update({ tooltip: { enabled: $("#sexcoexpviewtooltips").val() == "true" ? true : false } })
      }
    }
    processSexCoExpTooltips(sexcoexpgraphids);
  });
  // Sort plot by input or by correlation
  $('#sexcoexpsort').on('hidden.bs.select', function (e) {
    // Select all div with attr data-corr
    var divs = $('[id^=sexcoexpressionView_]');
    // Sort div using $('#coexpsort').val()
    divs.sort(function(a, b) {
      var corrA = parseFloat($(a).attr($('#sexcoexpsort').val()));
      var corrB = parseFloat($(b).attr($('#sexcoexpsort').val()));
      return corrB - corrA; // sort desc
    });
    // Add sorted div to container
    $.each(divs, function(index, element) {
      $(element).animate({opacity: 0}, 0);
      $(element).appendTo($(element).parent())
        .delay(50 * index) // Add delay
        .animate({opacity: 1}, {
          duration: 1000,
          queue: true,
          complete: function() {
              $(this).dequeue();
          }
        });
    });
  });
  $('#sexcoexphidecorr').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13') {
      // For each div with attr data-corr
      let nbvisible = 0
      $.each($('[id^=sexcoexpressionView_]'), function(index, div) {
        const dataCorr   = parseFloat($(this).attr('data-corr'));
        const inputValue = parseFloat($('#sexcoexphidecorr').val());
        if (dataCorr < inputValue) {
          $(div).fadeOut(500);
        }
        else {
          $(div).fadeIn(500);
          nbvisible += 1
        }
      });
      $('#sexcoexpvisible').text(nbvisible)
    }
  });
  // Build plot(s)
  $('#sexcoexpgnIdX,#sexcoexpgnIdY').keypress(function(event){
    $(this).removeClass("form-error");
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13' && $('#sexselecttissue3').val().length != 0 && $('#sexcoexpgnIdX').val() !== "" && $('#sexcoexpgnIdY').val() !== "") {
      var otherGenes = $('#sexcoexpgnIdY').val().split(/[\s,]+/)
      // By default sort by input
      $('#sexcoexpsort').val("data-input")
      $('#sexcoexpsort').selectpicker('refresh');
      $('#sexcoexpvisible').text(otherGenes.length)
      $('#sexcoexpnbplot').text(otherGenes.length);
      let h
      let collg
      if(otherGenes.length == 1) {
        h = 825
        collg = 12
      }
      else if(otherGenes.length <=6 ) {
        h = 400
        collg = 6
      }
      else {
        h = 250
        collg = 4
      }
      let html = '<div class="row ml-0 mr-0">'
      for(let i=0; i<otherGenes.length; i++) {
        html += '<div class="col-lg-'+collg+' pl-0 pr-0" id="sexcoexpressionView_'+i+'" data-input="'+(otherGenes.length-i)+'" style="height:'+h+'px"></div>'
      }
      html += '</div>'
      $('#sexexpressionView').html(html)
      $('#sexexpressionView').css("overflow","auto")
      
      // Container for graph
      var graphDiv = ""
      var graphCompleted = 0
      var promisebuildsexscatter = new Promise(function(resolve, reject) {
        $.each(otherGenes, function(indexDiv, otherGeneId) {
          graphDiv = "sexcoexpressionView_"+indexDiv
          $.ajax({
            url : '/get_expression_for_gnids_by_project',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({
              geneIdsX: $('#sexcoexpgnIdX').val(),
              geneIdsY: otherGeneId,
              tissue: $('#sexselecttissue3').val(),
              project: $('#selectexpbysex').val()[0],
              graphDiv: graphDiv
            }),
            beforeSend: function(){
              $('#'+graphDiv).html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:30%;height:auto;">');
            },
            success: function(d){
              // Save values by key (key = gnId+"_"+sex)
              let valX = {}
              let sexs = []
              d.X.forEach(function(v){
                if(typeof valX[v.gnId+"_"+v.sex] === "undefined") {
                  valX[v.gnId+"_"+v.sex] = { 'val': [] }
                }
                valX[v.gnId+"_"+v.sex]['val'].push(v.val)
                if(!sexs.includes(v.sex)) {
                  sexs.push(v.sex)
                }
              });
              let valY = {}
              let ytitle = {}
              d.Y.forEach(function(v){
                if(typeof valY[v.gnId+"_"+v.sex] === "undefined") {
                  valY[v.gnId+"_"+v.sex] = { 'val': [] }
                }
                valY[v.gnId+"_"+v.sex]['val'].push(v.val)
                ytitle[v.gnId] = v.gnId + "<br>in " + $('#sexselecttissue3').val() + " (" +
                  (v.gnName === v.gnId ? "-" : v.gnName) +
                  " / " + v.gnSimpleBiotype +")";
              });
              // If gnIdX and gnIdY found
              if(d.X.length>0 && d.Y.length>0) {
                let gnIdX = d.gnIdX
                let gnIdY = d.gnIdY             
                let data = []
                let arrayOfarray = []
                // To set x and y axis
                let maxX = -Infinity
                let maxY = -Infinity
                let minX = Infinity
                let minY = Infinity
                for(let s of sexs) {
                  for(let j=0; j<valX[gnIdX+"_"+s]['val'].length; j++) {
                    data.push({
                      x:parseFloat(valX[gnIdX+"_"+s]['val'][j]),
                      y:parseFloat(valY[gnIdY+"_"+s]['val'][j]),
                      name: s,
                      color: sexs_info[s].color,
                      dataLabels: {
                        enabled: $("#sexcoexpviewlabels").val() == "true" ? true : false,
                        format: '{point.name}',
                        color: sexs_info[s].color,
                        style: {
                          textOutline: 'none' 
                        }
                      }
                    })
                    arrayOfarray.push([parseFloat(valX[gnIdX+"_"+s]['val'][j]),parseFloat(valY[gnIdY+"_"+s]['val'][j])])
                    maxX = Math.max(maxX,parseFloat(valX[gnIdX+"_"+s]['val'][j]))
                    maxY = Math.max(maxY,parseFloat(valY[gnIdY+"_"+s]['val'][j]))
                    if(valX[gnIdX+"_"+s] != 0) {minX = Math.min(minX,parseFloat(valX[gnIdX+"_"+s]['val'][j]))}
                    if(valY[gnIdY+"_"+s] != 0) {minY = Math.min(minY,parseFloat(valY[gnIdY+"_"+s]['val'][j]))}
                  }
                }
                // Points for linear regression 
                let x = [Math.min(minX, minY)]
                /*for(var k=Math.min(minX, minY)/10; k<Math.max(maxX, maxY)*10; k*=2) {
                  x.push(Math.min(minX, minY)*k)
                }*/
                x.push(Math.max(maxX, maxY))
                let titletext = null
                let subtitletext = null
                if(otherGenes.length === 1) {
                  titletext = 'Co-expression by sex in ' + $('#sexselecttissue3').val() + ' (project ' + $('#selectexpbysex').val()[0] + ')'
                  subtitletext = 'GEGA - Version ' + updateVersion
                }
                // Get data to plot linearRegression
                $.ajax({
                  url : '/get_linearRegression',
                  type: "POST",
                  contentType: "application/json",
                  data: JSON.stringify({
                    aa : arrayOfarray,
                    x : x
                  }),
                  success: function(d2){
                    // Save correlation in attr of graph container
                    $('#'+d.gd).attr("data-corr", d2.corr)
                    if(d2.corr < parseFloat($('#sexcoexphidecorr').val())) {
                      $('#'+d.gd).fadeOut(500)
                    }
                    Highcharts.chart(d.gd, {
                      chart: {
                        type: 'scatter',
                        zoomType: 'xy',
                        events: {
                          click: function(event) {
                            navigator.clipboard.writeText(gnIdY)
                              .then(function() {
                                $('#toast_icon').html('<i class="bx bx-copy"></i>')
                                $('#toast_title').html("<b>"+gnIdY+"</b>")
                                $('#toast_body').html("Copied to clipboard.")
                                $('#toast').toast('show');
                              })
                              .catch(function(err) {
                                $('#toast_icon').html('<i class="bx bx-copy"></i>')
                                $('#toast_title').html("<b>Error</b>")
                                $('#toast_body').html("Failed to copy: ", err)
                                $('#toast').toast('show');
                              });
                          }
                        }
                      },
                      credits:  { enabled: false },
                      title:    { text: titletext },
                      subtitle: { text: subtitletext },
                      exporting:{ enabled: $("#sexcoexpviewexport").val() == "true" ? true : false },
                      tooltip:  { enabled: $("#sexcoexpviewtooltips").val() == "true" ? true : false },
                      xAxis: {
                        title: {
                          enabled: true,
                          text: gnIdX
                        },
                        startOnTick: true,
                        endOnTick: true,
                        showLastLabel: true,
                        type: $("#sexcoexplog").val() == "linear" ? 'linear' : 'logarithmic',
                        min:  minX, //$("#sexcoexplog").val() == "linear" ? 0 : minX,
                        max:  maxX
                      },
                      yAxis: {
                        title: { text: ytitle[gnIdY] },
                        type: $("#sexcoexplog").val() == "linear" ? 'linear' : 'logarithmic',
                        min:  minY, //$("#sexcoexplog").val() == "linear" ? 0 : minY,
                        max:  maxY
                      },
                      legend: { enabled: false },
                      plotOptions: {
                        scatter: {
                          marker: {
                            radius: 4,
                            states: {
                              hover: {
                                enabled: true,
                                lineColor: 'rgb(100,100,100)'
                              }
                            }
                          },
                          states: {
                            hover: {
                              marker: {
                                enabled: false
                              }
                            }
                          },
                          tooltip: {
                            headerFormat: '',
                            pointFormat: '<b>{point.name}</b><br>'+gnIdX+' : {point.x}<br>'+gnIdY+' : {point.y}'
                          }
                        }
                      },
                      series: [{
                        data: data
                      },
                      {
                        type: 'spline',
                        label: 'Linear regression',
                        data: d2.xy,
                        marker: {
                          enabled: false
                        },
                        states: {
                          hover: {
                            lineWidth: 0
                          }
                        },
                        enableMouseTracking: false,
                        dashStyle: 'dash',
                        opacity: 0.8,
                        visible: $("#sexcoexpviewlinereg").val() == "true" ? true : false
                      }],
                      labels: {
                        items: [{
                          html: '&rho; = '+ d2.corr,
                          style: {
                            left: '10px',
                            top: '10px',
                            fontWeight: 'bold'
                          }
                        }]
                      }
                    });
                  },
                  error : function(e) {
                    console.log("ERROR: ", e);
                  }
                });
              }
              // Else gnIdX or gnIdY not found
              else {
                let html = `
                  <div class="row" style="display:flex;height:250px;margin:auto">
                    <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                      <i class="bx bx-error" style="color:red;"></i>
                    </div>
                    <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                      <small><b>INPUT ERROR</b><br/>`
                if(d.X.length==0 || typeof d.X[0] === "undefined") {
                  html += "\""+ d.gnIdX + "\" not found ! <br/>"
                  $("#sexcoexpgnIdX").addClass("form-error");
                }
                if(d.Y.length==0 && typeof d.Y[0] === "undefined") {
                  html += "\""+ d.gnIdY +"\" not found !"
                  $("#sexcoexpgnIdY").addClass("form-error");
                }
                html += '</small></div></div>'
                $('#'+d.gd).html(html)
              }
            },
            complete : function(){
              graphCompleted += 1
              if (graphCompleted === otherGenes.length) {
                setTimeout(resolve, 1000);
              }
            },
            error : function(e) {
              console.log("ERROR: ", e);
            }
          });
        })
      })
      promisebuildsexscatter.then(function() {
        let nbvisible = 0
        $.each($('[id^=sexcoexpressionView_]'), function(index, div) {
          const dataCorr   = parseFloat($(this).attr('data-corr'));
          const inputValue = parseFloat($('#sexcoexphidecorr').val());
          if (dataCorr < inputValue) {
            $(div).fadeOut(500);
          }
          else {
            $(div).fadeIn(500);
            nbvisible += 1
          }
        });
        $('#sexcoexpvisible').text(nbvisible)
      });
    }
  });

  // Tools : convert separator
  $.event.special.convertSepFromText = {
    trigger: function (event, data) {
      let input = $("#tools-convert-input").val();
      let sepin = $("#tools-convert-sepin").val();
      let sepout = $("#tools-convert-sepout").val();

      if(input.length != 0 && sepin.length != 0 && sepout.length != 0) {
        if(sepin  == "\\n") { sepin  = "\n"; }
        if(sepout == "\\n") { sepout = "\n"; }
        let output = input.split(sepin).join(sepout).split(sepout);
        let outputtmp = output
        // Remove duplicates
        let nbdup = output.length - [...new Set(output)].length;
        $("#tools-convert-nbdup").text(nbdup)
        if($("#tools-convert-removedup").val() == "yes") {
          output = [...new Set(output)]
        }
        
        // Remove empty fields
        let nbempty = outputtmp.length
        if($("#tools-convert-removeempty").val() == "yes") {
          output = output.filter(function(element) {
            return element !== null && element !== undefined && element !== '';
          });
          outputtmp = outputtmp.filter(function(element) {
            return element !== null && element !== undefined && element !== '';
          });
        }
        nbempty -= outputtmp.length
        $("#tools-convert-nbempty").text(nbempty)

        $("#tools-convert-output-mess").text(output.length);
        if(output.length > 1) { $("#tools-convert-output-mess-s").text("s"); }
        $("#tools-convert-output").val(output.join(sepout));
        
        $("#tools-convert-copy").prop('disabled', output.length === 0);
      }
    }
  }
  $.event.special.convertUpdateForm = {
    trigger: function (event, data) {
      $("#tools-convert-output-mess").text(0)
      $("#tools-convert-output-mess-s").text("")
      $("#tools-convert-output").val('')
      $("#tools-convert-found-sc").text(0)
      $("#tools-convert-found-comma").text(0)
      $("#tools-convert-found-space").text(0)
      $("#tools-convert-found-tab").text(0)
      $("#tools-convert-found-br").text(0)
      $("#tools-convert-nbfields").text(0)
      $("#tools-convert-s").text("")
      $("#tools-convert-nbfieldsout").text(0)
      $("#tools-convert-nbfieldsout-s").text("")
      $("#tools-convert-nbdup").text(0)
      $("#tools-convert-nbempty").text(0)
      let input = $("#tools-convert-input").val();
      if(input.length == 0) { $("#tools-convert-input").parent().find('*').addClass("form-error"); }
      else                  { $("#tools-convert-input").parent().find('*').removeClass("form-error"); }
      let sepin = $("#tools-convert-sepin").val();
      if(sepin.length == 0) { $("#tools-convert-sepin").parent().parent().find('*').addClass("form-error"); }
      else                  { $("#tools-convert-sepin").parent().parent().find('*').removeClass("form-error"); }
      let sepout = $("#tools-convert-sepout").val();
      if(sepout.length == 0) { $("#tools-convert-sepout").parent().parent().find('*').addClass("form-error"); }
      else                   { $("#tools-convert-sepout").parent().parent().find('*').removeClass("form-error"); }

      if(input.length != 0) {
        $("#tools-convert-found-sc").text(input.split(";").length-1);
        $("#tools-convert-found-comma").text(input.split(",").length-1);
        $("#tools-convert-found-space").text(input.split(" ").length-1);
        $("#tools-convert-found-tab").text(input.split("\t").length-1);
        $("#tools-convert-found-br").text(input.split("\n").length-1);
        if(sepin.length != 0) {
          if(sepin  == "\\n") { sepin = "\n"; }
          let l = input.split(sepin).length;
          $("#tools-convert-nbfields").text(l);
          if(l > 1) { $("#tools-convert-s").text("s") }
          else      { $("#tools-convert-s").text("")  }
          if(sepout.length != 0) {
            if(sepout  == "\\n") { sepout = "\n"; }
            let l2 = input.split(sepin).join(sepout).split(sepout).length;
            $("#tools-convert-nbfieldsout").text(l2);
            if(l2 > 1) { $("#tools-convert-nbfieldsout-s").text("s") }
            else       { $("#tools-convert-nbfieldsout-s").text("")  }
            if(l != l2) {
              $("#tools-convert-nbfieldsout").parent().removeClass("btn-primary").addClass("btn-danger");
            }
            else {
              $("#tools-convert-nbfieldsout").parent().removeClass("btn-danger").addClass("btn-primary");
            }
            $(this).trigger('convertSepFromText');
          }
        }
      }
    }
  }
  $('#tools-convert-input').on('input', function() {
    $(this).trigger('convertUpdateForm');
  });
  $("#tools-convert-sepin, #tools-convert-sepout").on('changed.bs.select', function() {
    $(this).trigger('convertUpdateForm');
  });
  $("#tools-convert-removedup, #tools-convert-removeempty").on('changed.bs.select', function() {
    $(this).trigger('convertSepFromText');
  });
  $('#tools-convert-copy').click(function() {
    navigator.clipboard.writeText($("#tools-convert-output").val())
    .then(function() {
      $('#toast_icon').html('<i class="bx bx-copy"></i>')
      $('#toast_title').html("<b>"+$("#tools-convert-output-mess").text()+" field"+$("#tools-convert-output-mess-s").text()+"</b>")
      $('#toast_body').html("Copied to clipboard.")
      $('#toast').toast('show');
    })
    .catch(function(err) {
      $('#toast_icon').html('<i class="bx bx-copy"></i>')
      $('#toast_title').html("<b>Error</b>")
      $('#toast_body').html("Failed to copy: ", err)
      $('#toast').toast('show');
    });
  });


  /*
  *  Ortho HSA
  */
  // Bar plot
  // Set same max Y
  $('#orthobarexpsetmax').on('hidden.bs.select', function (e) {
    var barexpgraphids = []
    $('[id^=orthoexpressionView_]').each(function(){
      barexpgraphids.push($(this).attr("id"))
    });
    async function processOrthoBarExpSetMax(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        if (chart !== undefined) {
          chart.yAxis[0].update({
            min: $("#orthobarexpsetmax").val() == "true" ? parseFloat($("#orthobarexpsetmax").attr('data-maxy'))*-1 : null,
            max: $("#orthobarexpsetmax").val() == "true" ? parseFloat($("#orthobarexpsetmax").attr('data-maxy')) : null
          });
        }
      }
    }
    processOrthoBarExpSetMax(barexpgraphids);
  });
  // Display export button
  $('#orthobarexpviewexport').on('hidden.bs.select', function (e) {
    var barexpgraphids = []
    $('[id^=orthoexpressionView_]').each(function(){
      barexpgraphids.push($(this).attr("id"))
    });
    async function processOrthoBarExpButtons(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        if (chart !== undefined) {
          chart.update({ exporting: { enabled: $("#orthobarexpviewexport").val() == "true" ? true : false } })
        }
      }
    }
    processOrthoBarExpButtons(barexpgraphids);
  });
  // Display tooltips
  $('#orthobarexpviewtooltips').on('hidden.bs.select', function (e) {
    var barexpgraphids = []
    $('[id^=orthoexpressionView_]').each(function(){
      barexpgraphids.push($(this).attr("id"))
    });
    async function processOrthoBarExpTooltips(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        if (chart !== undefined) {
          chart.update({ tooltip: { enabled: $("#orthobarexpviewtooltips").val() == "true" ? true : false } })
        }
      }
    }
    processOrthoBarExpTooltips(barexpgraphids);
  });
  // Display line
  $('#orthobarexpviewline').on('hidden.bs.select', function (e) {
    var barexpgraphids = []
    $('[id^=orthoexpressionView_]').each(function(){
      barexpgraphids.push($(this).attr("id"))
    });
    async function processOrthoBarLine(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        if (chart !== undefined) {
          $("#orthobarexpviewline").val() == "true" ? chart.series[2].show() : chart.series[2].hide()
        }
      }
    }
    processOrthoBarLine(barexpgraphids);
  });
  // Build bar plot(s)
  $('#orthobarplotgnId').keypress(function(event){
    $(this).removeClass("form-error");
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
      var geneId = $(this).val().split(/[\s,]+/)
      let h = 825
      switch(geneId.length) {
        case 1:
          h = 825
          break
        case 2:
          h = 410
          break
        default:
          h = 275
          break
      }
      let html = '<div class="row ml-0 mr-0">'
      for(let i=0; i<geneId.length; i++) {
        html += '<div class="col-lg-12 pl-0 pr-0" id="orthoexpressionView_'+i+'" style="height:'+h+'px"></div>'
      }
      html += '</div>'
      $('#orthoexpressionView').html(html)
      $('#orthoexpressionView').css("overflow","auto")

      $.ajax({
        url : '/get_expression_for_ortho',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          geneIds: $('#orthobarplotgnId').val()
        }),
        beforeSend: function(){
          for(let i=0; i<geneId.length; i++) {
            $('#orthoexpressionView_'+i).html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:22%;height:auto;">');
          }
        },
        success: function(val){
          // Save maxY value
          let maxY = 0
          Object.keys(val).filter(c => /GGAe$|HSAe$/.test(c)).forEach(k => {
            if(val[k] != null) {
              let cmax = Math.max(...val[k].map(Math.abs));
              if(cmax > maxY) { maxY = cmax}
            }
          });
          $('#orthobarexpsetmax').attr('data-maxy', maxY)
          // Init colors by tissues
          let colors = []
          val.tissues.forEach(function(t){
            colors.push(tissues_info_ortho[t].color);
          });
          let gnIds = $('#orthobarplotgnId').val().split(/[\s,]+/)
          async function processBuildOrthoBarExpPlot(items) {
            for(let indexDiv=0; indexDiv<items.length; indexDiv++) {
              await new Promise(resolve => setTimeout(resolve, 0));
              // Container for graph
              let graphDiv = "orthoexpressionView_"+indexDiv
              // if ortholog found
              if(val[items[indexDiv]+"_GGA"] != null && val[items[indexDiv]+"_HSA"] != null) {
                // Build data for line (GGAe - HSAe by tissues)
                let tmparr = val[items[indexDiv]+"_GGAe"].map((e, index) => e + val[items[indexDiv]+"_HSAe"][index]);
                let data = [
                  {
                    name:val[items[indexDiv]+"_GGA"],
                    img:"poulet.png",
                    data:val[items[indexDiv]+"_GGAe"],
                    colorByPoint: true,
                    pointPadding: 0, groupPadding: 0
                  },
                  {
                    name:val[items[indexDiv]+"_HSA"],
                    img:"homme.png",
                    data:val[items[indexDiv]+"_HSAe"],
                    colorByPoint: true,
                    pointPadding: 0, groupPadding: 0
                  },
                  {
                    data: tmparr,
                    type: 'spline',
                    marker: {
                      enabled: false
                    },
                    states: {
                      hover: {
                        lineWidth: 0
                      }
                    },
                    lineWidth: 1.5,
                    enableMouseTracking: false,
                    dashStyle: 'dash',
                    color: 'black',
                    visible: $("#orthobarexpviewline").val() == "true" ? true : false
                  }
                ]
                let titletext = null
                let subtitletext = null
                if(items.length === 1) {
                  titletext = 'Expression profiles across tissues'
                  subtitletext = 'GEGA - Version ' + updateVersion
                }
                Highcharts.chart(graphDiv, {
                  chart: {
                    type: 'column',
                    zoomType: 'xy',
                    events: {
                      click: function(event) {
                        navigator.clipboard.writeText(items[indexDiv])
                          .then(function() {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>"+items[indexDiv]+"</b>")
                            $('#toast_body').html("Copied to clipboard.")
                            $('#toast').toast('show');
                          })
                          .catch(function(err) {
                            $('#toast_icon').html('<i class="bx bx-copy"></i>')
                            $('#toast_title').html("<b>Error</b>")
                            $('#toast_body').html("Failed to copy: ", err)
                            $('#toast').toast('show');
                          });
                      }
                    }
                  },
                  colors: colors,
                  title: {
                    text: titletext
                  },
                  subtitle: {
                    text: subtitletext
                  },
                  credits: { enabled: false },
                  legend: { enabled: false },
                  exporting:{ enabled: $("#orthobarexpviewexport").val() == "true" ? true : false },
                  xAxis: [
                    {
                      categories: val.tissues,
                      title : {
                        useHTML : true,
                        text : "<img src='/img/homme.png' width='17px' /> " + val[items[indexDiv]+"_HSA"]
                      },
                      labels: {
                        step: 1,
                        rotation: -45,
                        formatter: function () {
                          return '<span style="color:'+tissues_info_ortho[this.value].color+'">' + this.value + '</span>'
                        }
                      }
                    },
                    {
                      opposite: true,
                      title : {
                        useHTML : true,
                        text : "<img src='/img/poulet.png' width='17px' /> " + val[items[indexDiv]+"_GGA"]
                      },
                      categories: val.tissues,
                      linkedTo: 0,
                      labels: {
                          step: 1,
                          rotation: -45,
                          formatter: function () {
                            return '<span style="color:'+tissues_info_ortho[this.value].color+'">' + this.value + '</span>'
                          }
                      },
                    }
                  ],
                  yAxis: {
                    labels: {
                      formatter: function() {
                        return Math.abs(this.value)
                      }
                    },
                    title: { text: null },
                    min: $("#orthobarexpsetmax").val() == "true" ? maxY*-1 : null,
                    max: $("#orthobarexpsetmax").val() == "true" ? maxY : null
                  },
                  plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false
                        }
                    }
                  },
                  tooltip: {
                    headerFormat: '<span style="font-size:12px"><b>{point.key}</b></span><table style="margin-top:5px">',
                    pointFormatter: function() {
                      return '<tr><td style="padding:5px 0 0 0"><img src="/img/'+ this.series.userOptions.img+'" width="17px" /> ' +
                          this.series.name + ' : </td>' +
                          '<td style="padding:3px 0 0 5px;text-align:right"> <b>' +
                          Math.abs(this.y).toFixed(2) + '</b></td></tr>';
                    },
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true,
                    enabled: $("#orthobarexpviewtooltips").val() == "true" ? true : false
                  },
                  series: data
                });
              }
              else {
                let html = `
                  <div class="row" style="display:flex;height:250px;margin:auto">
                    <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                      <i class="bx bx-error" style="color:red;"></i>
                    </div>
                    <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                      <small><b>No ortholog found for the provided Gene Id :</b><br/>`
                html += "\""+ items[indexDiv] + "\"<br/>"
                html += '</small></div></div>'
                $('#'+graphDiv).html(html)
              }
            }
          }
          processBuildOrthoBarExpPlot(gnIds)
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }
  });

  // Ortho HSA co expression plot
  // Linear / Log
  $('#orthocoexplog').on('hidden.bs.select', function (e) {
    if($("#orthocoexplog").val() == 'logarithmic') {
      $('#orthocoexpviewlinereg').val("false");
      $('#orthocoexpviewlinereg').prop('disabled', true);
      $('#orthocoexpviewlinereg').selectpicker('refresh');
      $('#orthocoexpviewlinereg').trigger('hidden.bs.select');
    }
    else {
      $('#orthocoexpviewlinereg').val("true");
      $('#orthocoexpviewlinereg').prop('disabled', false);
      $('#orthocoexpviewlinereg').selectpicker('refresh');
      $('#orthocoexpviewlinereg').trigger('hidden.bs.select');
    }
    var orthocoexpgraphids = []
    $('[id^=orthocoexpressionView_]').each(function(){
      orthocoexpgraphids.push($(this).attr("id"))
    });
    async function processOrthoCoExpLog(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        if (chart !== undefined) {
          chart.xAxis[0].update({
            type: $("#orthocoexplog").val() == "linear" ? 'linear' : 'logarithmic',
            min:  $("#orthocoexplog").val() == "linear" ? 0 : null
          });
          chart.yAxis[0].update({
            type: $("#orthocoexplog").val() == "linear" ? 'linear' : 'logarithmic',
            min:  $("#orthocoexplog").val() == "linear" ? 0 : null
          });
        }
      }
    }
    processOrthoCoExpLog(orthocoexpgraphids);
  });
  // Display linear regression
  $('#orthocoexpviewlinereg').on('hidden.bs.select', function (e) {
    var orthocoexpgraphids = []
    $('[id^=orthocoexpressionView_]').each(function(){
      orthocoexpgraphids.push($(this).attr("id"))
    });
    async function processOrthoCoExpRegression(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        if (chart !== undefined) {
          $("#orthocoexpviewlinereg").val() == "true" ? chart.series[1].show() : chart.series[1].hide()
        }
      }
    }
    processOrthoCoExpRegression(orthocoexpgraphids);
  });
  // Display Labels
  $('#orthocoexpviewlabels').on('hidden.bs.select', function (e) {
    var orthocoexpgraphids = []
    $('[id^=orthocoexpressionView_]').each(function(){
      orthocoexpgraphids.push($(this).attr("id"))
    });
    async function processOrthoCoExpLabels(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        if (chart !== undefined) {
          var data = chart.series[0].options.data;
          $.each(data, function(i,point){
            point.dataLabels.enabled = $("#orthocoexpviewlabels").val() == "true" ? true : false
          })
          chart.series[0].update(data);
        }
      }
    }
    processOrthoCoExpLabels(orthocoexpgraphids);
  });
  // Display export button
  $('#orthocoexpviewexport').on('hidden.bs.select', function (e) {
    var orthocoexpgraphids = []
    $('[id^=orthocoexpressionView_]').each(function(){
      orthocoexpgraphids.push($(this).attr("id"))
    });
    async function processOrthoCoExpButtons(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        if (chart !== undefined) {
          chart.update({ exporting: { enabled: $("#orthocoexpviewexport").val() == "true" ? true : false } })
        }
      }
    }
    processOrthoCoExpButtons(orthocoexpgraphids);
  });
  // Display tooltips
  $('#orthocoexpviewtooltips').on('hidden.bs.select', function (e) {
    var orthocoexpgraphids = []
    $('[id^=orthocoexpressionView_]').each(function(){
      orthocoexpgraphids.push($(this).attr("id"))
    });
    async function processOrthoCoExpTooltips(items) {
      for (const item of items) {
        await new Promise(resolve => setTimeout(resolve, 0));
        var chart = $('#'+item).highcharts();
        if (chart !== undefined) {
          chart.update({ tooltip: { enabled: $("#orthocoexpviewtooltips").val() == "true" ? true : false } })
        }
      }
    }
    processOrthoCoExpTooltips(orthocoexpgraphids);
  });
  // Sort plot by input or by correlation
  $('#orthocoexpsort').on('hidden.bs.select', function (e) {
    // Select all div with attr data-corr
    var divs = $('[id^=orthocoexpressionView_]');
    // Sort div using $('#orthocoexpsort').val()
    divs.sort(function(a, b) {
      var corrA = parseFloat($(a).attr($('#orthocoexpsort').val()));
      var corrB = parseFloat($(b).attr($('#orthocoexpsort').val()));
      // NaN if no ortholog
      if (isNaN(corrA)) {
          return 1;
      } else if (isNaN(corrB)) {
          return -1;
      }
      return corrB - corrA; // sort desc
    });
    // Add sorted div to container
    $.each(divs, function(index, element) {
      $(element).animate({opacity: 0}, 0);
      $(element).appendTo($(element).parent())
        .delay(50 * index) // Add delay
        .animate({opacity: 1}, {
          duration: 1000,
          queue: true,
          complete: function() {
              $(this).dequeue();
          }
        });
    });
  });
  $('#orthocoexphidecorr').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13') {
      // For each div with attr data-corr
      let nbvisible = 0
      $.each($('[id^=orthocoexpressionView_]'), function(index, div) {
        const dataCorr   = parseFloat($(this).attr('data-corr'));
        const inputValue = parseFloat($('#orthocoexphidecorr').val());
        if (!isNaN(inputValue) && (dataCorr < inputValue || isNaN(dataCorr))) {
          $(div).fadeOut(500);
        }
        else {
          $(div).fadeIn(500);
          nbvisible += 1
        }
      });
      $('#orthocoexpvisible').text(nbvisible)
    }
  });
  // Build plot(s)
  $('#orthocoexpgnIdX').keypress(function(event){
    $(this).removeClass("form-error");
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13' && $('#orthocoexpgnIdX').val() !== "") {
      var inputGeneIds = $('#orthocoexpgnIdX').val().split(/[\s,]+/)
      // By default sort by input
      $('#orthocoexpsort').val("data-input")
      $('#orthocoexpsort').selectpicker('refresh');
      $('#orthocoexpvisible').text(inputGeneIds.length)
      $('#orthocoexpnbplot').text(inputGeneIds.length);
      let h
      let collg
      if(inputGeneIds.length == 1) {
        h = 825
        collg = 12
      }
      else if(inputGeneIds.length <=6 ) {
        h = 400
        collg = 6
      }
      else {
        h = 250
        collg = 4
      }
      let html = '<div class="row ml-0 mr-0">'
      for(let i=0; i<inputGeneIds.length; i++) {
        html += '<div class="col-lg-'+collg+' pl-0 pr-0" id="orthocoexpressionView_'+i+'" data-input="'+(inputGeneIds.length-i)+'" style="height:'+h+'px"></div>'
      }
      html += '</div>'
      $('#orthoexpressionView').html(html)
      $('#orthoexpressionView').css("overflow","auto")
      
      // Container for graph
      var graphDiv = ""
      var graphCompleted = 0
      var promisebuildscatter = new Promise(function(resolve, reject) {
        $.each(inputGeneIds, function(indexDiv, inputGeneId) {
          graphDiv = "orthocoexpressionView_"+indexDiv
          $.ajax({
            url : '/get_expression_for_ortho',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({
              geneIds: inputGeneId,
              graphDiv: graphDiv
            }),
            beforeSend: function(){
              $('#'+graphDiv).html('<img src="/img/poule_blue.gif" style="display:block;margin:auto;margin-top:90px;width:30%;height:auto;">');
            },
            success: function(val){
              // Init colors by tissues
              let colors = []
              val.tissues.forEach(function(t){
                colors.push(tissues_info_ortho[t].color);
              });
              // if ortholog found
              if(val[inputGeneId+"_GGA"] != null && val[inputGeneId+"_HSA"] != null) {
                let data = []
                let arrayOfarray = []
                // To set x and y axis
                let maxX = -Infinity
                let maxY = -Infinity
                let minX = Infinity
                let minY = Infinity
                
                for(let i=0; i<val.tissues.length; i++) {
                  data.push({
                    x:val[inputGeneId+"_GGAe"][i],
                    y:Math.abs(val[inputGeneId+"_HSAe"][i]),
                    name: val.tissues[i],
                    color: colors[i],
                    dataLabels: {
                      enabled: $("#orthocoexpviewlabels").val() == "true" ? true : false,
                      format: '{point.name}',
                      color: colors[i],
                      style: {
                        textOutline: 'none' 
                      }
                    }
                  })
                  arrayOfarray.push([val[inputGeneId+"_GGAe"][i], Math.abs(val[inputGeneId+"_HSAe"][i])])
                  maxX = Math.max(...val[inputGeneId+"_GGAe"].map(Math.abs))
                  maxY = Math.max(...val[inputGeneId+"_HSAe"].map(Math.abs))
                  minX = Math.min(...val[inputGeneId+"_GGAe"].filter(value => value !== 0).map(Math.abs));
                  minY = Math.min(...val[inputGeneId+"_HSAe"].filter(value => value !== 0).map(Math.abs));
                }
                // Points for linear regression 
                let x = [Math.min(minX, minY)]
                x.push(Math.max(maxX, maxY))
                let titletext = null
                let subtitletext = null
                if(inputGeneIds.length === 1) {
                  titletext = 'Co-expression across tissues'
                  subtitletext = 'GEGA - Version ' + updateVersion
                }
                // Get data to plot linearRegression
                $.ajax({
                  url : '/get_linearRegression',
                  type: "POST",
                  contentType: "application/json",
                  data: JSON.stringify({
                    aa : arrayOfarray,
                    x : x
                  }),
                  success: function(val2){
                    // Save correlation in attr of graph container
                    $('#'+val.gd).attr("data-corr", val2.corr)
                    if(val2.corr < parseFloat($('#orthocoexphidecorr').val())) {
                      $('#'+val.gd).fadeOut(500)
                    }
                    Highcharts.chart(val.gd, {
                      chart: {
                        type: 'scatter',
                        zoomType: 'xy',
                        events: {
                          click: function(event) {
                            navigator.clipboard.writeText(val[inputGeneId+"_GGA"])
                              .then(function() {
                                $('#toast_icon').html('<i class="bx bx-copy"></i>')
                                $('#toast_title').html("<b>"+val[inputGeneId+"_GGA"]+"</b>")
                                $('#toast_body').html("Copied to clipboard.")
                                $('#toast').toast('show');
                              })
                              .catch(function(err) {
                                $('#toast_icon').html('<i class="bx bx-copy"></i>')
                                $('#toast_title').html("<b>Error</b>")
                                $('#toast_body').html("Failed to copy: ", err)
                                $('#toast').toast('show');
                              });
                          }
                        }
                      },
                      credits:  { enabled: false },
                      title:    { text: titletext },
                      subtitle: { text: subtitletext },
                      exporting:{ enabled: $("#orthocoexpviewexport").val() == "true" ? true : false },
                      tooltip:  { enabled: $("#orthocoexpviewtooltips").val() == "true" ? true : false, useHTML: true },
                      xAxis: {
                        title: {
                          enabled: true,
                          useHTML : true,
                          text: "<img src='/img/poulet.png' width='17px' /> " + val[inputGeneId+"_GGA"]
                        },
                        startOnTick: true,
                        endOnTick: true,
                        showLastLabel: true,
                        type: $("#orthocoexplog").val() == "linear" ? 'linear' : 'logarithmic',
                        min:  minX, //$("#orthocoexplog").val() == "linear" ? 0 : minX,
                        max:  maxX
                      },
                      yAxis: {
                        title: {
                          enabled: true,
                          useHTML : true,
                          text: "<img src='/img/homme.png' width='17px' /> " + val[inputGeneId+"_HSA"] },
                        type: $("#orthocoexplog").val() == "linear" ? 'linear' : 'logarithmic',
                        min:  minY, //$("#orthocoexplog").val() == "linear" ? 0 : minY,
                        max:  maxY
                      },
                      legend: { enabled: false },
                      plotOptions: {
                        scatter: {
                          marker: {
                            radius: 4,
                            states: {
                              hover: {
                                enabled: true,
                                lineColor: 'rgb(100,100,100)'
                              }
                            }
                          },
                          states: {
                            hover: {
                              marker: {
                                enabled: false
                              }
                            }
                          },
                          tooltip: {
                            headerFormat: '',
                            pointFormatter: function() {
                              return '<span style="font-size:12px"><b>'+this.name+'</b></span><table style="margin-top:5px">'+
                                  '<tr><td style="padding:5px 0 0 0"><img src="/img/poulet.png" width="17px" /> ' +
                                  val[inputGeneId+"_GGA"] + ' : </td>' +
                                  '<td style="padding:3px 0 0 5px;text-align:right"> <b>' +
                                  Math.abs(this.x).toFixed(2) + '</b></td></tr>' +
                                  '<tr><td style="padding:5px 0 0 0"><img src="/img/homme.png" width="17px" /> ' +
                                  val[inputGeneId+"_HSA"] + ' : </td>' +
                                  '<td style="padding:3px 0 0 5px;text-align:right"> <b>' +
                                  Math.abs(this.y).toFixed(2) + '</b></td></tr>';
                            },
                            footerFormat: '</table>'
                          }
                        }
                      },
                      series: [{
                        data: data
                      },
                      {
                        type: 'spline',
                        label: 'Linear regression',
                        data: val2.xy,
                        marker: {
                          enabled: false
                        },
                        states: {
                          hover: {
                            lineWidth: 0
                          }
                        },
                        enableMouseTracking: false,
                        dashStyle: 'dash',
                        opacity: 0.8,
                        visible: $("#coexpviewlinereg").val() == "true" ? true : false
                      }],
                      labels: {
                        items: [{
                          html: '&rho; = '+ val2.corr,
                          style: {
                            left: '10px',
                            top: '10px',
                            fontWeight: 'bold'
                          }
                        }]
                      }
                    });
                  },
                  error : function(e) {
                    console.log("ERROR: ", e);
                  }
                });
              }
              // No ortholog found
              else {
                let html = `
                  <div class="row" style="display:flex;height:250px;margin:auto">
                    <div class="col-lg-2" style="font-size:30px;margin:auto;text-align:right">
                      <i class="bx bx-error" style="color:red;"></i>
                    </div>
                    <div class="col-lg-10" style="margin:auto;text-align:left;color:red">
                      <small><b>No ortholog found for the provided Gene Id :</b><br/>`
                html += "\""+ inputGeneId + "\"<br/>"
                html += '</small></div></div>'
                $('#'+val.gd).html(html)
              }
            },
            complete : function(){
              graphCompleted += 1
              if (graphCompleted === inputGeneIds.length) {
                setTimeout(resolve, 1000);
              }
            },
            error : function(e) {
              console.log("ERROR: ", e);
            }
          });
        })
      })
      promisebuildscatter.then(function() {
        let nbvisible = 0
        $.each($('[id^=orthocoexpressionView_]'), function(index, div) {
          const dataCorr   = parseFloat($(this).attr('data-corr'));
          const inputValue = parseFloat($('#orthocoexphidecorr').val());
          if (dataCorr < inputValue) {
            $(div).fadeOut(500);
          }
          else {
            $(div).fadeIn(500);
            nbvisible += 1
          }
        });
        $('#orthocoexpvisible').text(nbvisible)
      });
    }
  });
});