$(document).ready(function(){
  // Gloobal ptions for all highcharts
  Highcharts.setOptions({
    exporting: {
      buttons: {
        contextButton: {
          menuItems: [
            'downloadPNG',
            'downloadJPEG',
            'downloadPDF',
            'downloadSVG',
            "separator",
            "downloadCSV",
            "downloadXLS"
          ]
        }
      }
    },
    lang: {
      decimalPoint: '.',
      thousandsSep: ''
    }
  });

  Highcharts.chart('piencbi', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
      margin: [60,20,30,20],
      backgroundColor: '#fff',
      borderColor: '#e6e6e6',
      borderWidth: 1
    },
    title: { text: 'NCBI gene models (n=25670)' },
    subtitle: { text: 'GEGA - Version ' + updateVersion },
    credits: { enabled: false },
    tooltip: {
      useHTML: true,
      formatter: function(){
        return '<i><b>' + this.point.name + '</b></i><br><hr style="margin:5px 0 5px 0">' +
          'Percent: ' + this.point.percentage.toFixed(1) + '%</br>' +
          'Count: ' + this.point.y;
      }
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y}'
        }
      }
    },
    series: [{
      name: 'NCBI',
      colorByPoint: true,
      data: [
        {
          name: 'protein_coding',
          y: 18022,
          color: "#557da4"
        }, {
          name: 'lncRNA',
          y: 5789,
          color: "#b73c3c"
        }, {
          name: 'miRNA',
          y: 799,
          color: "#c9a044"
        }, {
          name: 'tRNA',
          y: 303,
          color: "#969696"
        }, {
          name: 'snoRNA',
          y: 186,
          color: "#969696"
        }, {
          name: 'pseudogene',
          y: 172,
          color: "#969696"
        }, {
          name: 'rRNA',
          y: 94,
          color: "#969696"
        }, {
          name: 'misc_RNA',
          y: 90,
          color: "#969696"
        }, {
          name: 'V_segment',
          y: 68,
          color: "#969696"
        }, {
          name: 'snRNA',
          y: 67,
          color: "#969696"
        }, {
          name: 'None Assignated',
          y: 34,
          color: "#969696"
        }, {
          name: 'transcribed_pseudogene',
          y: 26,
          color: "#969696"
        }, {
          name: 'ncRNA',
          y: 13,
          color: "#969696"
        }, {
          name: 'C_region',
          y: 2,
          color: "#969696"
        }, {
          name: 'Y_RNA',
          y: 2,
          color: "#969696"
        }, {
          name: 'other/NA',
          y: 1,
          color: "#969696"
        }, {
          name: 'SRP_RNA',
          y: 1,
          color: "#969696"
        }, {
          name: 'telomerase_RNA',
          y: 1,
          color: "#969696"
        }
      ]
    }]
  });
  Highcharts.chart('pieensembl', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
      margin: [60,20,30,20],
      backgroundColor: '#fff',
      borderColor: '#e6e6e6',
      borderWidth: 1
    },
    title: { text: 'Ensembl gene models (n=30108)' },
    subtitle: { text: 'GEGA - Version ' + updateVersion },
    credits: { enabled: false },
    tooltip: {
      useHTML: true,
      formatter: function(){
        return '<i><b>' + this.point.name + '</b></i><br><hr style="margin:5px 0 5px 0">' +
          'Percent: ' + this.point.percentage.toFixed(1) + '%</br>' +
          'Count: ' + this.point.y;
      }
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y}'
        }
      }
    },
    series: [{
      name: 'Ensembl',
      colorByPoint: true,
      data: [
        {
          name: 'protein_coding',
          y: 17007,
          color: "#557da4"
        }, {
          name: 'lncRNA',
          y: 11944,
          color: "#b73c3c"
        }, {
          name: 'miRNA',
          y: 674,
          color: "#c9a044"
        }, {
          name: 'snoRNA',
          y: 194,
          color: "#969696"
        }, {
          name: 'rRNA',
          y: 96,
          color: "#969696"
        }, {
          name: 'snRNA',
          y: 82,
          color: "#969696"
        }, {
          name: 'pseudogene',
          y: 46,
          color: "#969696"
        }, {
          name: 'Mt_tRNA',
          y: 22,
          color: "#969696"
        }, {
          name: 'processed_pseudogene',
          y: 15,
          color: "#969696"
        }, {
          name: 'scaRNA',
          y: 15,
          color: "#969696"
        }, {
          name: 'misc_RNA',
          y: 5,
          color: "#969696"
        }, {
          name: 'Mt_RNA',
          y: 2,
          color: "#969696"
        }, {
          name: 'ribozyme',
          y: 2,
          color: "#969696"
        }, {
          name: 'vault_RNA',
          y: 2,
          color: "#969696"
        }, {
          name: 'Y_RNA',
          y: 2,
          color: "#969696"
        }
      ]
    }]
  });
  Highcharts.chart('piegathered', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
      margin: [60,20,30,20],
      backgroundColor: '#fff',
      borderColor: '#e6e6e6',
      borderWidth: 1
    },
    title: { text: 'Gathered gene models (n=78323)' },
    subtitle: { text: 'GEGA - Version ' + updateVersion },
    credits: { enabled: false },
    tooltip: {
      useHTML: true,
      formatter: function(){
        return '<i><b>' + this.point.name + '</b></i><br><hr style="margin:5px 0 5px 0">' +
          'Percent: ' + this.point.percentage.toFixed(1) + '%</br>' +
          'Count: ' + this.point.y;
      }
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y}'
        }
      }
    },
    series: [{
      name: 'Gathered',
      colorByPoint: true,
      data: [
        {
          name: 'lncRNA',
          y: 44428,
          color: "#b73c3c"
        }, {
          name: 'protein_coding',
          y: 23926,
          color: "#557da4"
        }, {
          name: 'other/NA',
          y: 6238,
          color: "#969696"
        }, {
          name: 'miRNA',
          y: 991,
          color: "#c9a044"
        }, {
          name: 'misc_RNA',
          y: 936,
          color: "#969696"
        }, {
          name: 'TUCp',
          y: 533,
          color: "#969696"
        }, {
          name: 'tRNA',
          y: 303,
          color: "#969696"
        }, {
          name: 'snoRNA',
          y: 213,
          color: "#969696"
        }, {
          name: 'pseudogene',
          y: 194,
          color: "#969696"
        }, {
          name: 'mRNA',
          y: 176,
          color: "#969696"
        }, {
          name: 'rRNA',
          y: 113,
          color: "#969696"
        }, {
          name: 'snRNA',
          y: 93,
          color: "#969696"
        }, {
          name: 'V_segment',
          y: 68,
          color: "#969696"
        }, {
          name: 'transcribed_pseudogene',
          y: 26,
          color: "#969696"
        }, {
          name: 'Mt_tRNA',
          y: 22,
          color: "#969696"
        }, {
          name: 'processed_pseudogene',
          y: 15,
          color: "#969696"
        }, {
          name: 'IG_V_gene',
          y: 14,
          color: "#969696"
        }, {
          name: 'ncRNA',
          y: 13,
          color: "#969696"
        }, {
          name: 'noORF',
          y: 6,
          color: "#969696"
        }, {
          name: 'scaRNA',
          y: 3,
          color: "#969696"
        }, {
          name: 'C_region',
          y: 2,
          color: "#969696"
        }, {
          name: 'Mt_rRNA',
          y: 2,
          color: "#969696"
        }, {
          name: 'ribozyme',
          y: 2,
          color: "#969696"
        }, {
          name: 'Y_RNA',
          y: 2,
          color: "#969696"
        }, {
          name: 'sRNA',
          y: 1,
          color: "#969696"
        }, {
          name: 'SRP_RNA',
          y: 1,
          color: "#969696"
        }, {
          name: 'telomerase_RNA',
          y: 1,
          color: "#969696"
        }, {
          name: 'vault_RNA',
          y: 1,
          color: "#969696"
        }
      ]
    }]
  });
  Highcharts.chart('pieexpression', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
      margin: [60,20,30,20],
      backgroundColor: '#fff',
      borderColor: '#e6e6e6',
      borderWidth: 1
    },
    title: { text: 'Gathered expressed gene models (n=63491)' },
    subtitle: { text: 'GEGA - Version ' + updateVersion },
    credits: { enabled: false },
    tooltip: {
      useHTML: true,
      formatter: function(){
        return '<i><b>' + this.point.name + '</b></i><br><hr style="margin:5px 0 5px 0">' +
          'Percent: ' + this.point.percentage.toFixed(1) + '%</br>' +
          'Count: ' + this.point.y;
      }
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y}'
        }
      }
    },
    series: [{
      name: 'Expression',
      colorByPoint: true,
      data: [
        {
          name: 'lncRNA',
          y: 35257,
          color: "#b73c3c"
        }, {
          name: 'protein_coding',
          y: 22343,
          color: "#557da4"
        }, {
          name: 'other/NA',
          y: 4490,
          color: "#969696"
        }, {
          name: 'misc_RNA',
          y: 525,
          color: "#969696"
        }, {
          name: 'TUCp',
          y: 335,
          color: "#969696"
        }, {
          name: 'mRNA',
          y: 125,
          color: "#969696"
        }, {
          name: 'snoRNA',
          y: 122,
          color: "#969696"
        }, {
          name: 'V_segment',
          y: 67,
          color: "#969696"
        }, {
          name: 'rRNA',
          y: 60,
          color: "#969696"
        }, {
          name: 'snRNA',
          y: 41,
          color: "#969696"
        }, {
          name: 'transcribed_pseudogene',
          y: 26,
          color: "#969696"
        }, {
          name: 'Mt_tRNA',
          y: 22,
          color: "#969696"
        }, {
          name: 'tRNA',
          y: 20,
          color: "#969696"
        }, {
          name: 'IG_V_gene',
          y: 13,
          color: "#969696"
        }, {
          name: 'ncRNA',
          y: 13,
          color: "#969696"
        }, {
          name: 'processed_pseudogene',
          y: 12,
          color: "#969696"
        }, {
          name: 'pseudogene',
          y: 4,
          color: "#969696"
        }, {
          name: 'noORF',
          y: 3,
          color: "#969696"
        }, {
          name: 'scaRNA',
          y: 3,
          color: "#969696"
        }, {
          name: 'C_region',
          y: 2,
          color: "#969696"
        }, {
          name: 'Mt_rRNA',
          y: 2,
          color: "#969696"
        }, {
          name: 'ribozyme',
          y: 2,
          color: "#969696"
        }, {
          name: 'Y_RNA',
          y: 2,
          color: "#969696"
        }, {
          name: 'SRP_RNA',
          y: 1,
          color: "#969696"
        }, {
          name: 'telomerase_RNA',
          y: 1,
          color: "#969696"
        }
      ]
    }]
  });
});