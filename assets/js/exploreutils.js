// Function to build array of json object with filter info from filter box    
const buildFilterArray = () => {
  let output = [];
  $('[id^=filterRow_]').each(function(){
    let index = $(this).attr('id').split("_")[1]
    if($("#filterRemove_"+index).css("display") != "none" &&
       $("#filterSelect_"+index).val() != "" &&
       $("#filterText_"+index).val() != "") {
      output.push({
        "f": encodeURIComponent($("#filterSelect_"+index).val()),
        "d": encodeURIComponent($("#filterSelect_"+index).find('option:selected').attr("data-database")),
        "o": encodeURIComponent($("#filterOperator_"+index).val()),
        "v": encodeURIComponent($("#filterText_"+index).val())
      })
    }
  });
  return output;
}

// Explore valid filter form ?
const isValidFilterForm = (i) => {
  var valid = true
  $('#filterSelect_'+i).parent().parent().find('*').removeClass("form-error");
  $('#filterOperator_'+i).parent().parent().find('*').removeClass("form-error");
  $('#filterText_'+i).parent().children().removeClass("form-error");
  if($('#filterSelect_'+i).val().length == 0) {
    $('#filterSelect_'+i).parent().parent().find('*').addClass("form-error");
    $('#filterOperator_'+i).parent().parent().find('*').addClass("form-error");
    valid = false
  }
  if($('#filterText_'+i).val() == "") {
    $('#filterText_'+i).parent().children().addClass("form-error");
    valid = false
  }
  return valid
}

// Explore update operator foreach line of filter form 
const updateOperatorOnFilterEvent = () => {
  $('[id^=filterSelect_]').on('changed.bs.select', function (e) {
    let i = $(this).attr('id').split("_")[1]
    $('#filterOperator_'+i+ ' option').remove();
    $('#filterOperator_'+i).selectpicker('refresh');
    let f = encodeURIComponent($("#filterSelect_"+i).val());
    if(f != "" && $('option:selected', this).attr("data-type") === "string") {
      $('#filterOperator_'+i).append('<option value="~"  data-content="contains"></option>')
      $('#filterOperator_'+i).append('<option value="!~" data-content="not contains"></option>')
      $('#filterOperator_'+i).append('<option value="="  data-content="equals"></option>')
      $('#filterOperator_'+i).append('<option value="!=" data-content="not equals"></option>')
      $('#filterOperator_'+i).selectpicker('refresh');
      $('#filterOperator_'+i).selectpicker('val','~')
    }
    else if(f != "" && $('option:selected', this).attr("data-type") === "int") {
      $('#filterOperator_'+i).append('<option value="="  data-content="equals"></option>')
      $('#filterOperator_'+i).append('<option value="!=" data-content="not equals"></option>')
      $('#filterOperator_'+i).append('<option value=">="  data-content="greater than"></option>')
      $('#filterOperator_'+i).append('<option value="<="  data-content="lower than"></option>')
      $('#filterOperator_'+i).selectpicker('refresh');
      $('#filterOperator_'+i).selectpicker('val','=')
    }
  });
}

// Get any percentile from an array (boxplot)
const getPercentile = (data, percentile) => {
  const n = data.length;
  const k = percentile / 100 * (n - 1) + 1;
  const f = Math.floor(k);
  const c = Math.ceil(k);
  const delta = k - f;
  return (1 - delta) * data[f - 1] + delta * data[c - 1];
}
// Test if numeric
const isNumeric = (val) => {
  return (typeof val === 'number' || !isNaN(parseFloat(val)));
}
// Numerical sort (boxplot)
const numSort = (a,b) => { 
    return a - b; 
}
// Build data for box plot from array (boxplot)
// Warning data must be sorted
const getBoxValues = (data) => {
  var boxValues = {};
  boxValues.low    = parseFloat(data[0]);
  boxValues.q1     = getPercentile(data, 25);
  boxValues.median = getPercentile(data, 50);
  boxValues.q3     = getPercentile(data, 75);
  boxValues.high   = parseFloat(data[data.length - 1]);
  return boxValues;
}

function hexToRgba(hex, opacity) {
  const r = parseInt(hex.substring(1, 3), 16);
  const g = parseInt(hex.substring(3, 5), 16);
  const b = parseInt(hex.substring(5, 7), 16);
  const rgba = `rgba(${r}, ${g}, ${b}, ${opacity})`;
  return rgba;
}

// Venn functions 
const getArrayFromArea = (areaID) => {
  var lines = $("#"+areaID).val().split("\n");
  var table = new Array();
  for (var lindex in lines) {
    table.push(lines[lindex].trim());
  }
  return (table);
}
String.prototype.convertToRGB = function(){
  let aRgbHex = this.substring(1).match(/.{1,2}/g);
  let aRgb = [
    parseInt(aRgbHex[0], 16),
    parseInt(aRgbHex[1], 16),
    parseInt(aRgbHex[2], 16)
  ];
  return "rgb("+aRgb+")";
}
const updateJvenn = () => {
  let	seriesTable = new Array();
  let num = 0;
  for(var i=6; i>=1; i--) {
    if($("#venn"+i+"-ta").val() != "") {
      num = i;
      break;
    }
  }
  for(var i=1; i<=num; i++) {
    seriesTable.push({
      name: $("#venn"+i+"-name").val(),
      data: getArrayFromArea("venn"+i+"-ta")
    });
  }
  let colorsTable = new Array();
  colorsTable.push($('#venn1-color').val().convertToRGB());
  colorsTable.push($('#venn2-color').val().convertToRGB());
  colorsTable.push($('#venn3-color').val().convertToRGB());
  colorsTable.push($('#venn4-color').val().convertToRGB());
  colorsTable.push($('#venn5-color').val().convertToRGB());
  colorsTable.push($('#venn6-color').val().convertToRGB());
  $('#vennView').jvenn({
    series: seriesTable,
    colors: colorsTable,
    displayMode: $("#vennDisplaySelect").val(),
    displayStat: $("#vennStatSelect").val() == "true" ? true : false,
    displaySwitch: false,
    searchInput: $("#vennFind"),
    searchStatus:$("#vennFindStatus"),
    fnClickCallback: function() {
      var value = "";
      if (this.listnames.length == 1) {
        value += "Elements only in ";
      } else {
        value += "Common elements in ";
      }
      for (n in this.listnames) {
        value += this.listnames[n] + " ";
      }
      value += ":\n";
      if (this.list.length == 0) {
        value += "No element in this set\n"
      }
      else {
        for (val in this.list) {
          value += this.list[val] + "\n";
        }
      }
      $("#vennElements").val(value);
    }
  });
};

// Clear Define Region Left
const clearDefineRegionLeft = () => {
  $('#drLeftOffset').val(0)
  $('#drLeftGeneNameStatus').text("")
  $('#drLeftGeneIdStatus').text("")
  $('#drLeftChrStatus').text("")
  $('#drLeftOffsetStatus').fadeOut(500)
  $('[id^=HCoffset-posleft]').fadeOut(500)
  $('#HCchrleftLabel').hide()
  $('#HCchrleftLabel').children().text('Chr. : ?')
  $('#HCchrleftLabel').show()
  $('#HCposleftLabel').children().text('?')
  $('#HCgeneleft').fadeOut(500)
  $('#HCgeneleft').attr('transform', 'translate(101,120)')
  $('#HCgeneleftLabel1').hide()
  $('#HCgeneleftLabel1').attr('transform', 'translate(102,91)')
  $('#HCgeneleftLabel1').children().text('?')
  $('#HCgeneleftLabel2').hide()
  $('#HCgeneleftLabel2').attr('transform', 'translate(102,103)')
  $('#HCgeneleftLabel2').children().text('?')
  $('[id^=HCwrong]').fadeOut(500)
  $('#HC-wrong-pos').fadeOut(500)
};
// Clear Define Region Right
const clearDefineRegionRight = () => {
  $('#drRightOffset').val(0)
  $('#drRightGeneNameStatus').text("")
  $('#drRightGeneIdStatus').text("")
  $('#drRightChrStatus').text("")
  $('#drRightOffsetStatus').fadeOut(500)
  $('[id^=HCoffset-posright]').fadeOut(500);
  $('#HCchrrightLabel').hide()
  $('#HCchrrightLabel').children().text('Chr. : ?')
  $('#HCchrrightLabel').show()
  $('#HCposrightLabel').children().text('?')
  $('#HCgeneright').fadeOut(500)
  $('#HCgeneright').attr('transform', 'translate(718,120)')
  $('#HCgenerightLabel1').hide()
  $('#HCgenerightLabel1').attr('transform', 'translate(719,91)')
  $('#HCgenerightLabel1').children().text('?')
  $('#HCgenerightLabel2').hide()
  $('#HCgenerightLabel2').attr('transform', 'translate(719,103)')
  $('#HCgenerightLabel2').children().text('?')
  $('[id^=HCwrong]').fadeOut(500)
  $('#HC-wrong-pos').fadeOut(500)
};
// Test Define Region
const updateDefineRegion = (drVar) => {
  $('#defineRegionFilter').removeClass("btn-primary")
  $('#defineRegionFilter').addClass("btn-secondary")
  $('#defineRegionFilter').prop('disabled', true);
  $('#HC-wrong-pos').fadeOut(500)
  if(drVar.leftChr != drVar.rightChr && drVar.leftChr != '' && drVar.rightChr != '') {
    $('[id^=HCwrong]').fadeIn(500)
    $('[id^=HCopacity]').css('opacity','0.6')
    $('#drChrOK').val("?")
    $('#drStartOK').val("?")
    $('#drEndOK').val("?")
  }
  else {
    $('[id^=HCwrong]').fadeOut(500)
    $('[id^=HCopacity]').css('opacity','1')
    if(drVar.leftChr != '' && drVar.rightChr != '') {
      let beg = drVar.leftPos - drVar.leftOffset
      let end = drVar.rightPos + drVar.rightOffset
      $('#drChrOK').val(drVar.leftChr)
      $('#drStartOK').val(beg)
      $('#drEndOK').val(end)
      if(beg<end) {
        $('#defineRegionFilter').removeClass("btn-secondary")
        $('#defineRegionFilter').addClass("btn-primary")
        $('#defineRegionFilter').prop('disabled', false);
      }
      else {
        $('#HC-wrong-pos').fadeIn(500)
      }
    }
    else {
      $('#drChrOK').val("?")
      $('#drStartOK').val("?")
      $('#drEndOK').val("?")
    }
  }
};

// Compute distance between two genes
const geneDistance = (start1, end1, start2, end2) => {
  const d1 = Math.abs(start1 - start2);
  const d2 = Math.abs(end1 - end2);
  const d3 = Math.abs(start1 - end2);
  const d4 = Math.abs(end1 - start2);
  
  return Math.min(d1, d2, d3, d4);
}